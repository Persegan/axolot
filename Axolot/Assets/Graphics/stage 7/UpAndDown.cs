﻿using UnityEngine;
using System.Collections;

public class UpAndDown : MonoBehaviour {

	public float speed;

	public Vector3 UpDif;
	public Vector3 DownDif;

	Vector3 Min;
	Vector3 Max;

	bool UpTdownF = true;

	// Use this for initialization
	void Start () {
		Min = this.gameObject.transform.position - DownDif;
		Max = this.gameObject.transform.position + UpDif;
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameObject.transform.position.y >= Max.y) {
			UpTdownF = false;
			this.gameObject.transform.Translate (0, -speed * Time.deltaTime, 0);
		} else if (this.gameObject.transform.position.y <= Min.y) {
			UpTdownF = true;
		}
		if (UpTdownF) {
			this.gameObject.transform.Translate (0, speed * Time.deltaTime, 0);
		} else if(!UpTdownF) {
			this.gameObject.transform.Translate (0, -speed * Time.deltaTime, 0);
		}

	}
}
