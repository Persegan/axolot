﻿using UnityEngine;
using System.Collections;

public class LeftAndRight : MonoBehaviour {

	public float speed;

	public Vector3 RightDif;
	public Vector3 LeftDif;

	Vector3 Min;
	Vector3 Max;

	bool RightTleftF = true;

	// Use this for initialization
	void Start () {
		Min = this.gameObject.transform.position - LeftDif;
		Max = this.gameObject.transform.position + RightDif;
	}
	
	// Update is called once per frame
	void Update () {
		if (this.gameObject.transform.position.x >= Max.x) {
			RightTleftF = false;
		} else if (this.gameObject.transform.position.x <= Min.x) {
			RightTleftF = true;
		}
		if (RightTleftF) {
			this.gameObject.transform.Translate (speed * Time.deltaTime,0, 0);
		} else if(!RightTleftF) {
			this.gameObject.transform.Translate (-speed * Time.deltaTime,0, 0);
		}

	}
}
