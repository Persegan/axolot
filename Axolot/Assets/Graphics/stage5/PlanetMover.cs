﻿using UnityEngine;
using System.Collections;

public class PlanetMover : MonoBehaviour {
	public float speed;
	public float speed2;
	GameObject littlePlanet;
	GameObject bigPlanet;
	public float limiteMax;
	public float limiteMin;
	bool little = true;
	bool big = false;

	Vector3 restart;
	Vector3 restart2;

	// Use this for initialization
	void Start () {
		littlePlanet=this.gameObject.transform.GetChild(0).gameObject;
		bigPlanet=this.gameObject.transform.GetChild(1).gameObject;

		restart = littlePlanet.transform.position;
		restart2 = bigPlanet.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (little)
		if (littlePlanet.transform.position.x < limiteMax) {
			littlePlanet.transform.Translate (speed * Time.deltaTime, 0, 0);
		} else if (littlePlanet.transform.position.x >= limiteMax) {
			littlePlanet.transform.position= restart;
			little = false;
			big = true;
		}
		if (big)
		if (bigPlanet.transform.position.x > limiteMin) {
			bigPlanet.transform.Translate (-speed2 * Time.deltaTime, 0, 0);
		} else if (bigPlanet.transform.position.x <= limiteMin) {
			bigPlanet.transform.position= restart2;
			little = true;
			big = false;
		}

		}
}
