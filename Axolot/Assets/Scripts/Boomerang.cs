﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Boomerang : MonoBehaviour {

	public GameObject ship;
	public float rotation_speed;    // how fast it spins
	public float speed;				// the speed at which it will travel
	public float returning_speed;	// the max speed that will reach when returning
	public float time_life;     	// it will return after that duartion
	private bool fired;				// indicates if it has been fired
	public bool destructable;    	// if true, the enemy can shoot it down
	private Vector3 direction;		// used to save the direction the player was facing when he fired it
	private float duration;   		// used to count how much time has elapsed it was fired
	//public float attraction_power;  // how strong the force will be pushing it back to the player
	public Sprite icon;


	// Use this for initialization
	void Start () {
		if (time_life <= 0) 		// guarantees that time life will be at least 2 if you forget to set it
		{
			time_life = 2;
		}
		duration = time_life;
		fired = false;

		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = "";	// this is for the ammo UI

		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire) && fired == false) 
		{
			fired = true;
			direction = ship.transform.up;
		}
	}

	void FixedUpdate()
	{
		if (fired == false) {
			transform.position = ship.transform.position;
			transform.rotation = ship.transform.rotation;
		} 
		else 
		{	
			foreach (Collider2D coll in GetComponents<Collider2D>()) {
				coll.enabled = true;	
			}
					// the power up can collide with thing only after it is fired
			GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;

			if (duration > 0)							// if the duration is positive, it is still travelling forward
			{
				duration -= Time.fixedDeltaTime;
				GetComponent<Rigidbody2D>().AddForce (direction * speed);

			}
			else {     				// if the duration is negative, it will start returning to the player
				GetComponent<EdgeCollider2D> ().isTrigger = true;		// this is used as a special interaction when the boomerang is returning
				Vector3 velocity = GetComponent<Rigidbody2D>().velocity;
				transform.position = Vector3.SmoothDamp(transform.position, ship.transform.position, ref velocity, 0.05F, returning_speed);
			}			
		}
	}

	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject == ship  && duration <= 0)		// when it returns to the owner
		{
			//Instantiate(gameObject, ship.transform.position, new Quaternion());    // for testing purposes, you get another one
			ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
			Destroy (gameObject);
		}
		else if(collider.gameObject.tag == "satellite" || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "shield" 
				|| collider.gameObject.tag == "explosion" || collider.gameObject.tag == "reflector")   // it is the same result as the above but im doing it like this for easier testing
		{
			if (collider.gameObject.tag == "shield")
			{
				if (collider.gameObject.GetComponent<Shield> ().ship == ship) 
				{
				} 
				else 
				{
					ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
					ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
					Destroy (gameObject);
				}
			} 
			else {
				ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
				ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
				Destroy (gameObject);
			}
		}
		else if(collider.gameObject.tag == "terrain"){		// if collides with terrain, it will start the returning
			duration = -1;
		}
	}


	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject == ship  && duration <= 0)		// when it returns to the owner
		{
			//Instantiate(gameObject, ship.transform.position, new Quaternion());    // for testing purposes, you get another one
			ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
			Destroy (gameObject);
		}
		else if(collider.gameObject.tag == "satellite" || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "shield" 
			|| collider.gameObject.tag == "explosion" || collider.gameObject.tag == "reflector")   // it is the same result as the above but im doing it like this for easier testing
		{
			if (collider.gameObject.tag == "shield") {
				if (collider.gameObject.GetComponent<Shield> ().ship == ship) {

				} else {
					ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
					ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
					Destroy (gameObject);
				}
			} else {
				ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
				ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
				Destroy (gameObject);
			}
		}
	}

}
