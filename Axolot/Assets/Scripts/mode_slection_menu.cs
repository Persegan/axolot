﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class mode_slection_menu : MonoBehaviour {
	public Button FreeForAll;
	public GameObject laser;

	// Use this for initialization
	void Start () {
		Cursor.visible = false;
		FreeForAll.Select ();
		if (Music_Manager.music_manager.Song4.isPlaying == true) 
		{

		} else 
		{
			Music_Manager.music_manager.Song4.loop = true;
			Music_Manager.music_manager.Song4.Play ();

		}
	
	}
	
	// Update is called once per frame
	void Update () {
		Cursor.lockState = CursorLockMode.Locked;
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			SceneManager.LoadScene ("menu");

		}
	}


	public void mouseclick()
	{
		FreeForAll.Select ();
	}

	public void SubmitFreeForAll()
	{
		Instantiate (laser, FreeForAll.transform.position, FreeForAll.transform.rotation);
		SceneManager.LoadScene ("Player_Selection_Screen");
	}
		


}
