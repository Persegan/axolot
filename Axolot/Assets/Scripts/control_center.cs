﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class control_center : MonoBehaviour {
	public int puntuacion;
	public Text puntuacion_texto;
	public bool game_over = false;
	public bool actualizar_puntuacion = true;
	// Use this for initialization
	void Start () 
	{
		puntuacion_texto = puntuacion_texto.GetComponent <Text>();
	
	}

	void Awake()
	{
		DontDestroyOnLoad (gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void FixedUpdate()
	{
		if (actualizar_puntuacion == true)
		{
		puntuacion_texto.text = "Score: ";
		puntuacion_texto.text += puntuacion.ToString ();
		}

		if (game_over == true)
		{
			StartCoroutine(GameOver());
		
		}
	}

	IEnumerator GameOver()
	{
		yield return new WaitForSeconds (1.25f);
		SceneManager.LoadScene("Game_Over");
		game_over = false;
		actualizar_puntuacion = false;
	}
}
