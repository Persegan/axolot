﻿using UnityEngine;
using System.Collections;

public class MobScript : MonoBehaviour 
{
	public float FuerzaMin = 20f;
	public float FuerzaMax = 40f;
	public float TiempoCambioDireccion = 1f;
	public float IntervaloDisparos = 1f;
	public GameObject BalaMob;
	public float maxSpeed;
	public float leftBorder;
	public float rightBorder;
	public float topBorder;
	public float bottomBorder;
	public GameObject Explosion_Alien;
	public GameObject AreaEsquivar;

	private GameObject jugador;
	private float IntervaloCambioDireccion;
	private float intervaloDisparo;


	// Use this for initialization
	void Start () 
	{
		Empujar ();
		IntervaloCambioDireccion = TiempoCambioDireccion;
		intervaloDisparo = IntervaloDisparos;
		gameObject.GetComponent<Rigidbody2D>().angularDrag = 100000000;
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		IntervaloCambioDireccion -= Time.deltaTime;
		if (IntervaloCambioDireccion < 0)
		{
			Empujar();
			IntervaloCambioDireccion = TiempoCambioDireccion;
		}
		intervaloDisparo -= Time.deltaTime;
		if (intervaloDisparo<0)
		{
			Disparar();
			intervaloDisparo = IntervaloDisparos;
		}

	
	}

	void FixedUpdate()
	{
		// Limite de velocidad
		if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
		{
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
		}

		//rebotar en los bordes de la pantalla
		if((transform.position.x <= leftBorder) && GetComponent<Rigidbody2D>().velocity.x < 0f){
			GetComponent<Rigidbody2D>().velocity = new Vector2(-GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);
		}
		
		if((transform.position.x >= rightBorder) && GetComponent<Rigidbody2D>().velocity.x > 0){
			GetComponent<Rigidbody2D>().velocity = new Vector2(-GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y);
		}
		
		if((transform.position.y <= bottomBorder) && GetComponent<Rigidbody2D>().velocity.y < 0){
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -GetComponent<Rigidbody2D>().velocity.y);
		}
		
		if((transform.position.y >= topBorder) && GetComponent<Rigidbody2D>().velocity.y > 0){
			GetComponent<Rigidbody2D>().velocity = new Vector2(GetComponent<Rigidbody2D>().velocity.x, -GetComponent<Rigidbody2D>().velocity.y);
		}

	}

	void Empujar()
	{
		float fuerza = Random.Range (FuerzaMin, FuerzaMax);
		float x = Random.Range (-1f, 1f);
		float y = Random.Range (-1f, 1f);
		GetComponent<Rigidbody2D>().AddForce (fuerza * new Vector2(x, y));

	}
	void Disparar()
	{

		//encontrar posicion jugador
		jugador = UnityEngine.GameObject.FindGameObjectWithTag ("jugador");
		if (jugador != null)
		{ 

			//encontrar angulo jugador
			float angulo = (Mathf.Atan2 (transform.position.y - jugador.transform.position.y, transform.position.x - jugador.transform.position.x)+ Mathf.PI/2) * Mathf.Rad2Deg;

			//apuntar al jugador
			Instantiate (BalaMob, transform.position, Quaternion.Euler(new Vector3(0f, 0f, angulo)));
		}

	}



	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "bala_jugador")
		{
			Destroy (gameObject);
			Destroy (AreaEsquivar);
			Destroy (collider.gameObject);
			Instantiate (Explosion_Alien, transform.position, new Quaternion());
			GameObject.Find("Game_control_center").GetComponent<control_center>().puntuacion += 150;
		}
		
	}

}
