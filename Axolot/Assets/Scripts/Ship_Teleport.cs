﻿using UnityEngine;
using System.Collections;

public class Ship_Teleport : MonoBehaviour 
{
	/*public float MinX;
	public float MinY;
	public float MaxX;
	public float MaxY;*/
	Vector3 stageDimensionsmax;
	Vector3 stageDimensionsmin;
	private float distanciaextra = 0.35f;

	void Start()
	{
		stageDimensionsmin = Camera.main.ScreenToWorldPoint (new Vector3(-0,-0,0));
		stageDimensionsmax = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 0));
		stageDimensionsmin.x -= distanciaextra;
		stageDimensionsmax.x += distanciaextra;
		stageDimensionsmin.y -= distanciaextra;
		stageDimensionsmax.y += distanciaextra;
	}

	void FixedUpdate () 
	{
		float x = transform.position.x;
		float y = transform.position.y;
		/*
		if (x<MinX)
		{
			x = MaxX;

		}
		else if (x >MaxX)
		{
			x = MinX;
		}
		if (y<MinY)
		{
			y = MaxY;
			
		}
		else if (y >MaxY)
		{
			y = MinY;
		}
		transform.position = new Vector3 (x, y, transform.position.z);
		*/
		if (x < stageDimensionsmin.x)
		{
			x = stageDimensionsmax.x;
			transform.position = new Vector2 (x, y);
		}

		else if ( x > stageDimensionsmax.x)
		{
			x = stageDimensionsmin.x;
			transform.position = new Vector2 (x, y);
		}

		if (y < stageDimensionsmin.y)
		{
			y = stageDimensionsmax.y;
			transform.position = new Vector2 (x, y);
		}
		else if ( y > stageDimensionsmax.y)
		{
			y = stageDimensionsmin.y;
			transform.position = new Vector2 (x, y);
		}
	}
}
