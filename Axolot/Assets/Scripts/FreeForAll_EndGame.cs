﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class FreeForAll_EndGame : MonoBehaviour {

	public Text P1Score;
	public Text P2Score;
	public Text P3Score;
	public Text P4Score;

	public GameObject first;
	public GameObject second;
	public GameObject third;

	private int score1;
	private int score2;
	private int score3;
	private int score4;


	void Awake()
	{
		AudioSource[] songs;
		songs = Music_Manager.music_manager.GetComponentsInChildren<AudioSource> ();
		foreach (AudioSource audio in songs)
		{
			audio.Stop ();
		}
	}


	// Use this for initialization
	void Start () {

		//We enable the Score UI elements according to the number of players that played
		if (Controls_Manager.controls_manager.Player1.ready == true) 
		{
			P1Score.enabled = true;
			P1Score.text += Controls_Manager.controls_manager.Player1.score.ToString();
			score1 = Controls_Manager.controls_manager.Player1.score;
		}
		if (Controls_Manager.controls_manager.Player2.ready == true) 
		{
			P2Score.enabled = true;
			P2Score.text += Controls_Manager.controls_manager.Player2.score.ToString();
			score2 = Controls_Manager.controls_manager.Player2.score;
		}
		if (Controls_Manager.controls_manager.Player3.ready == true) 
		{
			P3Score.enabled = true;;
			P3Score.text += Controls_Manager.controls_manager.Player3.score.ToString();
			score3 = Controls_Manager.controls_manager.Player3.score;
		}
		if (Controls_Manager.controls_manager.Player4.ready == true) 
		{
			P4Score.enabled = true;;
			P4Score.text += Controls_Manager.controls_manager.Player4.score.ToString();
			score4 = Controls_Manager.controls_manager.Player4.score;
		}


	
		//We try to define who the winner is. 1st scenario, player 1 has the highest score
		if (score1 >= score2 && score1 >= score3 && score1 >= score4) 
		{
			first.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
			Text first_text = first.GetComponentInChildren<Text> ();
			first_text.color = Color.red;
			first_text.text = "P1";

			if (score2 >= score3 && score2 >= score4) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.blue;
				second_text.text = "P2";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score3 >= score4) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.green;
						third_text.text = "P3";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.yellow;
						third_text.text = "P3";
					}

				}


			}

			else if (score3 >= score2 && score3 >= score4) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.green;
				second_text.text = "P3";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score2 >= score4) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.blue;
						third_text.text = "P2";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.yellow;
						third_text.text = "P4";

					}


				}
				
			}
			else if (score4 >= score2 && score4 >= score3) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.yellow;
				second_text.text = "P4";
			
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score2 >= score3) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.blue;
						third_text.text = "P2";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.green;
						third_text.text = "P3";
					}

				}
			}

		}


		//In case Player 2 has the highest score
		else if (score2 >= score1 && score2 >= score3 && score2 >= score4) 
		{
			first.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
			Text first_text = first.GetComponentInChildren<Text> ();
			first_text.color = Color.blue;
			first_text.text = "P2";

			if (score1 >= score3 && score1 >= score4) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.red;
				second_text.text = "P1";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score3 >= score4) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.green;
						third_text.text = "P3";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.yellow;
						third_text.text = "P4";
					}

				}


			}

			else if (score3 >= score1 && score3 >= score4) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.grey;
				second_text.text = "P3";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score1 >= score4) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.red;
						third_text.text = "P1";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.yellow;
						third_text.text = "P4";
					}

				}

			}
			else if (score4 >= score2 && score4 >= score3) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.yellow;
				second_text.text = "P4";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score2 >= score3) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.blue;
						third_text.text = "P2";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.green;
						third_text.text = "P3";
					}

				}
			}

		}


		//In case Player 3 has the highest score
		else if (score3 >= score1 && score3 >= score2 && score3 >= score4) 
		{
			first.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
			Text first_text = first.GetComponentInChildren<Text> ();
			first_text.color = Color.green;
			first_text.text = "P3";

			if (score1 >= score3 && score1 >= score4) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.red;
				second_text.text = "P1";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score3 >= score4) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.green;
						third_text.text = "P3";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.yellow;
						third_text.text = "P4";
					}

				}


			}

			else if (score2 >= score1 && score2 >= score4) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.blue;
				second_text.text = "P2";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score1 >= score4) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.red;
						third_text.text = "P1";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.yellow;
						third_text.text = "P4";
					}

				}

			}
			else if (score4 >= score2 && score4 >= score1) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.yellow;
				second_text.text = "P4";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score2 >= score1) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.blue;
						third_text.text = "P2";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.red;
						third_text.text = "P1";
					}

				}
			}

		}

		//In case Player 4 has the highest score
		else if (score4 >= score1 && score4 >= score3 && score4 >= score2) 
		{
			first.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player4.skin);
			Text first_text = first.GetComponentInChildren<Text> ();
			first_text.color = Color.yellow;
			first_text.text = "P4";

			if (score1 >= score3 && score1 >= score2) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.red;
				second_text.text = "P1";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score3 >= score2) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.green;
						third_text.text = "P3";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.blue;
						third_text.text = "P2";
					}

				}


			}

			else if (score3 >= score1 && score3 >= score2) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.green;
				second_text.text = "P3";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score1 >= score2) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.red;
						third_text.text = "P1";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.blue;
						third_text.text = "P2";
					}

				}

			}
			else if (score2 >= score1 && score2 >= score3) 
			{
				second.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player2.skin);
				Text second_text = second.GetComponentInChildren<Text> ();
				second_text.color = Color.yellow;
				second_text.text = "P4";
				if (Controls_Manager.controls_manager.numberofplayers >= 3) 
				{
					third.SetActive (true);
					if (score1 >= score3) 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player1.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.red;
						third_text.text = "P1";

					} 
					else 
					{
						third.GetComponent<Animator> ().SetInteger ("Skin", Controls_Manager.controls_manager.Player3.skin);
						Text third_text = third.GetComponentInChildren<Text> ();
						third_text.color = Color.green;
						third_text.text = "P3";
					}

				}
			}

		}

		

	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape))
		{
			Destroy (Controls_Manager.controls_manager);
			Destroy (Music_Manager.music_manager);

			SceneManager.LoadScene ("menu");
		}
		if (Input.GetKeyDown (KeyCode.M)) 
		{
			Destroy (Controls_Manager.controls_manager);
			Music_Manager.music_manager.Song4.loop = true;
			Music_Manager.music_manager.Song4.Play ();

			SceneManager.LoadScene ("Mode Selection Screen");
		}
		if (Input.GetKeyDown (KeyCode.R)) 
		{
			Controls_Manager.controls_manager.Player1.score = 0;
			Controls_Manager.controls_manager.Player2.score = 0;
			Controls_Manager.controls_manager.Player3.score = 0;
			Controls_Manager.controls_manager.Player4.score = 0;
			Music_Manager.music_manager.Song4.loop = true;
			Music_Manager.music_manager.Song4.Play ();

			SceneManager.LoadScene ("Stage Selection Screen");

		}

	
	}


}
