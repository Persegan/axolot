﻿using UnityEngine;
using System.Collections;


public class Ship_Movement : MonoBehaviour {
	public float RotationSpeed;
	public float ThrustForce;
	public float freno;
	public ParticleSystem ThrustParticleEffect;
	public AudioSource SonidoMotor;
	public GameObject DisparoAjolote;
	public float maxSpeed;
	public GameObject Explosion_Jugador;
	public bool game_over_player = false;
	Animator animator;

	// Use this for initialization
	void Start () 
	{
		DisparoAjolote.GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "Foreground";
		ThrustParticleEffect.GetComponent<Renderer>().sortingLayerName = "Foreground";
		GetComponent<Rigidbody2D>().drag = freno;
		animator = gameObject.GetComponent<Animator> ();
		animator.SetBool("Movement", false);

	}
	void Update()
	{
		if (Input.GetButtonDown("P1 Fire"))
		{
			Instantiate (DisparoAjolote, transform.position, transform.rotation);
		}
	}
	

	void FixedUpdate()
	{
		if ((Input.GetAxisRaw ("P1 Rotate")) == -1) 
		{
			//rotar el ajolote a la izquierda
			GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;					
		}
		else if ((Input.GetAxisRaw ("P1 Rotate")) == 1) 
		{
			//rotar el ajolote a la derecha
			GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
		} 
		else
		{
			//parar la rotacion
			GetComponent<Rigidbody2D>().angularVelocity = 0f;

				
		}
		if (Input.GetButton ("P1 Move Forward"))
		{
			animator.SetBool("Movement", true);
			//engage the engine
			GetComponent<Rigidbody2D>().AddForce (transform.up*ThrustForce);
			//sistema de particulas :D
			ThrustParticleEffect.Emit(5);
			//Audio del motor
			if (SonidoMotor.isPlaying == false)
			{
			SonidoMotor.Play();
			}
		}
		else
		{
			animator.SetBool("Movement", false);
			//parar el sonido
			SonidoMotor.Stop ();
		}
		if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
		{
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
		}


	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "asteroide" )
		{
			Destroy (gameObject);
			Instantiate (Explosion_Jugador, transform.position, new Quaternion());
			GameObject.Find("Game_control_center").GetComponent<control_center>().game_over = true;



		}
		if (collider.gameObject.tag == "bala_mob" || collider.gameObject.tag == "mob")
		{
			Destroy (gameObject);
			Destroy (collider.gameObject);
			Instantiate (Explosion_Jugador, transform.position, new Quaternion());
			GameObject.Find("Game_control_center").GetComponent<control_center>().game_over = true;


		}

	}
	



}
