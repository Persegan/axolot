﻿using UnityEngine;
using System.Collections;

public class Goal_line : MonoBehaviour {
	public string team;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.gameObject.tag == "ball"){
			if (team == "red") {
				GetComponentInParent<soccer_field> ().BlueTeamScores ();
				//collider.gameObject.GetComponent<Rigidbody2D> ().AddForce (1000 * transform.right);
			} 
			else if (team == "blue") {
				GetComponentInParent<soccer_field> ().RedTeamScores ();
				//collider.gameObject.GetComponent<Rigidbody2D> ().AddForce (1000 * -transform.right);
				//print ("Red team scored!");
			} 
			else {
				Debug.Log ("Goal Line has not been initialized properly");
			}
		}
	}	
}
