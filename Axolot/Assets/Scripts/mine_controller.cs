﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class mine_controller : MonoBehaviour {

	public GameObject ship;
	public float number_of_mines;	// how many mines can be deployed per power up;
	public GameObject mine;			// the actual mine
	public Sprite icon;

	// Use this for initialization
	void Start () {
		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = number_of_mines.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire) && number_of_mines > 0) {
			GameObject temporal = Instantiate (mine, ship.transform.position, ship.transform.rotation) as GameObject;
			temporal.GetComponent<mine> ().ship = ship;
			number_of_mines--;
			ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = number_of_mines.ToString();
		} 
		else if (number_of_mines == 0) {
			ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
			Destroy(gameObject);
		}
	}

	void FixedUpdate()
	{

	}
}
