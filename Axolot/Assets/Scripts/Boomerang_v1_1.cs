﻿using UnityEngine;
using System.Collections;

public class Boomerang_v1_1 : MonoBehaviour {

	public GameObject ship;
	public float rotation_speed;    // how fast it spins
	public float speed;				// the speed at which it will travel
	public float returning_speed;	// the max speed that will reach when returning
	public float time_life;     	// it will return after that duartion
	private bool fired;				// indicates if it has been fired
	public bool destructable;    	// if true, the enemy can shoot it down
	private Vector3 direction;		// used to save the direction the player was facing when he fired it
	private float duration;   		// used to count how much time has elapsed it was fired
	//public float attraction_power;  // how strong the force will be pushing it back to the player
	public Transform movement_direction;
	
	private Vector3 v_diff;         // this is used for calculating the return direction
	private float atan2;			// this is used for calculating the return direction
	
	// Use this for initialization
	void Start () {
		if (time_life <= 0) 		// guarantees that time life will be at least 2 if you forget to set it
		{
			time_life = 2;
		}
		duration = time_life;
		fired = false;
		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		movement_direction.LookAt (ship.transform);

		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire) && fired == false) 
		{
			fired = true;
			direction = ship.transform.up;
		}
	}
	
	void FixedUpdate()
	{
		if (fired == false) {
			transform.position = ship.transform.position;
			transform.rotation = ship.transform.rotation;
		} 
		else 
		{	
			GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;
			
			if (duration > 0)							// if the duration is positive, it is still travelling forward
			{
				duration -= Time.fixedDeltaTime;
				GetComponent<Rigidbody2D>().AddForce (direction * speed);
				
			}
			else {     				// if the duration is negative, it will start returning to the player
				Vector3 distance = (transform.position - ship.transform.position);
				GetComponent<Rigidbody2D>().AddForce(movement_direction.forward * returning_speed / distance.sqrMagnitude );
			}			
		}
	}
	
	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject.name == ship.name  && duration <= 0)
		{
			Instantiate(gameObject, ship.transform.position, new Quaternion());    // for testing purposes, you get another one
			Destroy (gameObject);
		}
	}
}