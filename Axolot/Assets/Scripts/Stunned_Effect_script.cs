﻿using UnityEngine;
using System.Collections;

public class Stunned_Effect_script : MonoBehaviour {
	public float timebeforedestruction = 1.5f;
	public GameObject owner;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, timebeforedestruction);
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = new Vector2 (owner.transform.position.x, owner.transform.position.y+0.55f);
	}
}
