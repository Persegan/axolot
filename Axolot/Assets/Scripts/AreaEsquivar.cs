﻿using UnityEngine;
using System.Collections;

public class AreaEsquivar : MonoBehaviour
{
	public GameObject mob;
	public float cooldown;


	private float fuerza;
	private float cooldownprivate;
	private float angulo;
	// Use this for initialization
	void Start () 
	{
		cooldownprivate = -1f;
		fuerza = 50;
	}
	
	// Update is called once per frame
	void Update () 
	{
//		cooldownprivate -= Time.deltaTime;
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.gameObject.name != "Mob")
		{
			if (collider.gameObject.tag != "bala_mob")
			{
				if (cooldownprivate < 0f)
				{
				if (mob != null)
					{
						mob.GetComponent<Rigidbody2D>().AddForce (collider.GetComponent<Rigidbody2D>().velocity.normalized * fuerza);
						cooldownprivate = cooldown;
					}
				}
			}
		}

	}
}
