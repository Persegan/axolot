﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Directional_missile_v1_1 : MonoBehaviour {
	
	public GameObject ship;
	public GameObject explosion;
	public float rotation_speed;     // how fast the missile turns
	//public float angle;              // the angle at which the missile will start
	public float speed;		// the speed at which the missile will travel
	private bool fired;				// indicates if the missile has been fired
	public bool pin_player;        // if true, player will be unable to move while missile is traveling
	public bool detonatable;	// if true, the player can make  the missile explode at any time
	public bool destructable;    // if true, the enemy can shoot down the missile
	public float blast_radius;		// the area in which the missile will deal damage when explodes
	private bool exploded = false;
	public Sprite icon;
	
	// Use this for initialization
	void Start () {
		fired = false;
		transform.position = ship.transform.position;

		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = "";	// this is for the ammo UI
	}
	
	
	void Update()
	{
		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire)) 	// this gets the user's input for later use
		{
			if (fired == false)
			{
				fired = true;
			}
			else if (detonatable == true)
			{
				exploded = true;
			}
		}

	}
	
	void FixedUpdate () {
		if (fired == false) {
			transform.position = ship.transform.position;
			transform.rotation = ship.transform.rotation;			
		}
		else 
		{
			foreach (Collider2D coll in GetComponents<Collider2D>())
				coll.enabled = true;			// the power up can collide with thing only after it is fired
			if (pin_player == true)
			{
				ship.GetComponent<Ship_Movement_2Player>().set_disabled(true);     // the player is disabled while he is controlling the missile
			}
		
			GetComponent<Rigidbody2D>().AddForce (transform.up * speed);   // while the missile is flying, you can control it
			if (exploded == true && detonatable == true)
			{
				detonate ();
			}
			else if (Input.GetKey (ship.GetComponent<Ship_Movement_2Player>().p_rotate_left)) 
			{
				GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;					
			}
			else if (Input.GetKey (ship.GetComponent<Ship_Movement_2Player>().p_rotate_right))
			{
				GetComponent<Rigidbody2D>().angularVelocity = -rotation_speed;
			} 
            else if (ship.GetComponent<Ship_Movement_2Player>().player_number == 3)
            {
                if (Input.GetAxisRaw("joystick 1 x") == 1 || Input.GetAxisRaw("joystick 1 dx") == 1)
                {         
                    GetComponent<Rigidbody2D>().angularVelocity = -rotation_speed;
                }
                    else if (Input.GetAxisRaw("joystick 1 x") == -1 || Input.GetAxisRaw("joystick 1 dx") == -1)
                {
                    GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;
                }
                else
                { 
                    GetComponent<Rigidbody2D>().angularVelocity = 0f;
                }
            }
            else if (ship.GetComponent<Ship_Movement_2Player>().player_number == 4)
            {
                if (Input.GetAxisRaw("joystick 2 x") == 1 || Input.GetAxisRaw("joystick 2 dx") == 1)
                {
                    GetComponent<Rigidbody2D>().angularVelocity = -rotation_speed;
                }
                else if (Input.GetAxisRaw("joystick 2 x") == -1 || Input.GetAxisRaw("joystick 2 dx") == -1)
                {
                    GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;
                }
                else
                {
                    GetComponent<Rigidbody2D>().angularVelocity = 0f;
                }
            }
            else if (ship.GetComponent<Ship_Movement_2Player>().player_number == 5)
            {
                if (Input.GetAxisRaw("joystick 3 x") == 1 || Input.GetAxisRaw("joystick 3 dx") == 1)
                {
                    GetComponent<Rigidbody2D>().angularVelocity = -rotation_speed;
                }
                else if (Input.GetAxisRaw("joystick 3 x") == -1 || Input.GetAxisRaw("joystick 3 dx") == -1)
                {
                    GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;
                }
                else
                {
                    GetComponent<Rigidbody2D>().angularVelocity = 0f;
                }
            }
            else if (ship.GetComponent<Ship_Movement_2Player>().player_number == 6)
            {
                if (Input.GetAxisRaw("joystick 4 x") == 1 || Input.GetAxisRaw("joystick 4 dx") == 1)
                {
                    GetComponent<Rigidbody2D>().angularVelocity = -rotation_speed;
                }
                else if (Input.GetAxisRaw("joystick 4 x") == -1 || Input.GetAxisRaw("joystick 4 dx") == -1)
                {
                    GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;
                }
                else
                {
                    GetComponent<Rigidbody2D>().angularVelocity = 0f;
                }
            }
            else
			{
				GetComponent<Rigidbody2D>().angularVelocity = 0;
			}
			
		}
		
	}
	
	
	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject != ship && fired == true)		// can't collide with the player that threw it
		{
			if(collider.gameObject.tag == "jugador" || collider.gameObject.tag == "directional_missile" || collider.gameObject.tag == "mine" || collider.gameObject.tag == "bala_jugador" 
				|| collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "explosion" ||  collider.gameObject.tag == "terrain" || gameObject == collider.gameObject){		// it explodes normally when in contact with these

				detonate();
			}
			else if(collider.gameObject.tag == "satellite" || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "shield" || collider.gameObject.tag == "reflector")
			{	//this is just a small explosion, like the projectile gets destroyed

				if (collider.gameObject.tag == "shield") 
				{
					if (collider.gameObject.GetComponent<Shield> ().ship == ship) 
					{

					} 
					else 
					{
						ship.GetComponent<Ship_Movement_2Player> ().set_disabled (false);
						ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "reflector") {
					if (collider.gameObject.GetComponent<Reflector> ().ship == ship) {

					} else {
						ship.GetComponent<Ship_Movement_2Player> ().set_disabled (false);
						ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
						Destroy (gameObject);
					}
				} 
				else 
				{
					ship.GetComponent<Ship_Movement_2Player> ().set_disabled (false);
					ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
					Destroy (gameObject);
				}
		
			}
		}
	}


	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject != ship && fired == true)		// can't collide with the player that threw it
		{
			if(collider.gameObject.tag == "jugador" || collider.gameObject.tag == "directional_missile" || collider.gameObject.tag == "mine" || collider.gameObject.tag == "bala_jugador" 
				|| collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "explosion" || collider.gameObject.tag == "terrain" || gameObject == collider.gameObject){		// it explodes normally when in contact with these

				detonate();
			}
			else if(collider.gameObject.tag == "satellite" || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "shield" || collider.gameObject.tag == "reflector")
			{	//this is just a small explosion, like the projectile gets destroyed

				if (collider.gameObject.tag == "shield") 
				{
					if (collider.gameObject.GetComponent<Shield> ().ship == ship) 
					{

					} 
					else 
					{
						ship.GetComponent<Ship_Movement_2Player> ().set_disabled (false);
						ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
						Destroy (gameObject);
					}
				}
				else 
				{
					ship.GetComponent<Ship_Movement_2Player> ().set_disabled (false);
					ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
					Destroy (gameObject);
				}

			}
		}
	}


	void detonate(){
		GameObject temp = Instantiate (explosion, transform.position, new Quaternion ()) as GameObject;
		temp.GetComponent<missile_explosion> ().ship = ship;
		ship.GetComponent<Ship_Movement_2Player> ().set_disabled (false);
		//Instantiate(gameObject, ship.transform.position, new Quaternion());     // for testing purposes, you get another one
		ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
		ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
		Destroy (gameObject);
	}
}
