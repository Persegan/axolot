﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class UIManagerScript : MonoBehaviour {


	public Animator startbutton;
	public Animator settingsbutton;
	public Animator dialog;
	public Animator contentPanel;
	public Animator gearImage;
	public Animator twoplayerbutton;
	// Use this for initialization
	public void StartGame () {
		SceneManager.LoadScene("GetReady");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenSettings()
	{
		twoplayerbutton.SetBool ("isHidden", true);
		startbutton.SetBool ("isHidden", true);
		settingsbutton.SetBool ("isHidden", true);
		dialog.enabled = true;
		dialog.SetBool ("isHidden", false);
	}
	public void CloseSettings()
	{	
		twoplayerbutton.SetBool ("isHidden", false);
		startbutton.SetBool ("isHidden", false);
		settingsbutton.SetBool ("isHidden", false);
		dialog.SetBool ("isHidden", true);
	}

	public void ToggleMenu()
	{
		contentPanel.enabled = true;
		bool isHidden = contentPanel.GetBool ("isHidden");
		contentPanel.SetBool ("isHidden", !isHidden);
		gearImage.enabled = true;
		gearImage.SetBool ("isHidden", !isHidden);
	}
	void Start()
	{
		RectTransform transform = contentPanel.gameObject.transform as RectTransform;
		Vector2 position = transform.anchoredPosition;
		position.y -= transform.rect.height;
		transform.anchoredPosition = position;

	}
	public void Start2Player()
	{
		SceneManager.LoadScene ("2Players");
	}
	public void Quit()
	{
		Application.Quit();
	}


}
