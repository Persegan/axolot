﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent (typeof(AudioSource))]
public class Auto_Play_Movie : MonoBehaviour {

    public MovieTexture movie;
    private AudioSource Audio;

	// Use this for initialization
	void Start () {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        Audio = GetComponent<AudioSource>();
        movie.Play();
        Audio.Play();
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
