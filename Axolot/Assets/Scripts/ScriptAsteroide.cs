﻿using UnityEngine;
using System.Collections;

public class ScriptAsteroide : MonoBehaviour {

	public AsteroidType AsteroidType;
	public float MinTorque = -100f;
	public float MaxTorque = 100f;
	public float MinFuerza = 20f;
	public float MaxFuerza = 40f;
	public float maxSpeed = 3;
	public GameObject Explosion;
	public GameObject AsteroidesHijos;
	public int NumeroHijos;


	private float emissionRate = 0f;


	// Use this for initialization
	void Start () 
	{
		//movimiento del asteroide
		float magnitud = Random.Range (MinFuerza, MaxFuerza);
		float x = Random.Range(-1f, 1f);
		float y = Random.Range (-1f, 1f);
		//rotacion del asteroide
		GetComponent<Rigidbody2D>().AddForce (magnitud * new Vector2(x, y)); 
		float torque = Random.Range (MinTorque, MaxTorque);
		GetComponent<Rigidbody2D>().AddTorque (torque);
		GetComponent<Rigidbody2D>().angularDrag = 0.5f;

	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
		{
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{


		if (collider.tag == "bala_jugador")
			//Hace que la explosion cambie de tamaño en base al asteroide
		{
			GameObject.Find("Game_control_center").GetComponent<control_center>().puntuacion += 100;

			if (AsteroidType == AsteroidType.Large)
			{
				emissionRate = 200;
			}
			else if (AsteroidType == AsteroidType.Medium)
			{
				emissionRate = 50;
			}

			else
			{
				emissionRate = 25;
			}
			ParticleSystem ps = Explosion.GetComponent<ParticleSystem> ();
			var em = ps.emission;
			var rate = new ParticleSystem.MinMaxCurve();
			rate.constantMax = emissionRate;
			em.rate = rate;

			Instantiate (Explosion, transform.position, new Quaternion());
			Destroy (gameObject);
			Destroy (collider.gameObject);

			if (AsteroidesHijos != null)
			{
				for (int i = 0; i<NumeroHijos; i++)
				{
				Instantiate (AsteroidesHijos, transform.position, new Quaternion());
				}
			}
		}
		if (collider.tag == "Bala_jugador_1")
		{
			emissionRate = 200;
			ParticleSystem ps = Explosion.GetComponent<ParticleSystem> ();
			var em = ps.emission;
			var rate = new ParticleSystem.MinMaxCurve();
			rate.constantMax = emissionRate;
			em.rate = rate;
			Instantiate (Explosion, transform.position, new Quaternion());
			Destroy (gameObject);
			Destroy (collider.gameObject);
		}
		if (collider.tag == "Bala_jugador_2")
		{
			emissionRate = 200;
			ParticleSystem ps = Explosion.GetComponent<ParticleSystem> ();
			var em = ps.emission;
			var rate = new ParticleSystem.MinMaxCurve();
			rate.constantMax = emissionRate;
			em.rate = rate;
			Instantiate (Explosion, transform.position, new Quaternion());
			Destroy (gameObject);
			Destroy (collider.gameObject);
		}

	}
}


public enum AsteroidType
{
	Small,
	Medium,
	Large
}
