﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Shield : MonoBehaviour {

	public GameObject ship;
	public float duration;
	public Sprite icon;


	// Use this for initialization
	void Start () {
		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = "";	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		duration -= Time.deltaTime;

		if (duration < 0) {
			//ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			Destroy(gameObject);
		}
			
	}




	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject != ship) 
		{
			if(collider.gameObject.tag == "boomerang" || collider.gameObject.tag == "directional_missile" || collider.gameObject.tag == "mine" || collider.gameObject.tag == "satellite" || collider.gameObject.tag == "cluster_bomb"
				|| collider.gameObject.tag == "bala_jugador" ||  collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "explosion")
			{		// if it collides with any of these, it gets destroyed normaly
				if (collider.gameObject.tag == "boomerang") 
				{
					if (collider.gameObject.GetComponent<Boomerang> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "cluster_bomb") 
				{
					if (collider.gameObject.GetComponent<Cluster_bomb> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "directional_missile") 
				{
					if (collider.gameObject.GetComponent<Directional_missile_v1_1> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "satellite") 
				{
					if (collider.gameObject.GetComponent<satellite_powerup> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "mega_turbo_killer") 
				{
					if (collider.gameObject.GetComponent<mega_turbo_killer> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "explosion") 
				{
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);

				}
				if (collider.gameObject.tag == "bala_jugador") 
				{
					if (collider.gameObject.GetComponent<ScriptDisparo> ().ship != ship) 
					{
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject != ship) {
			if(collider.gameObject.tag == "boomerang" || collider.gameObject.tag == "directional_missile" || collider.gameObject.tag == "mine" || collider.gameObject.tag == "satellite" || collider.gameObject.tag == "cluster_bomb"
				|| collider.gameObject.tag == "bala_jugador" ||  collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "explosion")
			{		// if it collides with any of these, it gets destroyed normaly
				if (collider.gameObject.tag == "boomerang") 
				{
					if (collider.gameObject.GetComponent<Boomerang> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "cluster_bomb") 
				{
					if (collider.gameObject.GetComponent<Cluster_bomb> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "directional_missile") 
				{
					if (collider.gameObject.GetComponent<Directional_missile_v1_1> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "satellite") 
				{
					if (collider.gameObject.GetComponent<satellite_powerup> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "mega_turbo_killer") 
				{
					if (collider.gameObject.GetComponent<mega_turbo_killer> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
				if (collider.gameObject.tag == "explosion") 
				{
					
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);


				}
				if (collider.gameObject.tag == "bala_jugador") 
				{
					if (collider.gameObject.GetComponent<ScriptDisparo> ().ship != ship) {
						ship.GetComponent<Ship_Movement_2Player> ().invincibility ();
						Destroy (gameObject);
					}
				}
			}
		}
		

	}
}