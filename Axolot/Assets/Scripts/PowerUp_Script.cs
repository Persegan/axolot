﻿using UnityEngine;
using System.Collections;

public class PowerUp_Script : MonoBehaviour {
	public bool PowerUp_available;
	// Use this for initialization
	void Start () {
		PowerUp_available = true;
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		if (PowerUp_available == false)
		{
			Destroy (gameObject.GetComponent<BoxCollider2D>());
		}
	}
		
	void OnMouseDown()
	{
		print ("I was clicked " + transform.position);	
	}
}
