﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FreeForAll_ScoreTimer_Manager : MonoBehaviour {

	public Canvas canvas;

	public Text Timer;
	public float tiempo_actualizar;
	public float tiempo_restante_segundos;
	public float tiempo_restante_minutos;

	public float max_score = 3;

	public Text P1Score;
	public Text P2Score;
	public Text P3Score;
	public Text P4Score;

	void Awake()
	{
		if (Controls_Manager.controls_manager.Player1.ready == true)
		{

			P1Score.enabled = true;
		}
		if (Controls_Manager.controls_manager.Player2.ready == true)
		{
			P2Score.enabled = true;
		}
		if (Controls_Manager.controls_manager.Player3.ready == true)
		{
			P3Score.enabled = true;
		}
		if (Controls_Manager.controls_manager.Player4.ready == true)
		{
			P4Score.enabled = true;
		}
	}
	void Start () 
	{
		tiempo_actualizar = 0;
		tiempo_restante_segundos = 100;
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		//This manages the timer
		tiempo_actualizar -= Time.deltaTime;
		if (tiempo_restante_segundos == 0 && tiempo_restante_minutos == 0)
		{	
			SceneManager.LoadScene ("FreeForAll_EndGameScreen");
		}
		else if (tiempo_restante_segundos == 0)
		{
			tiempo_restante_minutos -= 1;
			tiempo_restante_segundos = 60;
		}
		if (tiempo_restante_segundos >  60)
		{
			tiempo_restante_minutos +=1;
			tiempo_restante_segundos = tiempo_restante_segundos-60;
		}
		if (tiempo_actualizar < 0)
		{
			tiempo_restante_segundos -= 1;
			Timer.text = tiempo_restante_minutos.ToString();
			Timer.text += ":";
			if (tiempo_restante_segundos <10)
			{
				Timer.text += "0";
				Timer.text += tiempo_restante_segundos.ToString();
			}
			else
			{
				Timer.text += tiempo_restante_segundos.ToString();
			}
			tiempo_actualizar = 1;
		}

		if (Controls_Manager.controls_manager.Player1.score == max_score || Controls_Manager.controls_manager.Player2.score == max_score || 
		    Controls_Manager.controls_manager.Player3.score == max_score || Controls_Manager.controls_manager.Player4.score == max_score)
		{
			SceneManager.LoadScene ("Free For All End Game Screen");
		}

		//This manages the score
		P1Score.text = "P1: " + Controls_Manager.controls_manager.Player1.score;
		P2Score.text = "P2: " + Controls_Manager.controls_manager.Player2.score;
		P3Score.text = "P3: " + Controls_Manager.controls_manager.Player3.score;
		P4Score.text = "P4: " + Controls_Manager.controls_manager.Player4.score;
			
	}
}
