﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class mega_turbo_killer : MonoBehaviour {

	public GameObject ship;
	private bool fired;				// indicates if it has been fired
	public float speed;				// the speed the axolot will move for the duration
	public float time_life;     	// the amount of time the power up will be active
	private float duration;   		// used to count how much time has elapsed sine the bomb was fired
	public Transform origin_point; // used to make a fixed position for the power up to be
	public Sprite icon;

	// Use this for initialization
	void Start () {
		GetComponent<SpriteRenderer> ().enabled = false;
		fired = false;
		duration = time_life;
		transform.position = origin_point.position;		// the origin_point is a child of the ship, so it follows it automatically, but it has a slightly smaller pos.y
		//transform.rotation = origin_point;
		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = "";
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire)) 
		{
			fired = true;
		}
	}


	void FixedUpdate()
	{
		if (fired == true && duration > 0) {
			transform.position = origin_point.position;
			transform.rotation = origin_point.transform.rotation;
			//transform.LookAt(origin_point.forward);
			GetComponent<SpriteRenderer> ().enabled = true;
			ship.GetComponent<Ship_Movement_2Player>().set_disabled(true);
			ship.GetComponent<Rigidbody2D>().angularVelocity = 0;
			ship.GetComponent<Rigidbody2D>().AddForce(ship.transform.up * speed);
			duration -= Time.fixedDeltaTime;
		} 
		else if( duration <= 0)
		{
			/*fired = false;
			duration = time_life; 		// for  testing purpsoses */
			GetComponent<SpriteRenderer> ().enabled = false;
			ship.GetComponent<Ship_Movement_2Player>().set_disabled(false);
			ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
			Destroy(gameObject);
		}
	}


	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject.name != ship.name && fired == true) 
		{
			// nothing happens probably
		}
	}
}
