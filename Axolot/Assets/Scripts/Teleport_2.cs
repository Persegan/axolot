﻿using UnityEngine;
using System.Collections;

public class Teleport_2 : MonoBehaviour {

	Vector3 stageDimensionsmax;
	Vector3 stageDimensionsmin;
	private float distanciaextra = 0.35f;
	
	void Start()
	{
		stageDimensionsmin = Camera.main.ScreenToWorldPoint (new Vector3(-0,-0,0));
		stageDimensionsmax = Camera.main.ScreenToWorldPoint (new Vector3 (Screen.width, Screen.height, 0));
		stageDimensionsmin.x -= distanciaextra;
		stageDimensionsmax.x += distanciaextra;
		stageDimensionsmin.y -= distanciaextra;
		stageDimensionsmax.y += distanciaextra;
		transform.position = new Vector2(GameObject.Find ("2Player_Ship1").transform.position.x + (stageDimensionsmax.x ), GameObject.Find ("2Player_Ship1").transform.position.y + (stageDimensionsmax.y ));
	}
	
	void FixedUpdate () 
	{
		float x = transform.position.x;
		float y = transform.position.y;

		if (x < stageDimensionsmin.x-(stageDimensionsmax.x/2))
		{
			x = stageDimensionsmax.x+(stageDimensionsmax.x/2);
		}
		
		else if ( x > stageDimensionsmax.x + (stageDimensionsmax.x/2))
		{
			x = stageDimensionsmin.x - (stageDimensionsmax.x/2);
		}
		
		if (y < stageDimensionsmin.y-(stageDimensionsmax.y/2))
		{
			y = stageDimensionsmax.y+(stageDimensionsmax.y/2);
		}
		
		else if ( y > stageDimensionsmax.y + (stageDimensionsmax.y/2))
		{
			y = stageDimensionsmin.y - (stageDimensionsmax.y/2);
		}
		transform.position = new Vector2 (x, y);;
	}
}
