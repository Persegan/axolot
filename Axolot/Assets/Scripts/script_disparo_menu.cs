﻿using UnityEngine;
using System.Collections;

public class script_disparo_menu : MonoBehaviour
{

	public float velocidadDisparo;
	public float TiempodeVida;
	
	
	void Start () 
	{
		
		Destroy (gameObject, TiempodeVida);
		GetComponent<Rigidbody2D>().AddForce (transform.right * velocidadDisparo);
	}
	
	// Update is called once per frame
	void Update () 
	{
		GetComponent<ParticleSystem>().Emit (5);
		
	}
}
