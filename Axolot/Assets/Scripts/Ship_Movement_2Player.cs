﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ship_Movement_2Player : MonoBehaviour {
	public float RotationSpeed;
	public float ThrustForce;
	public float TurboForce = 50;
	public float freno;
	public ParticleSystem ThrustParticleEffect;
	public ParticleSystem TurboParticleEffect;
	public AudioSource SonidoMotor;
	public AudioSource PowerUpSound;
	public AudioSource BeingHit;
	public float maxSpeed;
	public float maxSpeedTurbo = 5.5f;
	public GameObject Explosion_Jugador;
	public GameObject DisparoAjolote;
	public Text municion_texto;
	public Image arma;
	public Sprite Arma1;
	public Slider turbo;
	public Text PlayerIndicator;
	public GameObject stun_effect;
	public GameObject explosion_burbuja;
	public GameObject explosion_burbuja_stun;
	Animator animator;
	public GameObject boomerang;
	public GameObject cluster_bomb;
	public GameObject directional_missile;
	public GameObject mines;
	public GameObject satellites;
	public GameObject shotgun;
	public GameObject mega_turbo_killer;
	public GameObject mega_death_rocket;
	public GameObject shield;
	public GameObject reflector;
	public int score;


	public int PowerUp = 0;
	private int municion = 0;
	private GameObject Powerup_PickedUp;
	private bool turbo_bool = false;
	private bool stunned = false;
	private bool disabled = false;
	private bool fired = false;		// used to capture the fire button in the update function to minimize input loss
	private GameObject temp;
	Animator powerup_animator;

	public int tutorial_pu {
		get;
		set;
	}

	// Variables below are only used to differentiate P1 from P2 for testing the 1v1
	public int player_number;
	private KeyCode p_turbo;
	private KeyCode p_move_forward;

	/*public string p_bala_jugador{
		get;
		set;
	}not used for now*/
	
	public KeyCode p_rotate_right {
		get;
		set;
	}

	public KeyCode p_rotate_left {
		get;
		set;
	}
	
	public KeyCode p_fire {
		get;
		set;
	}

	
	// Use this for initialization
	void Start () 
	{
		ThrustParticleEffect.GetComponent<Renderer>().sortingLayerName = "Foreground";
		GetComponent<Rigidbody2D>().drag = freno;
		animator = gameObject.GetComponent<Animator> ();
		animator.SetBool("Movement", false);
		municion_texto = municion_texto.GetComponent <Text>();
		municion_texto.text = "";
		tutorial_pu = 0;

		// The code below is only used to differentiate P1 from P2 for testing the 1v1
		if (player_number == 1) {
			p_rotate_right = Controls_Manager.controls_manager.Right1;
			p_rotate_left = Controls_Manager.controls_manager.Left1;
			p_turbo = Controls_Manager.controls_manager.cancelturbo1;
			p_move_forward = Controls_Manager.controls_manager.Up1;
			p_fire = Controls_Manager.controls_manager.acceptfire1;
		}
		else if (player_number == 2) {
			p_rotate_right = Controls_Manager.controls_manager.Right2;
			p_rotate_left = Controls_Manager.controls_manager.Left2;
			p_turbo = Controls_Manager.controls_manager.cancelturbo2;
			p_move_forward = Controls_Manager.controls_manager.Up2;
			p_fire = Controls_Manager.controls_manager.acceptfire2;
		}
        else if (player_number == 3)
        {
            //p_rotate_right = Controls_Manager.controls_manager.Right2;
            //p_rotate_left = Controls_Manager.controls_manager.Left2;
            p_turbo = KeyCode.Joystick1Button2;
            p_move_forward = KeyCode.Joystick1Button5;
            p_fire = KeyCode.Joystick1Button0;
        }
        else if (player_number == 4)
        {
            //p_rotate_right = Controls_Manager.controls_manager.Right2;
            //p_rotate_left = Controls_Manager.controls_manager.Left2;
            p_turbo = KeyCode.Joystick2Button2;
            p_move_forward = KeyCode.Joystick2Button5;
            p_fire = KeyCode.Joystick2Button0;
        }
        else if (player_number == 5)
        {
            //p_rotate_right = Controls_Manager.controls_manager.Right2;
            //p_rotate_left = Controls_Manager.controls_manager.Left2;
            p_turbo = KeyCode.Joystick3Button2;
            p_move_forward = KeyCode.Joystick3Button5;
            p_fire = KeyCode.Joystick3Button0;
        }
        else if (player_number == 6)
        {
            //p_rotate_right = Controls_Manager.controls_manager.Right2;
            //p_rotate_left = Controls_Manager.controls_manager.Left2;
            p_turbo = KeyCode.Joystick4Button2;
            p_move_forward = KeyCode.Joystick4Button5;
            p_fire = KeyCode.Joystick4Button0;
        }
    }

	void Update()
	{
		if (Input.GetKeyDown (p_fire)) {
			fired = true;
		}
	}
	
	
	void FixedUpdate()
	{
		if (PowerUp == 0)
		{
			arma.enabled = false;
			municion_texto.text = "";
		}
		else if (PowerUp == 9)
		{
			arma.enabled = true;
			municion_texto.text = municion.ToString();
		}
		if (stunned == false && disabled == false)
		{
			if (Input.GetKey(p_rotate_left))
			{
				animator.SetBool("Rotating_left", true);
				animator.SetBool("Rotating_right", false);
				//rotar el ajolote a la izquierda
				GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;					
			}
			else if (Input.GetKey(p_rotate_right))
			{
				animator.SetBool("Rotating_right", true);
				animator.SetBool("Rotating_left", false);
				//rotar el ajolote a la derecha
				GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
			} 
			else
			{
				animator.SetBool("Rotating_right", false);
				animator.SetBool("Rotating_left", false);
				//parar la rotacion
				GetComponent<Rigidbody2D>().angularVelocity = 0f;		
			}

            switch(player_number)
            {
                case 3:
                    if (Input.GetAxisRaw("joystick 1 x") == 1 || Input.GetAxisRaw("joystick 1 dx") == 1)
                    {
                        animator.SetBool("Rotating_right", true);
                        animator.SetBool("Rotating_left", false);
                        //rotar el ajolote a la derecha
                        GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
                    }
                    else if (Input.GetAxisRaw("joystick 1 x") == -1 || Input.GetAxisRaw("joystick 1 dx") == -1)
                    {
                        animator.SetBool("Rotating_left", true);
                        animator.SetBool("Rotating_right", false);
                        //rotar el ajolote a la izquierda
                        GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;
                    }
                    else
                    {
                        animator.SetBool("Rotating_right", false);
                        animator.SetBool("Rotating_left", false);
                        //parar la rotacion
                        GetComponent<Rigidbody2D>().angularVelocity = 0f;
                    }
                    break;

                case 4:
                    if (Input.GetAxisRaw("joystick 2 x") == 1 || Input.GetAxisRaw("joystick 2 dx") == 1)
                    {
                        animator.SetBool("Rotating_right", true);
                        animator.SetBool("Rotating_left", false);
                        //rotar el ajolote a la derecha
                        GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
                    }
                    else if (Input.GetAxisRaw("joystick 2 x") == -1 || Input.GetAxisRaw("joystick 2 dx") == -1)
                    {
                        animator.SetBool("Rotating_left", true);
                        animator.SetBool("Rotating_right", false);
                        //rotar el ajolote a la izquierda
                        GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;
                    }
                    else
                    {
                        animator.SetBool("Rotating_right", false);
                        animator.SetBool("Rotating_left", false);
                        //parar la rotacion
                        GetComponent<Rigidbody2D>().angularVelocity = 0f;
                    }

                    break;

                case 5:
                    if (Input.GetAxisRaw("joystick 3 x") == 1 || Input.GetAxisRaw("joystick 3 dx") == 1)
                    {
                        animator.SetBool("Rotating_right", true);
                        animator.SetBool("Rotating_left", false);
                        //rotar el ajolote a la derecha
                        GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
                    }
                    else if (Input.GetAxisRaw("joystick 3 x") == -1 || Input.GetAxisRaw("joystick 3 dx") == -1)
                    {
                        animator.SetBool("Rotating_left", true);
                        animator.SetBool("Rotating_right", false);
                        //rotar el ajolote a la izquierda
                        GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;
                    }
                    else
                    {
                        animator.SetBool("Rotating_right", false);
                        animator.SetBool("Rotating_left", false);
                        //parar la rotacion
                        GetComponent<Rigidbody2D>().angularVelocity = 0f;
                    }

                    break;

                case 6:
                    if (Input.GetAxisRaw("joystick 4 x") == 1 || Input.GetAxisRaw("joystick 4 dx") == 1)
                    {
                        animator.SetBool("Rotating_right", true);
                        animator.SetBool("Rotating_left", false);
                        //rotar el ajolote a la derecha
                        GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
                    }
                    else if (Input.GetAxisRaw("joystick 4 x") == -1 || Input.GetAxisRaw("joystick 4 dx") == -1)
                    {
                        animator.SetBool("Rotating_left", true);
                        animator.SetBool("Rotating_right", false);
                        //rotar el ajolote a la izquierda
                        GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;
                    }
                    else
                    {
                        animator.SetBool("Rotating_right", false);
                        animator.SetBool("Rotating_left", false);
                        //parar la rotacion
                        GetComponent<Rigidbody2D>().angularVelocity = 0f;
                    }

                    break;
            }

			if (Input.GetKey(p_turbo))
			{
				if (turbo.value > 0)
				{
					turbo.value = turbo.value - 3f;
					turbo_bool = true;
					animator.SetBool("Movement", true);
					//engage the engine
					GetComponent<Rigidbody2D>().AddForce (transform.up*TurboForce);
					//sistema de particulas :D
					TurboParticleEffect.Emit(15);
					//Audio del motor
					if (SonidoMotor.isPlaying == false)
					{
						SonidoMotor.Play();
					}
				}
				else
				{
					animator.SetBool("Movement", false);
					//parar el sonido
					SonidoMotor.Stop ();
				}
			}
			else 
			{
				turbo_bool = false;
				turbo.value = turbo.value + 0.5f;
			}
			/*if (Input.GetButtonDown ("P1 Turbo"))
			{
				turbo_bool = true;
			}
			if (Input.GetButtonUp ("P1 Turbo"))
			{
				turbo_bool = false;
			}*/
			if (Input.GetKey(p_move_forward))
			{
				if (turbo_bool == false)
				{
					animator.SetBool("Movement", true);
					//engage the engine
					GetComponent<Rigidbody2D>().AddForce (transform.up*ThrustForce);
					//GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
					//sistema de particulas :D
					ThrustParticleEffect.Emit(5);
					//Audio del motor
					if (SonidoMotor.isPlaying == false)
					{
						SonidoMotor.Play();
					}
				}
			}
			else if (turbo_bool == false)
			{
				animator.SetBool("Movement", false);
				//parar el sonido
				SonidoMotor.Stop ();
			}
			if (turbo_bool == false)
			{
				if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
				{
					
					GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
				}
			}
			
			if (turbo_bool == true)
			{
				if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeedTurbo)
				{					
					GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeedTurbo;
				}
				
			}
			if (fired == true)
			{
				if (PowerUp == 9)
				{
					temp = Instantiate (DisparoAjolote, transform.position, transform.rotation) as GameObject;
					temp.GetComponent<ScriptDisparo>().ship = gameObject;		// this sets the bullet owner. why? none of your business #MC2015
					municion -= 1;

					if (municion == 0)
					{
						PowerUp = 0;
						tutorial_pu = 0;	// for the tutorial only
					}
				}
				fired = false;		// reset that the player hasnt fired

			}
		}
		else if (stunned == true)
		{
			StartCoroutine(GetStunned());
		}		
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "asteroide") {		// this is the interaction with the bubbles
			if (turbo_bool == false && collider.gameObject.GetComponent<Script_Burbuja> ().can_stun == false) {
				GetComponent<Rigidbody2D> ().AddForce (1000 * new Vector2 ((transform.position.x - collider.gameObject.transform.position.x), (transform.position.y - collider.gameObject.transform.position.y)));	
				Instantiate (explosion_burbuja, collider.gameObject.transform.position, new Quaternion ());
				Destroy (collider.gameObject);
			} else if (turbo_bool == true && collider.gameObject.GetComponent<Script_Burbuja> ().can_stun == false) {
				collider.gameObject.GetComponent<Script_Burbuja> ().maxSpeed = 4;
				GetComponent<Rigidbody2D> ().AddForce (500 * new Vector2 ((transform.position.x - collider.gameObject.transform.position.x), (transform.position.y - collider.gameObject.transform.position.y)));
				collider.gameObject.GetComponent<Rigidbody2D> ().AddForce (900 * new Vector2 ((collider.gameObject.transform.position.x - transform.position.x), (collider.gameObject.transform.position.y - transform.position.y)));
				collider.gameObject.GetComponent<Script_Burbuja> ().can_stun = true;
			} else if (collider.gameObject.GetComponent<Script_Burbuja> ().can_stun == true) {
				GetComponent<Rigidbody2D> ().AddForce (350 * new Vector2 ((transform.position.x - collider.gameObject.transform.position.x), (transform.position.y - collider.gameObject.transform.position.y)));
				Instantiate (explosion_burbuja_stun, collider.gameObject.transform.position, new Quaternion ());
				Destroy (collider.gameObject);			// the bubble is destriyed fomr here instead in it's own collison function
				GameObject stunned_effect;
				stunned_effect = Instantiate (stun_effect);
				stunned_effect.GetComponent<Stunned_Effect_script> ().owner = gameObject;
				stunned = true;
			}

		} else if (collider.gameObject.tag == "PowerUp") {		// this is the interaction with the power ups when you pick them up
			if (PowerUp == 0) {
				Powerup_PickedUp = collider.gameObject;
				StartCoroutine (PowerUpPickUp ());				
			}
		} else if (collider.gameObject.tag == "boomerang" || collider.gameObject.tag == "directional_missile" || collider.gameObject.tag == "satellite"
		         || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "explosion" || collider.gameObject.tag == "bala_jugador" || collider.gameObject.tag == "mega_death_rocket") {
			if (collider.gameObject != temp) {
				if (collider.gameObject.tag == "boomerang") {
					collider.gameObject.GetComponent<Boomerang> ().ship.GetComponent<Ship_Movement_2Player> ().score += 1;
					StartCoroutine (Blinkafterimpact ());
				} else if (collider.gameObject.tag == "directional_missile") {
					collider.gameObject.GetComponent<Directional_missile_v1_1> ().ship.GetComponent<Ship_Movement_2Player> ().score += 1;
					StartCoroutine (Blinkafterimpact ());
				} else if (collider.gameObject.tag == "satellite") {
					if (collider.gameObject.GetComponent<satellite_powerup> ().ship != gameObject) {
						collider.gameObject.GetComponent<satellite_powerup> ().ship.GetComponent<Ship_Movement_2Player> ().score += 1;
						StartCoroutine (Blinkafterimpact ());
					}
				} else if (collider.gameObject.tag == "mega_turbo_killer") {
					collider.gameObject.GetComponent<mega_turbo_killer> ().ship.GetComponent<Ship_Movement_2Player> ().score += 1;
					StartCoroutine (Blinkafterimpact ());
				} else if (collider.gameObject.tag == "explosion") {
					if (collider.gameObject.GetComponent<missile_explosion> ().ship == gameObject) {
						gameObject.GetComponent<Ship_Movement_2Player> ().score -= 1;
						StartCoroutine (Blinkafterimpact ());

					} else {
						collider.gameObject.GetComponent<missile_explosion> ().ship.GetComponent<Ship_Movement_2Player> ().score += 1;
						StartCoroutine (Blinkafterimpact ());
					}

				} else if (collider.gameObject.tag == "bala_jugador") {
					if (collider.gameObject.GetComponent<ScriptDisparo> ().ship != gameObject) {
						collider.gameObject.GetComponent<ScriptDisparo> ().ship.GetComponent<Ship_Movement_2Player> ().score += 1;
						StartCoroutine (Blinkafterimpact ());
					}
				} else if (collider.gameObject.tag == "mega_death_rocket") {
					if (collider.gameObject.GetComponent<Mega_death_rocket> ().ship != gameObject) {
						collider.gameObject.GetComponent<Mega_death_rocket> ().ship.GetComponent<Ship_Movement_2Player> ().score += 1;
						StartCoroutine (Blinkafterimpact ());
					}
				}
			}
		}/* this is irrelevant since I added another collider to the player to handle the interaction with ball
			else if (collider.gameObject.tag == "ball") {		// this is the interaction with the ball in soccer mode
			float temp = Vector2.Dot (GetComponent<Rigidbody2D> ().velocity, collider.gameObject.GetComponent<Rigidbody2D> ().velocity); //this tells us the relative motion between the player and the ball ranging from -1 to 1

			if (GetComponent<Rigidbody2D> ().velocity.magnitude > 0) {	// if the player is moving, he pushes the ball with a force relative to his velocity and the balls velocity	
				collider.GetComponent<Rigidbody2D> ().AddForce (-250 * GetComponent<Rigidbody2D> ().velocity.magnitude * new Vector2 ((transform.position.x - collider.gameObject.transform.position.x), (transform.position.y - collider.gameObject.transform.position.y)).normalized);
			} 
			else {		// if the player is not moving then the ball wil bounce off of him with a force relative to the balls velocity
				collider.GetComponent<Rigidbody2D> ().AddForce (-250 * collider.gameObject.GetComponent<Rigidbody2D> ().velocity.magnitude * new Vector2 ((transform.position.x - collider.gameObject.transform.position.x), (transform.position.y - collider.gameObject.transform.position.y)).normalized);
			}
		}*/

        else if (collider.gameObject.tag == "Tutorial_Powerup")
        {
            Powerup_PickedUp = collider.gameObject;
            municion = 3;
            arma.GetComponent<Image>().sprite = Arma1;
            PowerUp = 9;
            powerup_animator = Powerup_PickedUp.GetComponent<Animator>();
            Powerup_PickedUp.GetComponent<PowerUp_Script>().PowerUp_available = false;
            powerup_animator.SetBool("pick_up", true);
            Destroy(Powerup_PickedUp, 0.45f);
            PowerUpSound.Play();
        }

    }


	void OnTriggerStay2D(Collider2D collider){		// this helps pick up power ups that you are already standing on
		if (collider.gameObject.tag == "PowerUp")		// this is the interaction with the power ups when you pick them up
		{
			if (PowerUp == 0)
			{
				Powerup_PickedUp = collider.gameObject;
				StartCoroutine(PowerUpPickUp());				
			}
		}
	}


	public void set_disabled(bool status)		// this is just so we can enable/disable the controls from other scripts properly
	{
		disabled = status;
	}

	
	public IEnumerator PowerUpPickUp()
	{	//this handles the assignment of a power up that is picked up
		//when the player gets a power up, one will be randomly assigned to him here

		PowerUp = Random.Range (1, 12);
		if (tutorial_pu == 0) {			// this is not usufull for the tutorial
			powerup_animator = Powerup_PickedUp.GetComponent<Animator> ();
			Powerup_PickedUp.GetComponent<PowerUp_Script> ().PowerUp_available = false;
			powerup_animator.SetBool ("pick_up", true);
			Destroy (Powerup_PickedUp, 0.45f);
		}
		PowerUpSound.Play ();

		if (tutorial_pu > 0) 	// these 2 lines are only usufull for aquiring the correct power up from the tutorial 
			PowerUp = tutorial_pu;
		
		print (tutorial_pu);
		
		// this is for testing purposes
		/*if (player_number == 1) {
			PowerUp = 2;
		} else if (player_number == 2) {
			PowerUp = 10;
		}
			*/
		// this is for testing purposes

		switch (PowerUp) {
		case 1: 								//1. boomerang
			boomerang.GetComponent<Boomerang>().ship = gameObject;
			arma.GetComponent<Image>().sprite = boomerang.GetComponent<Boomerang>().icon;
			temp = Instantiate(boomerang);
			break;
		case 2: 								//2. cluster bomb
			cluster_bomb.GetComponent<Cluster_bomb>().ship = gameObject;
			arma.GetComponent<Image>().sprite = cluster_bomb.GetComponent<Cluster_bomb>().icon;
			temp = Instantiate(cluster_bomb);
			break;
		case 3: 								//3. directional missile
			directional_missile.GetComponent<Directional_missile_v1_1>().ship = gameObject;
			arma.GetComponent<Image>().sprite = directional_missile.GetComponent<Directional_missile_v1_1>().icon;
			temp = Instantiate(directional_missile);
			break;
		case 4: 								//4. mines
			mines.GetComponent<mine_controller>().ship = gameObject;
			arma.GetComponent<Image>().sprite = mines.GetComponent<mine_controller>().icon;
			temp = Instantiate(mines);
			break;
		case 5: 								//5. satelites
			satellites.GetComponent<satellite_controller>().ship = gameObject;
			arma.GetComponent<Image>().sprite = satellites.GetComponent<satellite_controller>().icon;
			temp = Instantiate(satellites);
			satellites.GetComponent<satellite_controller>().satellite.GetComponent<satellite_powerup>().controller = temp;
			break;
		case 6: 								//6. shotgun
			shotgun.GetComponent<Shotgun>().ship = gameObject;
			arma.GetComponent<Image>().sprite = shotgun.GetComponent<Shotgun>().icon;
			temp = Instantiate(shotgun);
			break;
		case 7: 								//7. mega turbo killer
			mega_turbo_killer.GetComponent<mega_turbo_killer>().ship = gameObject;
			mega_turbo_killer.GetComponent<mega_turbo_killer>().origin_point = gameObject.transform.GetChild(4); // this is going to break if the ships childs are moved
			arma.GetComponent<Image>().sprite = mega_turbo_killer.GetComponent<mega_turbo_killer>().icon;
			temp = Instantiate(mega_turbo_killer);
			break;
		case 8: 								//8. mega death rocket
			mega_death_rocket.GetComponent<Mega_death_rocket>().ship = gameObject;
			arma.GetComponent<Image>().sprite = mega_death_rocket.GetComponent<Mega_death_rocket>().icon;
			temp = Instantiate(mega_death_rocket);
			break;
		case 9: 								//9. normal projectiles
			municion = 3;
			arma.GetComponent<Image> ().sprite = Arma1;
			break;
		case 10:								//10. shield
			shield.GetComponent<Shield> ().ship = gameObject;
			arma.GetComponent<Image> ().sprite = shield.GetComponent<Shield> ().icon;
			temp = Instantiate (shield);
			PowerUp = 0;
			tutorial_pu = 0;	// for the tutorial only
			break;
		case 11:								//11. reflector, reflects bullets, destroys one (or more) bigger projectiles
			reflector.GetComponent<Reflector> ().ship = gameObject;
			reflector.GetComponent<Reflector>().origin_point = gameObject.transform.GetChild(5);
			arma.GetComponent<Image> ().sprite = reflector.GetComponent<Reflector> ().icon;
			temp = Instantiate (reflector);
			break;
		default:
			break;
		}
		arma.enabled = true;						// this activates the UI elements for the ammo and icon
		yield return new WaitForSeconds(0.0f);
	}


	IEnumerator GetStunned()	// this stuns the player through a collision with a red bubble
	{
		animator.SetBool("Movement", false);
		//parar el sonido
		SonidoMotor.Stop ();
		yield return new WaitForSeconds (1.5f);
		stunned = false;
	}

	IEnumerator Blinkafterimpact()
	{
		foreach(Collider2D c in GetComponents<BoxCollider2D> ())
		{

			c.enabled = false;
		}
		BeingHit.Play ();
		gameObject.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f, 0f);

		gameObject.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f, 0.25f);
		yield return new WaitForSeconds (0.5f);
		gameObject.GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 1f);
		yield return new WaitForSeconds (0.5f);
		gameObject.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f, 0.25f);
		yield return new WaitForSeconds (0.5f);
		gameObject.GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 1f);
		yield return new WaitForSeconds (0.5f);
		gameObject.GetComponent<SpriteRenderer>().color = new Color(1f,1f,1f, 0.25f);
		yield return new WaitForSeconds (0.5f);
		gameObject.GetComponent<SpriteRenderer>().color = new Color (1f, 1f, 1f, 1f);
		yield return new WaitForSeconds (0.5f);
	
		foreach(Collider2D c in GetComponents<BoxCollider2D> ())
		{
			
			c.enabled = true;
		}


	}

	public void invincibility(){
		StartCoroutine (Blinkafterimpact ());
	}
	
	void OnDisable() {			// clean up code for when the ship gets destroyed
		arma.enabled = false;
		turbo.enabled = false;
		Destroy (temp);
	}
}
