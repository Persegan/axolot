﻿using UnityEngine;
using System.Collections;

public class Music_Manager : MonoBehaviour {
	public static Music_Manager music_manager;

	public AudioSource Song4;
	public AudioSource Song3;
	public AudioSource Song2;
	public AudioSource SongMenu;


	void Awake()
	{
		if (music_manager == null)
		{
			DontDestroyOnLoad (gameObject);
			music_manager = this;
		}

		else if (music_manager != this)
		{
			Destroy(gameObject);
		}

	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
