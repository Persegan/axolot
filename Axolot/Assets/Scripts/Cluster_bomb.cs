using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Cluster_bomb : MonoBehaviour {

	public GameObject ship;
	public GameObject explosion;
	public float rotation_speed;    // how fast the bomb spins
	public float speed;				// the speed at which the bomb will travel
	public float time_life;     	// if the time is not 0, the bomb will explode after that duartion
	private bool fired;				// indicates if the bomb has been fired
	public bool detonatable;		// if true, the player can make the bomb explode at any time
	public bool destructable;    	// if true, the enemy can shoot down the bomb
	//public float blast_radius;		// the range in which the clusters will travel when the bomb explodes
	private bool exploded;			// if true, the bomb has exploded
	private Vector3 direction;		//used for applying a force behind the bomb so it keeps moving straigth while rotating
	private float duration;   		// used to count how much time has elapsed sine the bomb was fired
	public Sprite icon;

	// Use this for initialization
	void Start () {
		fired = false;
		exploded = false;
		duration = time_life;
		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;

		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = "";  	// this is for the ammo UI
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire)) {
			if (fired == false) {
				fired = true;
				direction = ship.transform.up;			// this saves the direction the player is looking while firing, so the force to the bomb is applied correctly
			} else if (detonatable == true) {
				exploded = true;
				fired = false;
			}
		}
	}

	void FixedUpdate () {
		if (fired == false && exploded == false) 	// before you fire it
		{
			transform.position = ship.transform.position;
			transform.rotation = ship.transform.rotation;			
		}
		else 
		{	
			foreach (Collider2D coll in GetComponents<Collider2D>())
				coll.enabled = true;				// the power up can collide with things only after it is fired
			if (duration > 0)
			{
				duration -= Time.fixedDeltaTime;
			}
			else if (duration < 0)
			{
				exploded = true;
			}
			GetComponent<Rigidbody2D>().AddForce (direction * speed);   // the bomb will move to where the player is looking when firing
			if (rotation_speed > 0)
			{
				GetComponent<Rigidbody2D>().angularVelocity = rotation_speed;
			}
			if (exploded == true && detonatable == true)
			{
				detonate ();
			}			
		}
		
	}
	
	
	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject.name != ship.name  && (fired == true || exploded == true))		// can't collide with the player that threw it
		{
			if(collider.gameObject.tag == "mine" || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "explosion" ||  collider.gameObject.tag == "shield" 
				||  collider.gameObject.tag == "terrain"){	// it will explode if it contacts these
				detonate();
			}
			speed = 0;					// these two lines make it so the bomb stops trying to go to the initial direction and can bounce freely
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.name != ship.name  && (fired == true || exploded == true))		// can't collide with the player that threw it
		{
			if (collider.gameObject.tag == "mine" || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "explosion"
			   || collider.gameObject.tag == "terrain") {	// it will explode if it contacts these
				detonate ();
				speed = 0;		// this makes the bomb move freely after colliding
			} 
			else if (collider.gameObject.tag == "shield") {		// this is a special case for the trigger function so the cluster bomb doest detonate in your own shiled
				if(ship != collider.gameObject.GetComponent<Shield>().ship)
				{
					detonate ();
					speed = 0;		// this makes the bomb move freely after colliding
				}
			}
		}
	}

	void detonate(){		// used to detonate projectiles manually and when colliding with other things
		GameObject temp = Instantiate (explosion, transform.position, new Quaternion ()) as GameObject;
		temp.GetComponent<Cluster_explosion>().ship = ship;	// this passes the game owner to the cluster that will produce the bullets
		ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
		ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
		Destroy (gameObject);
	}
}
