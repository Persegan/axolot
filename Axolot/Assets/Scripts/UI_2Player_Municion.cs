﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI_2Player_Municion : MonoBehaviour {


	public GameObject Owner;
	public Image Arma;
	public Text municion;
	public Slider turbo;
	public Text PlayerIndicator;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	void FixedUpdate()
	{
		if (Owner != null)
		{
		Arma.rectTransform.position = new Vector2 (Owner.transform.position.x+0.5f, Owner.transform.position.y-0.6f);
		municion.rectTransform.position = new Vector2 (Owner.transform.position.x+0.6f, Owner.transform.position.y-0.6f);
		turbo.transform.position = new Vector2 (Owner.transform.position.x + 0.5f, Owner.transform.position.y);
		PlayerIndicator.rectTransform.position = new Vector2 (Owner.transform.position.x, Owner.transform.position.y + 0.6f);
		}

	}
}
