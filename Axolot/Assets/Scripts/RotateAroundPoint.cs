﻿using UnityEngine;
using System.Collections;

public class RotateAroundPoint : MonoBehaviour {
	public Transform point;	   
	public float rotationSpeed;    // how fast they  go
	public float rotationDistance; // how far away from the point they are
	public float angle;            // the angle at which will start

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (point.position.x + Mathf.Cos (angle * Mathf.Deg2Rad) * rotationDistance, point.position.y + Mathf.Sin (angle * Mathf.Deg2Rad) * rotationDistance);
		angle = angle%360 + rotationSpeed;
	}
}
