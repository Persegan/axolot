﻿using UnityEngine;
using System.Collections;

public class ScriptDisparo : MonoBehaviour 
{
	public float velocidadDisparo;
	public float TiempodeVida;

	public GameObject ship;

	/*
	public int owner {		// used to avoid killing the player with his own bullets
		get;				// should be initialized after firing any bullet by the script that is producing them
		set;
	}
*/

	void Start () 
	{

		Destroy (gameObject, TiempodeVida);
		GetComponent<Rigidbody2D>().AddForce (transform.up * velocidadDisparo);
	}
	
	// Update is called once per frame
	void Update () 
	{
		GetComponent<ParticleSystem>().Emit (5);
		
	}

	void OnCollisionEnter2D(Collision2D collider)		
	{
		if (collider.gameObject != ship)		// bullets can't kill their owners //future comment: this is wrong
		{
			if(collider.gameObject.tag == "directional_missile" || collider.gameObject.tag == "mine" || collider.gameObject.tag == "satellite" || collider.gameObject.tag == "terrain"
				||  collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "shield"|| collider.gameObject.tag == "asteroide"){		// if it collides with any of these, it gets destroyed
				if (collider.gameObject.tag == "shield") {
					if (collider.gameObject.GetComponent<Shield> ().ship == ship) {

					} else {				
						Destroy (gameObject);
					}
				} else {
					Destroy (gameObject);
				}
			}
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject != ship) {		// bullets can't kill their owners //future comment: this is wrong
			if (collider.gameObject.tag == "directional_missile" || collider.gameObject.tag == "mine" || collider.gameObject.tag == "satellite" || collider.gameObject.tag == "terrain"
			   || collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "shield" || collider.gameObject.tag == "asteroide") {		// if it collides with any of these, it gets destroyed
				if (collider.gameObject.tag == "shield") {
					if (collider.gameObject.GetComponent<Shield> ().ship == ship) {
					} else {

						Destroy (gameObject);
					}
				} else {
					Destroy (gameObject);
				}
			}
		}
	}
}
