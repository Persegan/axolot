﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class UI_manager : MonoBehaviour {
	public Button play;
	public Button options;
	public Button extras;
	public Button exit;
	public Button play_playonline;
	public Button play_playoffline;
	public Button play_howtoplay;
	public Button play_back;
	public Button options_sound;
	public Button options_controls;
	public Button options_back;
	public Button extras_achievements;
	public Button extras_unlockables;
	public Button extras_back;
	public Button options_controls_return;
	public Image options_controls_image;
	public Image howtoplay1;
	public Image howtoplay2;
	public Image howtoplay3;
	public Image howtoplay4;
	public Image howtoplay5;
	public Image howtoplay6;
	public Button howtoplaybutton1;
	public Button howtoplaybutton2;
	public Button howtoplaybutton3;
	public Button howtoplaybutton4;
	public Button howtoplaybutton5;
	public Button howtoplaybutton6;
	public AudioSource select;
	public GameObject laser;

	private bool menu_play = false;
	private bool menu_options = false;
	private bool menu_extras = false;
	private bool controls = false;
	private bool howtoplaybool1 = false;
	private bool howtoplaybool2 = false;
	private bool howtoplaybool3 = false;
	private bool howtoplaybool4 = false;
	private bool howtoplaybool5 = false;
	private bool howtoplaybool6 = false;
	// Use this for initialization
	void Start () 
	{
		Cursor.visible = false;
		play.Select ();
		options_controls_image.GetComponent<Image> ().enabled = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		Cursor.lockState = CursorLockMode.Locked;
	}

	public void ClickOutside()
	{
		if (menu_play == true)
		{
			play_playonline.Select ();
		}
		else if (menu_options == true)
		{
			options_sound.Select ();
		}
		else if (menu_extras == true)
		{
			extras_achievements.Select ();
		}
		else if (controls == true)
		{
			options_controls_return.Select();
		}
		else if (howtoplaybool1 == true)
		{
			howtoplaybutton1.Select ();
		}
		else if (howtoplaybool2 == true)
		{
			howtoplaybutton2.Select();
		}
		else if (howtoplaybool3 == true)
		{
			howtoplaybutton3.Select ();
		}
		else if (howtoplaybool4 == true)
		{
			howtoplaybutton4.Select ();
		}
		else if (howtoplaybool5 == true)
		{
			howtoplaybutton5.Select ();
		}
		else if (howtoplaybool6 == true)
		{
			howtoplaybutton6.Select ();
		}
		else
		{
		play.Select ();
		}

	}

	public void Play_sound()
	{
		select.Play ();
	}

	public void SubmitPlay()
	{

		play_playonline.interactable = true;
		menu_play = true;
		play_playoffline.interactable = true;
		play_howtoplay.interactable = true;
		play_back.interactable = true;
		Instantiate (laser, play.transform.position, play.transform.rotation);
		play_playonline.Select ();	
		options.interactable = false;
		extras.interactable = false;
		exit.interactable = false;
		play.interactable = false;

	}

	public void CancelPlay1()
	{
		play_back.Select ();
	}

	public void CancelPlay2()
	{
		play_playonline.interactable = false;
		menu_play = false;
		play_playoffline.interactable = false;
		play_howtoplay.interactable = false;
		play_back.interactable = false;
		Instantiate (laser, play_back.transform.position, play_back.transform.rotation);
		play.Select ();	
		options.interactable = true;
		extras.interactable = true;
		exit.interactable = true;
		play.interactable = true;

	}

	public void SubmitOptions()
	{
		options_back.interactable = true;
		options_controls.interactable = true;
		options_sound.interactable = true;
		Instantiate (laser, options.transform.position, options.transform.rotation);
		menu_options = true;
		options.interactable = false;
		extras.interactable = false;
		exit.interactable = false;
		play.interactable = false;
		options_sound.Select ();

	}

	public void CancelOptions1()
	{
		options_back.Select ();
	}

	public void CancelOptions2()
	{
		options_back.interactable = false;
		options_controls.interactable = false;
		options_sound.interactable = false;
		menu_options = false;
		Instantiate (laser, options_back.transform.position, options_back.transform.rotation);
		options.interactable = true;
		extras.interactable = true;
		exit.interactable = true;
		play.interactable = true;
		options.Select ();
	}

	public void SubmitExtras()
	{
		extras_achievements.interactable = true;
		menu_extras = true;
		extras_unlockables.interactable = true;
		extras_back.interactable = true;
		Instantiate (laser, extras.transform.position, extras.transform.rotation);
		extras_achievements.Select ();
		options.interactable = false;
		extras.interactable = false;
		exit.interactable = false;
		play.interactable = false;
	}

	public void CancelExtras1()
	{
		extras_back.Select ();
	}

	public void CancelExtras2()
	{	
		extras_achievements.interactable = false;
		menu_extras = false;
		extras_unlockables.interactable = false;
		extras_back.interactable = false;
		Instantiate (laser, extras_back.transform.position, extras_back.transform.rotation);
		extras.Select ();
		options.interactable = true;
		extras.interactable = true;
		exit.interactable = true;
		play.interactable = true;

	}

	public void ExitGame()
	{
		Application.Quit ();
	}
	       

	public void SubmitOptionsControls1()
	{
		/*controls = true;
		menu_options = false;
		options_controls_return.interactable = true;
		options_controls_image.GetComponent<Image> ().enabled = true;
		select.Play ();
		options_controls_return.Select ();
		options_back.interactable = false;
		options_controls.interactable = false;
		options_sound.interactable= false;*/

		SceneManager.LoadScene ("controls");
	}

	public void SubmitOptionsControls2()
	{
		menu_options = true;
		controls = false;
		options_controls_return.interactable = false;
		options_controls_image.GetComponent<Image> ().enabled = false;
		select.Play ();
		options_back.interactable = true;
		options_controls.interactable = true;
		options_sound.interactable = true;
		options_controls.Select ();

	}

	public void SubmitPlayHowtoplay()
	{
		howtoplaybool1 = true;
		menu_play = false;
		play_playonline.interactable = false;
		play_playoffline.interactable = false;
		play_howtoplay.interactable = false;
		play_back.interactable = false;
		howtoplaybutton1.interactable = true;
		howtoplaybutton1.Select ();
		howtoplay1.GetComponent<Image> ().enabled = true;
		select.Play ();
	}

	public void SubmitPlayHowtoplay1()
	{
		howtoplaybool1 = false;
		howtoplaybool2 = true;
		howtoplaybutton1.interactable = false;
		howtoplaybutton2.interactable = true;
		howtoplaybutton2.Select ();
		howtoplay1.GetComponent<Image> ().enabled = false;
		howtoplay2.GetComponent<Image> ().enabled = true;
		select.Play ();
	}

	public void SubmitPlayHowtoplay2()
	{
		howtoplaybool2 = false;
		howtoplaybool3 = true;
		howtoplaybutton2.interactable = false;
		howtoplaybutton3.interactable = true;
		howtoplaybutton3.Select ();
		howtoplay2.GetComponent<Image> ().enabled = false;
		howtoplay3.GetComponent<Image> ().enabled = true;
		select.Play ();
	}
	public void SubmitPlayHowtoplay3()
	{
		howtoplaybool3 = false;
		howtoplaybool4 = true;
		howtoplaybutton3.interactable = false;
		howtoplaybutton4.interactable = true;
		howtoplaybutton4.Select ();
		howtoplay3.GetComponent<Image> ().enabled = false;
		howtoplay4.GetComponent<Image> ().enabled = true;
		select.Play ();
	}
	public void SubmitPlayHowtoplay4()
	{
		howtoplaybool4 = false;
		howtoplaybool5 = true;
		howtoplaybutton4.interactable = false;
		howtoplaybutton5.interactable = true;
		howtoplaybutton5.Select ();
		howtoplay4.GetComponent<Image> ().enabled = false;
		howtoplay5.GetComponent<Image> ().enabled = true;
		select.Play ();
	}
	public void SubmitPlayHowtoplay5()
	{
		howtoplaybool5 = false;
		howtoplaybool6 = true;
		howtoplaybutton5.interactable = false;
		howtoplaybutton6.interactable = true;
		howtoplaybutton6.Select ();
		howtoplay5.GetComponent<Image> ().enabled = false;
		howtoplay6.GetComponent<Image> ().enabled = true;
		select.Play ();
	}
	public void SubmitPlayHowtoplay6()
	{
		howtoplaybool5 = false;
		menu_play = true;
		howtoplaybutton6.interactable = false;
		play_playonline.interactable = true;
		play_playoffline.interactable = true;
		play_howtoplay.interactable = true;
		play_back.interactable = true;
		play_playonline.Select ();
		howtoplay6.GetComponent<Image> ().enabled = false;
		select.Play ();
	}

	public void SubmitPlayPlayOffline()
	{
		SceneManager.LoadScene("Mode Selection Screen");
	}

	public void SubmitExtrasUnlockables()
	{
		SceneManager.LoadScene ("Test");
	}
}
