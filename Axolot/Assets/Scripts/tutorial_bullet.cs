﻿using UnityEngine;
using System.Collections;

public class tutorial_bullet : MonoBehaviour {

	public float speed;
	public float life;

	private Vector3 start_pos;
	void Start () 
	{

		//Destroy (gameObject, life);
		start_pos = transform.position;
		GetComponent<Rigidbody2D>().AddForce (transform.up * speed);
	}

	// Update is called once per frame
	void Update () 
	{
		GetComponent<ParticleSystem>().Emit (5);
		if ((transform.position - start_pos).magnitude > 5) {
			transform.position = new Vector3 (start_pos.x, start_pos.y);
		}
	}
}
