﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Shotgun : MonoBehaviour {

	public GameObject ship;
	public GameObject projectile;	// this is the type of particle
	private bool fired;				// indicates if it has been fired
	public float fire_angle; 		// the angle between 2 bullets
	public float charges;			// how many times you can fire
	public Sprite icon;

	private GameObject temp;

	// Use this for initialization
	void Start () {

		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		fired = false;
		if (charges < 1) {
			charges = 1;
		}
		ship.GetComponent< Ship_Movement_2Player> ().municion_texto.text = charges.ToString();
	}
	
	// Update is called once per frame
	void Update () 
	{
		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		projectile.transform.rotation = transform.rotation;

		if (Input.GetKeyDown (ship.GetComponent< Ship_Movement_2Player>().p_fire)) {
			fired = true;
			charges--;
		}
		ship.GetComponent< Ship_Movement_2Player> ().municion_texto.text = charges.ToString();
	}

	void FixedUpdate()
	{

		if (fired == true)
		{
			projectile.transform.rotation = transform.rotation;
			projectile.transform.Rotate(0,0,-fire_angle);
			temp = Instantiate (projectile, transform.position, projectile.transform.rotation) as GameObject;
			temp.GetComponent<ScriptDisparo>().ship = ship;		// this sets the owner so these bullets can't kill the owner
			projectile.transform.Rotate(0,0,fire_angle);
			temp = Instantiate (projectile, transform.position, projectile.transform.rotation) as GameObject;
			temp.GetComponent<ScriptDisparo> ().ship = ship;
			projectile.transform.Rotate(0,0,fire_angle);
			temp = Instantiate (projectile, transform.position, projectile.transform.rotation) as GameObject;
			temp.GetComponent<ScriptDisparo> ().ship = ship;
			fired = false;
		}

		if (charges == 0) {
			ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
			Destroy(gameObject);
		}
	}
}
