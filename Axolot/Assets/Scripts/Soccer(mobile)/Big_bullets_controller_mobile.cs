﻿using UnityEngine;
using System.Collections;

public class Big_bullets_controller_mobile : MonoBehaviour {

	public GameObject ship;
	public GameObject projectile;	// this is the type of particle
	private bool fired;				// indicates if it has been fired
	public float duration;
	public float attack_delay;		// time between attacks
	private float temp_time;

	public Sprite icon;

	private GameObject temp;

	// Use this for initialization
	void Start () {

		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		fired = false;
		temp_time = 0;

		//ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = charges.ToString();
	}

	// Update is called once per frame
	void Update () 
	{
		duration -= Time.deltaTime;

		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		projectile.transform.rotation = transform.rotation;

		if (Input.GetKeyDown (ship.GetComponent<Controls_mobile>().p_fire)  &&  temp_time <= 0) {
			fired = true;
			temp_time = attack_delay;
		}
		if(temp_time > 0){
			temp_time -= Time.deltaTime;
		}
		ship.GetComponent<Controls_mobile> ().municion_texto.text = ((int)duration).ToString();
	}

	void FixedUpdate()
	{

		if (fired == true)
		{
			projectile.transform.rotation = transform.rotation;
			temp = Instantiate (projectile, transform.position, projectile.transform.rotation) as GameObject;
			//temp.GetComponent<ScriptDisparo> ().ship = ship;
			fired = false;
		}

		if (duration <= 0) {
			ship.GetComponent<Controls_mobile>().PowerUp = 0;
			Destroy(gameObject);
		}
	}
	public void Fire()
	{
		if (temp_time <= 0) {
			fired = true;
			temp_time = attack_delay;
		}
		if(temp_time > 0){
			temp_time -= Time.deltaTime;
		}
		ship.GetComponent<Controls_mobile> ().municion_texto.text = ((int)duration).ToString();
	}

}
