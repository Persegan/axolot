﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Controls_mobile : MonoBehaviour {

	public float RotationSpeed;
	public AudioSource PowerUpSound;
	public GameObject DisparoAjolote;
	public Text municion_texto;
	public Image arma;
	public Sprite Arma1;
	public Text PlayerIndicator;
	//public GameObject stun_effect;
	Animator animator;
	public GameObject big_bullets;
	public GameObject cluster_bomb;
	public GameObject directional_missile;
	public GameObject mines;
	public GameObject satellites;
	public GameObject shotgun;
	public GameObject mega_turbo_killer;
	public GameObject mega_death_rocket;
	public GameObject shield_mobile;
	public GameObject reflector;
	public int score;


	public int PowerUp = 0;
	public int municion = 0;
	private GameObject Powerup_PickedUp;
	//private bool turbo_bool = false;
	private bool stunned = false;
	private bool disabled = false;
	private bool fired = false;		// used to capture the fire button in the update function to minimize input loss
	public GameObject temp {
		get;
		set;
	}

	public bool shield_deployed = false;
	Animator powerup_animator;

	// Variables below are only used to differentiate P1 from P2 for testing the 1v1
	public int player_number;
	private KeyCode p_turbo;
	private KeyCode p_move_forward;

	private bool rotateright = false;
	private bool rotateleft = false;

	public KeyCode p_rotate_right {
		get;
		set;
	}

	public KeyCode p_rotate_left {
		get;
		set;
	}

	public KeyCode p_fire {
		get;
		set;
	}


	// Use this for initialization
	void Start () 
	{
		animator = gameObject.GetComponent<Animator> ();
		animator.SetBool("Movement", false);
		municion_texto = municion_texto.GetComponent <Text>();
		municion_texto.text = "";

		// The code below is only used to differentiate P1 from P2 for testing the 1v1
		if (player_number == 1) {
			p_rotate_right = Controls_Manager.controls_manager.Right1;
			p_rotate_left = Controls_Manager.controls_manager.Left1;
			p_turbo = Controls_Manager.controls_manager.cancelturbo1;
			p_move_forward = Controls_Manager.controls_manager.Up1;
			p_fire = Controls_Manager.controls_manager.acceptfire1;
		}
		else if (player_number == 2) {
			p_rotate_right = Controls_Manager.controls_manager.Right2;
			p_rotate_left = Controls_Manager.controls_manager.Left2;
			p_turbo = Controls_Manager.controls_manager.cancelturbo2;
			p_move_forward = Controls_Manager.controls_manager.Up2;
			p_fire = Controls_Manager.controls_manager.acceptfire2;
		}
	}

	void Update()
	{
		float angle = transform.eulerAngles.z;
		if (Input.GetKeyDown (p_fire)) {
			fired = true;
		}
		if (rotateleft == true) {
			angle += RotationSpeed;
			animator.SetBool("Rotating_left", true);
			animator.SetBool("Rotating_right", false);

			if (player_number == 1) {
				angle = Mathf.Clamp (angle, 0, 180);
			}
			else if (player_number == 2){
				angle = Mathf.Clamp (angle, 180, 359.9f);
			}
			transform.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);
		}

		if (rotateright == true) {
			angle -= RotationSpeed;
			animator.SetBool ("Rotating_right", true);
			animator.SetBool ("Rotating_left", false);

			if (player_number == 1) {
				angle = Mathf.Clamp (angle, 0, 180);
			}
			else if (player_number == 2){
				angle = Mathf.Clamp (angle, 180, 359.9f);
			}
			transform.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);
		}

		if (rotateright == false && rotateleft == false) {
			animator.SetBool ("Rotating_right", false);
			animator.SetBool ("Rotating_left", false);
		}
	}


	public void StartRotateRight()
	{
		rotateright = true;
	}

	public void StartRotateLeft()
	{
		rotateleft = true;
	}

	public void StopRotateRight()
	{
		rotateright = false;
	}

	public void StopRotateLeft()
	{
		rotateleft = false;
	}

	public void Fire()
	{
		switch (PowerUp) {
		case 1:
			temp.GetComponent<Big_bullets_controller_mobile> ().Fire ();
			break;
		case 2:
			temp.GetComponent<Shotgun_mobile> ().Fire ();
			break;

		case 3:
			fired = true;			
			break;
		default:
			break;

		}

	}

	void FixedUpdate()
	{
		if (PowerUp == 0)
		{
			arma.enabled = false;
			municion_texto.text = "";
		}
		else if (PowerUp == 3)
		{
			arma.enabled = true;
			municion_texto.text = municion.ToString();
		}
		if (stunned == false && disabled == false)
		{
			float angle = transform.eulerAngles.z;

			if (Input.GetKey(p_rotate_left))
			{
				angle += RotationSpeed;
				animator.SetBool("Rotating_left", true);
				animator.SetBool("Rotating_right", false);

				if (player_number == 1) {
					angle = Mathf.Clamp (angle, 0, 180);
				}
				else if (player_number == 2){
					angle = Mathf.Clamp (angle, 180, 359.9f);
				}
				transform.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);
			}
			else if (Input.GetKey(p_rotate_right))
			{
				angle -= RotationSpeed;
				animator.SetBool ("Rotating_right", true);
				animator.SetBool ("Rotating_left", false);

				if (player_number == 1) {
					angle = Mathf.Clamp (angle, 0, 180);
				}
				else if (player_number == 2){
					angle = Mathf.Clamp (angle, 180, 359.9f);
				}
				transform.localRotation = Quaternion.AngleAxis (angle, Vector3.forward);
			}


			if (Input.GetKey(p_move_forward))
			{
				
			}
			if (fired == true)
			{
				if (PowerUp == 3)
				{
					temp = Instantiate (DisparoAjolote, transform.position, transform.rotation) as GameObject;
					temp.GetComponent<Bullets_mobile>().ship = gameObject;		// this sets the bullet owner. why? none of your business #MC2015
					municion -= 1;

					if (municion == 0)
					{
						PowerUp = 0;
					}
				}
				fired = false;		// reset that the player hasnt fired

			}
			else if (stunned == true)
			{
				StartCoroutine(GetStunned());
			}		
		}
	}
		

	public void set_disabled(bool status)		// this is just so we can enable/disable the controls from other scripts properly
	{
		disabled = status;
	}


	public void PowerUpPickUp(GameObject power_up)
	{	//this handles the assignment of a power up that is picked up
		//when the player gets a power up, one will be randomly assigned to him here

		do {
			PowerUp = Random.Range (1, 5);
		} while(PowerUp == 4 && shield_deployed == true);		// this is so you font get the shield power up if you have it already
		Powerup_PickedUp = power_up;
		powerup_animator = Powerup_PickedUp.GetComponent<Animator> ();
		Powerup_PickedUp.GetComponent<PowerUp_mobile> ().PowerUp_available = false;
		powerup_animator.SetBool ("pick_up", true);
		Destroy (Powerup_PickedUp, 0.45f);
		PowerUpSound.Play ();


		// this is for testing purposes
		//PowerUp = 1;

		// this is for testing purposes

		switch (PowerUp) {
		case 1: 								//1. big bullets
			big_bullets.GetComponent<Big_bullets_controller_mobile>().ship = gameObject;
			arma.GetComponent<Image>().sprite = big_bullets.GetComponent<Big_bullets_controller_mobile>().icon;
			temp = Instantiate(big_bullets);
			break;
		case 20: 								//2. cluster bomb
			cluster_bomb.GetComponent<Cluster_bomb>().ship = gameObject;
			arma.GetComponent<Image>().sprite = cluster_bomb.GetComponent<Cluster_bomb>().icon;
			temp = Instantiate(cluster_bomb);
			break;
		case 30: 								//3. directional missile
			directional_missile.GetComponent<Directional_missile_v1_1>().ship = gameObject;
			arma.GetComponent<Image>().sprite = directional_missile.GetComponent<Directional_missile_v1_1>().icon;
			temp = Instantiate(directional_missile);
			break;
		case 40: 								//4. mines
			mines.GetComponent<mine_controller>().ship = gameObject;
			arma.GetComponent<Image>().sprite = mines.GetComponent<mine_controller>().icon;
			temp = Instantiate(mines);
			break;
		case 50: 								//5. satelites
			satellites.GetComponent<satellite_controller>().ship = gameObject;
			arma.GetComponent<Image>().sprite = satellites.GetComponent<satellite_controller>().icon;
			temp = Instantiate(satellites);
			satellites.GetComponent<satellite_controller>().satellite.GetComponent<satellite_powerup>().controller = temp;
			break;
		case 2: 								//6. shotgun
			shotgun.GetComponent<Shotgun_mobile>().ship = gameObject;
			arma.GetComponent<Image>().sprite = shotgun.GetComponent<Shotgun_mobile>().icon;
			temp = Instantiate(shotgun);
			break;
		case 70: 								//7. mega turbo killer
			mega_turbo_killer.GetComponent<mega_turbo_killer>().ship = gameObject;
			mega_turbo_killer.GetComponent<mega_turbo_killer>().origin_point = gameObject.transform.GetChild(4); // this is going to break if the ships childs are moved
			arma.GetComponent<Image>().sprite = mega_turbo_killer.GetComponent<mega_turbo_killer>().icon;
			temp = Instantiate(mega_turbo_killer);
			break;
		case 80: 								//8. mega death rocket
			mega_death_rocket.GetComponent<Mega_death_rocket>().ship = gameObject;
			arma.GetComponent<Image>().sprite = mega_death_rocket.GetComponent<Mega_death_rocket>().icon;
			temp = Instantiate(mega_death_rocket);
			break;
		case 3: 								//3. normal projectiles
			municion = 5;
			arma.GetComponent<Image> ().sprite = Arma1;
			break;
		case 4:								//10. shield
			shield_mobile.GetComponent<Shield_mobile> ().ship = gameObject;
			temp = Instantiate (shield_mobile);
			shield_deployed = true;
			PowerUp = 0;
			break;
		case 110:								//11. reflector, reflects bullets, destroys one (or more) bigger projectiles
			reflector.GetComponent<Reflector> ().ship = gameObject;
			reflector.GetComponent<Reflector>().origin_point = gameObject.transform.GetChild(5);
			arma.GetComponent<Image> ().sprite = reflector.GetComponent<Reflector> ().icon;
			temp = Instantiate (reflector);
			break;
		default:
			break;
		}
		arma.enabled = true;						// this activates the UI elements for the ammo and icon
		//yield return new WaitForSeconds(0.0f);
	}


	IEnumerator GetStunned()	// this stuns the player through a collision with a red bubble
	{
		animator.SetBool("Movement", false);
		//parar el sonido
		yield return new WaitForSeconds (1.5f);
		stunned = false;
	}


	void OnDisable() {			// clean up code for when the ship gets destroyed
		arma.enabled = false;
		Destroy (temp);
	}
}