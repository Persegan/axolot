﻿using UnityEngine;
using System.Collections;

public class Shield_mobile : MonoBehaviour {

	public GameObject ship;
	public float duration;

	// Use this for initialization
	void Start () {
		if (ship.GetComponent<Controls_mobile> ().player_number == 1) {
			transform.position = new Vector3 (4.55f, 0.17f, 0f);
		} else if (ship.GetComponent<Controls_mobile> ().player_number == 2) {
			transform.position = new Vector3 (-4.57f, 0.17f, 0f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (duration <= 0) {
			ship.GetComponent<Controls_mobile> ().shield_deployed = false;
			Destroy (gameObject);
		}

		duration -= Time.deltaTime;
	}
}
