﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class soccer_field_mobile : MonoBehaviour {

	public GameObject ball;

	public static float red_score, blue_score;

	public Text Timer;
	public Text Score1;
	public Text Score2;
	public float tiempo_actualizar;
	public float tiempo_restante_segundos;
	public float tiempo_restante_minutos;



	void Start () {
		red_score = blue_score = 0;
	}

	void FixedUpdate () {
		Score1.text = red_score.ToString ();
		Score2.text = blue_score.ToString ();
	
		tiempo_actualizar -= Time.deltaTime;
		if (tiempo_restante_segundos == 0 && tiempo_restante_minutos == 0)
		{	
			SceneManager.LoadScene ("Soccer Game_Over");
		}
		else if (tiempo_restante_segundos == 0)
		{
			tiempo_restante_minutos -= 1;
			tiempo_restante_segundos = 60;
		}
		if (tiempo_restante_segundos >  60)
		{
			tiempo_restante_minutos +=1;
			tiempo_restante_segundos = tiempo_restante_segundos-60;
		}
		if (tiempo_actualizar < 0)
		{
			tiempo_restante_segundos -= 1;
			Timer.text = tiempo_restante_minutos.ToString();
			Timer.text += ":";
			if (tiempo_restante_segundos <10)
			{
				Timer.text += "0";
				Timer.text += tiempo_restante_segundos.ToString();
			}
			else
			{
				Timer.text += tiempo_restante_segundos.ToString();
			}
			tiempo_actualizar = 1;
		}
	}


	public void RedTeamScores(){
		GameObject [] players;
		players = GameObject.FindGameObjectsWithTag ("jugador");
		Destroy (players [0].GetComponent<Controls_mobile> ().temp);
		Destroy (players [1].GetComponent<Controls_mobile> ().temp);
		players [0].GetComponent<Controls_mobile> ().PowerUp = 0;
		players [1].GetComponent<Controls_mobile> ().PowerUp = 0;
		red_score++;
		print ("Red team scored! " + red_score + ":" + blue_score);
		ball.GetComponent<ball> ().RespawnBall ();
	}

	public void BlueTeamScores(){
		GameObject [] players;
		players = GameObject.FindGameObjectsWithTag ("jugador");
		Destroy (players [0].GetComponent<Controls_mobile> ().temp);
		Destroy (players [1].GetComponent<Controls_mobile> ().temp);
		players [0].GetComponent<Controls_mobile> ().PowerUp = 0;
		players [1].GetComponent<Controls_mobile> ().PowerUp = 0;
		blue_score++;
		print ("Blue team scored! " + red_score + ":" + blue_score);
		ball.GetComponent<ball> ().RespawnBall ();
	}
}
