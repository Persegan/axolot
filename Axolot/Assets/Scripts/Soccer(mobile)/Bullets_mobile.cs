﻿using UnityEngine;
using System.Collections;

public class Bullets_mobile : MonoBehaviour {

	public float velocidadDisparo;
	public float TiempodeVida;

	public GameObject ship;

	void Start () 
	{

		Destroy (gameObject, TiempodeVida);
		GetComponent<Rigidbody2D>().AddForce (transform.up * velocidadDisparo);
	}

	// Update is called once per frame
	void Update () 
	{
		GetComponent<ParticleSystem>().Emit (5);

	}

	void OnCollisionExit2D(Collision2D collider){
		if(collider.gameObject.tag == "ball")
			Destroy (gameObject);
	}
}
