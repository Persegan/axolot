﻿using UnityEngine;
using System.Collections;

public class Big_bullet : MonoBehaviour {

	public float speed;
	public float life;

	public GameObject ship;

	void Start () 
	{

		Destroy (gameObject, life);
		GetComponent<Rigidbody2D>().AddForce (transform.up * speed);
	}

	// Update is called once per frame
	void Update () 
	{
		//GetComponent<ParticleSystem>().Emit (5);

	}

	void OnCollisionExit2D(Collision2D collider){
		if(collider.gameObject.tag == "ball")
			Destroy (gameObject);
	}
}
