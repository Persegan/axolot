﻿using UnityEngine;
using System.Collections;

public class Shotgun_mobile : MonoBehaviour {

	public GameObject ship;
	public GameObject projectile;	// this is the type of particle
	private bool fired;				// indicates if it has been fired
	public float fire_angle; 		// the angle between 2 bullets
	public float charges;			// how many times you can fire
	public Sprite icon;

	private GameObject temp;

	// Use this for initialization
	void Start () {

		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		fired = false;
		if (charges < 1) {
			charges = 1;
		}
		ship.GetComponent<Controls_mobile> ().municion_texto.text = charges.ToString();
	}

	// Update is called once per frame
	void Update () 
	{
		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		projectile.transform.rotation = transform.rotation;

		if (Input.GetKeyDown (ship.GetComponent<Controls_mobile>().p_fire)) {
			fired = true;
		}
		ship.GetComponent<Controls_mobile> ().municion_texto.text = charges.ToString();
	}

	void FixedUpdate()
	{

		if (fired == true)
		{
			projectile.transform.rotation = transform.rotation;
			projectile.transform.Rotate(0,0,-fire_angle);
			temp = Instantiate (projectile, transform.position, projectile.transform.rotation) as GameObject;
			temp.GetComponent<Bullets_mobile>().ship = ship;
			projectile.transform.Rotate(0,0,fire_angle);
			temp = Instantiate (projectile, transform.position, projectile.transform.rotation) as GameObject;
			temp.GetComponent<Bullets_mobile> ().ship = ship;
			projectile.transform.Rotate(0,0,fire_angle);
			temp = Instantiate (projectile, transform.position, projectile.transform.rotation) as GameObject;
			temp.GetComponent<Bullets_mobile> ().ship = ship;
			fired = false;
			charges--;
		}

		if (charges == 0) {
			ship.GetComponent<Controls_mobile>().PowerUp = 0;
			Destroy(gameObject);
		}
	}

	public void Fire()
	{
		fired = true;
	}
}
