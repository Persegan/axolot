﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PowerUp_mobile : MonoBehaviour {

	public bool PowerUp_available;
	public Button Powerup_Button;
	private GameObject left_ship;
	private GameObject right_ship;
	private GameObject[] ships;


	// Use this for initialization
	void Start () {
		PowerUp_available = true;
		gameObject.GetComponentInChildren<Canvas> ().worldCamera = Camera.main;
		//Powerup_Button.transform.position = transform.position;
		ships = GameObject.FindGameObjectsWithTag("jugador");
		right_ship = ships [1];
		left_ship = ships [0];
	}

	// Update is called once per frame
	void FixedUpdate () 
	{
		if (PowerUp_available == false)
		{
			Destroy (gameObject.GetComponent<BoxCollider2D>());
		}
	}

	void OnMouseDown()
	{
		if (transform.position.x >= 0) {
			if(right_ship.GetComponent<Controls_mobile> ().PowerUp == 0){
				right_ship.GetComponent<Controls_mobile> ().PowerUpPickUp (gameObject);
			}
		} 
		else if (transform.position.x < 0) {
			if (left_ship.GetComponent<Controls_mobile> ().PowerUp == 0) {
				left_ship.GetComponent<Controls_mobile> ().PowerUpPickUp (gameObject);
			}
		}
	}
}
