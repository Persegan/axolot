﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FreeForAll_ControlCenter : MonoBehaviour {

	public GameObject[] ListOfSkins;


	int maxskins = 11;
	public GameObject Skin1;
	public GameObject Skin2;
	public GameObject Skin3;
	public GameObject Skin4;
	public GameObject Skin5;
    public GameObject Skin6;
    public GameObject Skin7;
    public GameObject Skin8;
    public GameObject Skin9;
    public GameObject Skin10;
    public GameObject Skin11;

    public GameObject Player1;
	public GameObject Player2;
	public GameObject Player3;
	public GameObject Player4;

	public GameObject explosion;


	public Canvas canvas;

	public Text Timer;
	public float tiempo_actualizar;
	public float tiempo_restante_segundos;
	public float tiempo_restante_minutos;

	public float max_score = 3;

	public Text P1Score;
	public Text P2Score;
	public Text P3Score;
	public Text P4Score;



	void Awake()
	{
		ListOfSkins = new GameObject[maxskins];
		ListOfSkins [0] = Skin1;
		ListOfSkins [1] = Skin2;
		ListOfSkins [2] = Skin3;
		ListOfSkins [3] = Skin4;
		ListOfSkins [4] = Skin5;
        ListOfSkins[5] = Skin6;
        ListOfSkins[6] = Skin7;
        ListOfSkins[7] = Skin8;
        ListOfSkins[8] = Skin9;
        ListOfSkins[9] = Skin10;
        ListOfSkins[10] = Skin11;
        tiempo_actualizar = 0;
		tiempo_restante_segundos = 100;
	}
	// Use this for initialization
	void Start () 
	{
		if (Controls_Manager.controls_manager.Player1.ready == true)
		{
			// if there's a player 1 according to controls manager, we spawn it in the p1 position, and we assign it a valid controller
			Player1 = Instantiate (ListOfSkins [Controls_Manager.controls_manager.Player1.skin], new Vector3(-4.5f, 1.5f,1f), Quaternion.Euler(0,0,-130)) as GameObject;
			Player1.GetComponent<Ship_Movement_2Player> ().player_number = Controls_Manager.controls_manager.Player1.controller;
			Player1.GetComponentInChildren<Canvas> ().worldCamera = Camera.main;
			Player1.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.text = "P1";
			Player1.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.color = Color.red;
			P1Score.enabled = true;
		}

		//Now we rinse and repeat the Player 1 stuff with the rest of players

		if (Controls_Manager.controls_manager.Player2.ready == true)
		{
			Player2 = Instantiate (ListOfSkins [Controls_Manager.controls_manager.Player2.skin], new Vector3(4.5f, 1.5f,1f), Quaternion.Euler(0,0,130)) as GameObject;
			Player2.GetComponent<Ship_Movement_2Player> ().player_number = Controls_Manager.controls_manager.Player2.controller;
			Player2.GetComponentInChildren<Canvas> ().worldCamera = Camera.main;
			Player2.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.text = "P2";
			Player2.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.color = Color.blue;
			P2Score.enabled = true;

		}

		if (Controls_Manager.controls_manager.Player3.ready == true)
		{
			
			Player3 = Instantiate (ListOfSkins [Controls_Manager.controls_manager.Player3.skin], new Vector3(-4.5f, -1f,1f), Quaternion.Euler(0,0,-60)) as GameObject;
			Player3.GetComponent<Ship_Movement_2Player> ().player_number = Controls_Manager.controls_manager.Player3.controller;
			Player3.GetComponentInChildren<Canvas> ().worldCamera = Camera.main;
			Player3.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.text = "P3";
			Player3.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.color = Color.green;
			P3Score.enabled = true;

		}

		if (Controls_Manager.controls_manager.Player4.ready == true)
		{
			Player4 = Instantiate (ListOfSkins [Controls_Manager.controls_manager.Player4.skin], new Vector3(4.5f, -1f,1f), Quaternion.Euler(0,0,60)) as GameObject;
			Player4.GetComponent<Ship_Movement_2Player> ().player_number = Controls_Manager.controls_manager.Player4.controller;
			Player4.GetComponentInChildren<Canvas> ().worldCamera = Camera.main;
			Player4.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.text = "P4";
			Player4.GetComponent<Ship_Movement_2Player> ().PlayerIndicator.color = Color.yellow;
			P4Score.enabled = true;

		}

	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Player1 != null)
		{
			Controls_Manager.controls_manager.Player1.score = Player1.GetComponent<Ship_Movement_2Player> ().score;
		}

		if (Player2 != null)
		{
			Controls_Manager.controls_manager.Player2.score = Player2.GetComponent<Ship_Movement_2Player> ().score;
		}

		if (Player3 != null)
		{
			Controls_Manager.controls_manager.Player3.score = Player3.GetComponent<Ship_Movement_2Player> ().score;
		}
		if (Player4 != null)
		{
			Controls_Manager.controls_manager.Player4.score = Player4.GetComponent<Ship_Movement_2Player> ().score;
		}
	}
	void FixedUpdate()
	{
		//This manages the timer
		tiempo_actualizar -= Time.deltaTime;
		if (tiempo_restante_segundos == 0 && tiempo_restante_minutos == 0)
		{	
			Destroy (Timer);
			if (Controls_Manager.controls_manager.Player1.ready == true) 
			{
				Instantiate (explosion, Player1.transform.position, Player1.transform.rotation);
				Destroy (Player1);
			}
			if (Controls_Manager.controls_manager.Player2.ready == true) 
			{
				Instantiate (explosion, Player2.transform.position, Player2.transform.rotation);
				Destroy (Player2);
			}
			if (Controls_Manager.controls_manager.Player3.ready == true) 
			{
				Instantiate (explosion, Player3.transform.position, Player3.transform.rotation);
				Destroy (Player3);
			}
			if (Controls_Manager.controls_manager.Player4.ready == true) 
			{
				Instantiate (explosion, Player4.transform.position, Player4.transform.rotation);
				Destroy (Player4);
			}
			StartCoroutine (endgame ());
		}
		else if (tiempo_restante_segundos == 0)
		{
			tiempo_restante_minutos -= 1;
			tiempo_restante_segundos = 60;
		}
		if (tiempo_restante_segundos >  60)
		{
			tiempo_restante_minutos +=1;
			tiempo_restante_segundos = tiempo_restante_segundos-60;
		}
		if (tiempo_actualizar < 0)
		{
			tiempo_restante_segundos -= 1;
			Timer.text = tiempo_restante_minutos.ToString();
			Timer.text += ":";
			if (tiempo_restante_segundos <10)
			{
				Timer.text += "0";
				Timer.text += tiempo_restante_segundos.ToString();
			}
			else
			{
				Timer.text += tiempo_restante_segundos.ToString();
			}
			tiempo_actualizar = 1;
		}

		if (Controls_Manager.controls_manager.Player1.score == max_score || Controls_Manager.controls_manager.Player2.score == max_score || 
			Controls_Manager.controls_manager.Player3.score == max_score || Controls_Manager.controls_manager.Player4.score == max_score)
		{
			
			if (Controls_Manager.controls_manager.Player1.ready == true && Controls_Manager.controls_manager.Player1.score != max_score) 
			{
				Instantiate (explosion, Player1.transform.position, Player1.transform.rotation);
				Destroy (Player1);
			}
			if (Controls_Manager.controls_manager.Player2.ready == true && Controls_Manager.controls_manager.Player2.score != max_score) 
			{
				Instantiate (explosion, Player2.transform.position, Player2.transform.rotation);
				Destroy (Player2);
			}
			if (Controls_Manager.controls_manager.Player3.ready == true && Controls_Manager.controls_manager.Player3.score != max_score) 
			{
				Instantiate (explosion, Player3.transform.position, Player3.transform.rotation);
				Destroy (Player3);
			}
			if (Controls_Manager.controls_manager.Player4.ready == true && Controls_Manager.controls_manager.Player4.score != max_score) 
			{
				Instantiate (explosion, Player4.transform.position, Player4.transform.rotation);
				Destroy (Player4);
			}
			StartCoroutine (endgame ());
		}

		//This manages the score
		P1Score.text = "P1: " + Controls_Manager.controls_manager.Player1.score;
		P2Score.text = "P2: " + Controls_Manager.controls_manager.Player2.score;
		P3Score.text = "P3: " + Controls_Manager.controls_manager.Player3.score;
		P4Score.text = "P4: " + Controls_Manager.controls_manager.Player4.score;
	}

	IEnumerator endgame()
	{
		yield return new WaitForSeconds (1f);
		SceneManager.LoadScene ("Free For All End Game Screen");
	}
}
