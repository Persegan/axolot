﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Reflector : MonoBehaviour {
	public GameObject ship;
	public float duration;
	public float knockback_force;			// If the reflector gets hit with a big prokectile, the ship will be knocked back
	public Transform origin_point;			// where the reflector will appear in relation to the ship
	public Sprite icon;
	

	// Use this for initialization
	void Start () {
		transform.position = origin_point.position;
		transform.rotation = origin_point.rotation;
		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = "";
	}
	
	// Update is called once per frame
	void Update () {
		//GetComponent<Rigidbody2D> ().MovePosition (origin_point.position);
		//GetComponent<Rigidbody2D> ().MoveRotation (origin_point.eulerAngles.magnitude);  // this 2 move functions are better than changing the transform in this case
		transform.position = origin_point.position;
		transform.rotation = origin_point.rotation;
		if (duration < 0) {
			ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			Destroy(gameObject);
		}
		duration -= Time.deltaTime;
	}
		

	void OnCollisionEnter2D(Collision2D other){		// the reflector never gets destroyed by collisions
		/*if (collider.tag != "bala_jugador" || collider.tag != "Bala_jugador_1" || collider.tag != "Bala_jugador_2") {
			ship.GetComponent<Rigidbody2D>().AddForce(-ship.transform.up * knockback_force);
		}*/
	}
}
