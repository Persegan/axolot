﻿using UnityEngine;
using System.Collections;

public class missile_explosion : MonoBehaviour {
	public GameObject ship;
	public float duration;				// how long the explosion will persist
	public float expansion_time;		// after this time passes, the collider will stop being a trigger. should probably be tied to the animation timing
	//public float radius;

	// Use this for initialization
	void Start () {
		Destroy (gameObject, duration);	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (expansion_time <= 0)
			GetComponent<CircleCollider2D> ().isTrigger = false;
		expansion_time -= Time.fixedDeltaTime;
	}
		
}
