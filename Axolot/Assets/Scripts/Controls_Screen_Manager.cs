﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Controls_Screen_Manager : MonoBehaviour {

	public GameObject Controls_Manager;
	public Button Keyboard_Layout;
	public Button Gamepad_Layout;
	public Image Gamepad;
	public Button Back;
	public Button Up1;
	public Text Up1_Text;
	public Button Up2;
	public Text Up2_Text;
	public Button Right1;
	public Text Right1_Text;
	public Button Right2;
	public Text Right2_Text;
	public Button Left1;
	public Text Left1_Text;
	public Button Left2;
	public Text Left2_Text;
	public Button Down1;
	public Text Down1_Text;
	public Button Down2;
	public Text Down2_Text;
	public Button Fire1;
	public Text Fire1_Text;
	public Button Fire2;
	public Text Fire2_Text;
	public Button Turbo1;
	public Text Turbo1_Text;
	public Button Turbo2;
	public Text Turbo2_Text;
	public Button Save;
	public Button Default;
	public Button Empty;
	public Button Gamepad_Button;

	private bool Up1bool = false;
	private bool Up2bool = false;
	private bool Right1bool = false;
	private bool Right2bool = false;
	private bool Left1bool = false;
	private bool Left2bool = false;
	private bool Down1bool = false;
	private bool Down2bool = false;
	private bool Shoot1bool = false;
	private bool Shoot2bool = false;
	private bool Turbo1bool = false;
	private bool Turbo2bool = false;




	// Use this for initialization
	void Start () {
		Keyboard_Layout.Select ();
		Controls_Manager = GameObject.Find ("Controls_Manager");

		Up1_Text = Up1_Text.GetComponent<Text> ();
		Up1_Text.text = "P1 Up: ";
		Up1_Text.text += PlayerPrefs.GetString("Up1");

		Up2_Text = Up2_Text.GetComponent<Text> ();
		Up2_Text.text = "P2 Up: ";
		Up2_Text.text += PlayerPrefs.GetString("Up2");

		Right1_Text = Right1_Text.GetComponent<Text> ();
		Right1_Text.text = "P1 Right: ";
		Right1_Text.text += PlayerPrefs.GetString("Right1");

		Right2_Text = Right2_Text.GetComponent<Text> ();
		Right2_Text.text = "P2 Right: ";
		Right2_Text.text += PlayerPrefs.GetString("Right2");

		Down1_Text = Down1_Text.GetComponent<Text> ();
		Down1_Text.text = "P1 Down: ";
		Down1_Text.text += PlayerPrefs.GetString("Down1");

		Down2_Text = Down2_Text.GetComponent<Text> ();
		Down2_Text.text = "P2 Down: ";
		Down2_Text.text += PlayerPrefs.GetString("Down2");

		Left1_Text = Left1_Text.GetComponent<Text> ();
		Left1_Text.text = "P1 Left: ";
		Left1_Text.text += PlayerPrefs.GetString("Left1");

		Left2_Text = Left2_Text.GetComponent<Text> ();
		Left2_Text.text = "P2 Left: ";
		Left2_Text.text += PlayerPrefs.GetString("Left2");

		Fire1_Text = Fire1_Text.GetComponent<Text> ();
		Fire1_Text.text = "P1 Accept / Fire: ";
		Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");

		Fire2_Text = Fire2_Text.GetComponent<Text> ();
		Fire2_Text.text = "P2 Accept / Fire: ";
		Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");

		Turbo1_Text = Turbo1_Text.GetComponent<Text> ();
		Turbo1_Text.text = "P1 Cancel / Turbo: ";
		Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");

		Turbo2_Text = Turbo2_Text.GetComponent<Text> ();
		Turbo2_Text.text = "P2 Cancel / Turbo: ";
		Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");



	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Up1bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Up1", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Up1", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Up1", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Up1", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{

			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Up1", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Up1", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Up1", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Up1", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Up1", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Up1", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Up1", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Up1", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Up1", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Up1", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Up1", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Up1", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Up1", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Up1", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Up1", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Up1", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Up1", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Up1", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Up1", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Up1", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Up1", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Up1", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Up1", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Up1", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Up1", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Up1", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Up1", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Up1", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Up1", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Up1", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Up1", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Up1", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Up1", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Up1", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Up1", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Up1", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Up1", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Up1", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Up1", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Up1", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Up1", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Up1", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Up1", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Up1", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Up1", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Up1", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Up1", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Up1", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Up1", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Up1", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Up1", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Up1", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Up1", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Up1", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Up1", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Up1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Up1", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Up1", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Up1", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Up1", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Up1", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Up1", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Up1", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Up1", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Up1", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Up1", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Up1", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Up1", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Up1", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Up1", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Up1", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Up1", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Up1", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Up1", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Up1", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Up1", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Up1", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Up1", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Up1", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Up1", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Up1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Up1", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Up1", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Up1", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Up1", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Up1", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Up1", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Up1", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Up1", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Up1", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Up1", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
				Up1_Text.text = "P1 Up: ";
				Up1_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}

		}
	
		else if (Up2bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Up2", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Up2", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Up2", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Up2", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Up2", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Up2", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Up2", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Up2", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Up2", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Up2", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Up2", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Up2", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Up2", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Up2", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Up2", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Up2", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Up2", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Up2", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Up2", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Up2", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Up2", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Up2", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Up2", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Up2", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Up2", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Up2", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Up2", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Up2", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Up2", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Up2", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Up2", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Up2", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Up2", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Up2", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Up2", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Up2", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Up2", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Up2", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Up2", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Up2", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Up2", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Up2", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Up2", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Up2", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Up2", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Up2", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Up2", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Up2", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Up2", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Up2", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Up2", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Up2", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Up2", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Up2", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Up2", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Up2", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Up2", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Up2", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Up2", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Up2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Up2", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Up2", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Up2", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Up2", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Up2", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Up2", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Up2", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Up2", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Up2", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Up2", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Up2", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Up2", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Up2", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Up2", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Up2", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Up2", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Up2", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Up2", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Up2", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Up2", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Up2", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Up2", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Up2", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Up2", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Up2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Up2", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Up2", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Up2", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Up2", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Up2", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Up2", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Up2", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Up2", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Up2", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Up2", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
				Up2_Text.text = "P2 Up: ";
				Up2_Text.text += PlayerPrefs.GetString("Up2");
				Up2.Select ();
				Up2bool = false;
			}
		}
		else if (Right1bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Right1", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Right1", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Right1", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Right1", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Right1", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Right1", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Right1", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Right1", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Right1", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Right1", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Right1", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Right1", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Right1", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Right1", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Right1", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Right1", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Right1", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Right1", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Right1", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Right1", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Right1", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Right1", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Right1", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Right1", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Right1", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Right1", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Right1", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Right1", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Right1", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Right1", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Right1", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Right1", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Right1", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Right1", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Right1", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Right1", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Right1", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Right1", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Right1", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Right1", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Right1", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Right1", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Right1", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Right1", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Right1", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Right1", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Right1", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Right1", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Right1", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Right1", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Right1", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Right1", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Right1", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Right1", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Right1", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Right1", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Right1", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Right1", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Right1", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Right1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Right1", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Right1", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Right1", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Right1", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Right1", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Right1", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Right1", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Right1", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Right1", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Right1", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Right1", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Right1", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Right1", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Right1", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Right1", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Right1", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Right1", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Right1", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Right1", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Right1", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Right1", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Right1", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Right1", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Right1", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Right1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Right1", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Right1", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Right1", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Right1", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Right1", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Right1", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Right1", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Right1", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Right1", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Right1", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
				Right1_Text.text = "P1 Right: ";
				Right1_Text.text += PlayerPrefs.GetString("Right1");
				Right1.Select ();
				Right1bool = false;
			}
		}
		else if (Right2bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Right2", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Right2", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Right2", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Right2", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Right2", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Right2", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Right2", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Right2", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Right2", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Right2", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Right2", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Right2", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Right2", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Right2", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Right2", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Right2", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Right2", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Right2", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Right2", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Right2", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Right2", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Right2", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Right2", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Right2", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Right2", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Right2", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Right2", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Right2", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Right2", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Right2", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Right2", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Right2", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Right2", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Right2", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Right2", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Right2", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Right2", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Right2", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Right2", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Right2", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Right2", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Right2", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Right2", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Right2", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Right2", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Right2", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Right2", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Right2", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Right2", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Right2", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Right2", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Right2", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Right2", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Right2", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Right2", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Right2", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Right2", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Right2", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Right2", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Right2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Right2", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Right2", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Right2", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Right2", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Right2", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Right2", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Right2", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Right2", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Right2", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Right2", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Right2", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Right2", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Right2", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Right2", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Right2", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Right2", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Right2", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Right2", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Right2", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Right2", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Right2", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Right2", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Right2", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Right2", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Right2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Right2", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Right2", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Right2", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Right2", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Right2", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Right2", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Right2", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Right2", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Right2", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P2 Right: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Right2", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
				Right2_Text.text = "P1 Up: ";
				Right2_Text.text += PlayerPrefs.GetString("Right2");
				Right2.Select ();
				Right2bool = false;
			}
		}
		else if (Left1bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Left1", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Left1", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Left1", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Left1", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Left1", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Left1", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Left1", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Left1", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Left1", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Left1", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Left1", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Left1", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Left1", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Left1", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Left1", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Left1", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Left1", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Left1", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Left1", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Left1", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Left1", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Left1", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Left1", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Left1", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Left1", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Left1", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Left1", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Left1", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Left1", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Left1", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Left1", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Left1", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Left1", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Left1", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Left1", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Left1", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Left1", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Left1", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Left1", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Left1", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Left1", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Left1", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Left1", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Left1", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Left1", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Left1", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Left1", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Left1", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Left1", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Left1", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Left1", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Left1", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Left1", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Left1", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Left1", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Left1", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Left1", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Left1", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Left1", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Left1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Left1", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Left1", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Left1", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Left1", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Left1", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Left1", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Left1", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Left1", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Left1", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Left1", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Left1", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Left1", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Left1", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Left1", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Left1", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Left1", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Left1", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Left1", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Left1", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Left1", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Left1", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Left1", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Left1", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Left1", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Left1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Left1", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Left1", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Left1", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Left1", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Left1", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Left1", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Left1", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Left1", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Left1", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Left1", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
				Left1_Text.text = "P1 Left: ";
				Left1_Text.text += PlayerPrefs.GetString("Left1");
				Left1.Select ();
				Left1bool = false;
			}
		}
		else if (Left2bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Left2", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Left2", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Left2", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Left2", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Left2", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Left2", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Left2", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Left2", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Left2", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Left2", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Left2", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Left2", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Left2", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Left2", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Left2", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Left2", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Left2", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Left2", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Left2", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Left2", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Left2", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Left2", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Left2", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Left2", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Left2", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Left2", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Left2", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Left2", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Left2", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Left2", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Left2", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Left2", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Left2", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Left2", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Left2", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Left2", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Left2", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Left2", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Left2", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Left2", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Left2", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Left2", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Left2", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Left2", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Left2", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Left2", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Left2", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Left2", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Left2", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Left2", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Left2", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Left2", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Left2", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Left2", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Left2", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Left2", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Left2", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Left2", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Left2", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Left2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Left2", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Left2", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Left2", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Left2", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Left2", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Left2", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Left2", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Left2", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Left2", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Left2", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Left2", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Left2", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Left2", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Left2", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Left2", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Left2", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Left2", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Left2", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Left2", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Left2", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Left2", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Left2", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Left2", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Left2", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Left2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Left2", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Left2", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Left2", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Left2", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Left2", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Left2", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Left2", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Left2", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Left2", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P2 Left: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Left2", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
				Left2_Text.text = "P1 Up: ";
				Left2_Text.text += PlayerPrefs.GetString("Left2");
				Left2.Select ();
				Left2bool = false;
			}
		}
		else if (Down1bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Down1", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Down1", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Down1", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Down1", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Down1", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Down1", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Down1", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Down1", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Down1", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Down1", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Down1", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Down1", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Down1", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Down1", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Down1", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Down1", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Down1", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Down1", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Down1", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Down1", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Down1", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Down1", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Down1", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Down1", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Down1", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Down1", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Down1", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Down1", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Down1", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Down1", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Down1", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Down1", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Down1", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Down1", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Down1", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Down1", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Down1", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Down1", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Down1", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Down1", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Down1", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Down1", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Down1", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Down1", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Down1", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Down1", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Down1", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Down1", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Down1", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Down1", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Down1", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Down1", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Down1", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Down1", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Down1", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Down1", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Down1", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Down1", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Down1", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Down1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Down1", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Down1", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Down1", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Down1", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Down1", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Down1", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Down1", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Down1", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Down1", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Down1", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Down1", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Down1", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Down1", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Down1", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Down1", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Down1", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Down1", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Down1", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Down1", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Down1", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Down1", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Down1", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Down1", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Down1", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Down1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Down1", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Down1", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Down1", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Down1", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Down1", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Down1", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Down1", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Down1", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Down1", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Down1", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
				Down1_Text.text = "P1 Down: ";
				Down1_Text.text += PlayerPrefs.GetString("Down1");
				Down1.Select ();
				Down1bool = false;
			}
		}
		else if (Down2bool == true)
		{
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("Down2", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("Down2", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("Down2", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("Down2", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("Down2", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("Down2", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("Down2", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("Down2", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("Down2", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("Down2", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("Down2", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("Down2", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("Down2", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("Down2", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("Down2", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("Down2", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("Down2", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("Down2", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("Down2", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("Down2", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("Down2", "End");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("Down2", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("Down2", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("Down2", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("Down2", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("Down2", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("Down2", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("Down2", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("Down2", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("Down2", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("Down2", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("Down2", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("Down2", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("Down2", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("Down2", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("Down2", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("Down2", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("Down2", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("Down2", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("Down2", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("Down2", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("Down2", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("Down2", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("Down2", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("Down2", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("Down2", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("Down2", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("Down2", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("Down2", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("Down2", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("Down2", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("Down2", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("Down2", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("Down2", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("Down2", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("Down2", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("Down2", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("Down2", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("Down2", "A");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Down2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("Down2", "C");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("Down2", "D");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("Down2", "E");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("Down2", "F");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("Down2", "G");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("Down2", "H");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("Down2", "I");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("Down2", "J");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("Down2", "K");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("Down2", "L");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("Down2", "M");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("Down2", "N");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("Down2", "O");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("Down2", "P");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("Down2", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("Down2", "R");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("Down2", "S");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("Down2", "T");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("Down2", "U");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("Down2", "V");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("Down2", "W");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("Down2", "X");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("Down2", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("Down2", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("Down2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("Down2", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("Down2", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("Down2", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("Down2", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("Down2", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("Down2", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("Down2", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("Down2", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("Down2", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Down2");
				Down2.Select ();
				Down2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("Down2", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
				Down2_Text.text = "P2 Down: ";
				Down2_Text.text += PlayerPrefs.GetString("Up1");
				Up1.Select ();
				Up1bool = false;
			}

		}
		else if (Shoot1bool == true)
		{
			/*if (Input.anyKeyDown) 
			{
				PlayerPrefs.SetString("acceptfire1", Input.inputString);
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Shoot1bool = false;
				Fire1.Select ();
			}*/
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("acceptfire1", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("acceptfire1", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("acceptfire1", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("acceptfire1", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("acceptfire1", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("acceptfire1", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("acceptfire1", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("acceptfire1", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("acceptfire1", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("acceptfire1", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("acceptfire1", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("acceptfire1", "End");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("acceptfire1", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("acceptfire1", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("acceptfire1", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("acceptfire1", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("acceptfire1", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("acceptfire1", "A");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("acceptfire1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("acceptfire1", "C");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("acceptfire1", "D");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("acceptfire1", "E");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("acceptfire1", "F");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("acceptfire1", "G");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("acceptfire1", "H");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("acceptfire1", "I");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("acceptfire1", "J");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("acceptfire1", "K");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("acceptfire1", "L");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("acceptfire1", "M");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("acceptfire1", "N");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("acceptfire1", "O");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("acceptfire1", "P");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("acceptfire1", "R");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("acceptfire1", "S");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("acceptfire1", "T");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("acceptfire1", "U");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("acceptfire1", "V");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("acceptfire1", "W");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("acceptfire1", "X");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("acceptfire1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("acceptfire1", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("acceptfire1", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("acceptfire1", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("acceptfire1", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("acceptfire1", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("acceptfire1", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("acceptfire1", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("acceptfire1", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("acceptfire1", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("acceptfire1", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
				Fire1_Text.text = "P1 Accept / Fire: ";
				Fire1_Text.text += PlayerPrefs.GetString("acceptfire1");
				Fire1.Select ();
				Shoot1bool = false;
			}

		}
		else if (Shoot2bool == true)
		{
			/*if (Input.anyKeyDown) 
			{
				PlayerPrefs.SetString("acceptfire2", Input.inputString);
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Shoot2bool = false;
				Fire2.Select ();
			}*/
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("acceptfire2", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("acceptfire2", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("acceptfire2", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("acceptfire2", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("acceptfire2", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("acceptfire2", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("acceptfire2", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("acceptfire2", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("acceptfire2", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("acceptfire2", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("acceptfire2", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("acceptfire2", "End");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("acceptfire2", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("acceptfire2", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("acceptfire2", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("acceptfire2", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("acceptfire2", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("acceptfire2", "A");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("acceptfire2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("acceptfire2", "C");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("acceptfire2", "D");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("acceptfire2", "E");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("acceptfire2", "F");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("acceptfire2", "G");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("acceptfire2", "H");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("acceptfire2", "I");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("acceptfire2", "J");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("acceptfire2", "K");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("acceptfire2", "L");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("acceptfire2", "M");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("acceptfire2", "N");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("acceptfire2", "O");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("acceptfire2", "P");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("acceptfire2", "R");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("acceptfire2", "S");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("acceptfire2", "T");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("acceptfire2", "U");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("acceptfire2", "V");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("acceptfire2", "W");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("acceptfire2", "X");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("acceptfire2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("acceptfire2", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("acceptfire2", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("acceptfire2", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("acceptfire2", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("acceptfire2", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("acceptfire2", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("acceptfire2", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("acceptfire2", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("acceptfire2", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("acceptfire2", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
				Fire2_Text.text = "P2 Accept / Fire: ";
				Fire2_Text.text += PlayerPrefs.GetString("acceptfire2");
				Fire2.Select ();
				Shoot2bool = false;
			}

		}
		else if (Turbo1bool == true)
		{
			/*if (Input.anyKeyDown)
			{
				PlayerPrefs.SetString("cancelturbo1", Input.inputString);
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1bool = false;
				Turbo1.Select ();
			}*/
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("cancelturbo1", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "End");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "A");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "C");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "D");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "E");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "F");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "G");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "H");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "I");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "J");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "K");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "L");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "M");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "N");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "O");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "P");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "R");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "S");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "T");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "U");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "V");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "W");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "X");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "B");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("cancelturbo1", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
				Turbo1_Text.text = "P1 Cancel / Turbo: ";
				Turbo1_Text.text += PlayerPrefs.GetString("cancelturbo1");
				Turbo1.Select ();
				Turbo1bool = false;
			}
		}
		else if (Turbo2bool == true)
		{
			if (Input.anyKeyDown)
			{
				PlayerPrefs.SetString("cancelturbo2", Input.inputString);
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2bool = false;
				Turbo2.Select ();
			}
			if (Input.GetKeyDown (KeyCode.Backspace)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Backspace");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Delete)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Delete");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Tab)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Tab");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Clear)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Clear");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Return)) 
			{
				
			}if (Input.GetKeyDown (KeyCode.Pause)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Pause");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Escape)) 
			{
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Space)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Space");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad0)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad0");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad1)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad1");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad2)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad2");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad3)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad3");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad4)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad4");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad5)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad5");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad6)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad6");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad7)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad7");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad8)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad8");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Keypad9)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Keypad9");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPeriod)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "KeypadPeriod");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadDivide)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "KeypadDivide");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMultiply)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "KeypadMultiply");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadMinus)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "KeypadMinus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadPlus)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "KeypadPlus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEnter)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "KeypadEnter");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.KeypadEquals)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "KeypadEquals");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.UpArrow)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "UpArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.DownArrow) )
			{
				PlayerPrefs.SetString("cancelturbo2", "DownArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightArrow)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "RightArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftArrow)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "LeftArrow");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Insert)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Insert");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Home)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Home");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.End)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "End");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageUp)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "PageUp");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.PageDown)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "PageDown");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F1)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F1");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F2)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F2");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F3)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F3");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F4)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F4");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F5)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F5");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F6)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F6");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F7)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F7");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F8)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F8");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F9)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F9");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F10)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F10");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F11)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F11");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F12)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F12");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F13)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F13");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F14)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F14");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.F15)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F15");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha0)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha0");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha1)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha1");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha2)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha2");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha3)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha3");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha4)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha4");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha5)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha5");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha6)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha6");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha7)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha7");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.Alpha8)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha8");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Alpha9)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Alpha9");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Quote)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Quote");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Asterisk)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Asterisk");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Plus)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Plus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Comma)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Comma");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Minus)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Minus");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Period)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Period");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Slash)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Slash");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Colon)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Colon");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Semicolon)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Semicolon");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Less)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Less");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Equals)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Equals");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Greater)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Greater");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Question)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Question");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftBracket)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "LeftBracket");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Backslash)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Backslash");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightBracket)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "RightBracket");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Caret)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Caret");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Underscore)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Underscore");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.BackQuote)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "BackQuote");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.A)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "A");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.C)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "C");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.D)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "D");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.E)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "E");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.F)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "F");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.G)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "G");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.H)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "H");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.I)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "I");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.J)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "J");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.K)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "K");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.L)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "L");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.M)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "M");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.N)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "N");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.O)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "O");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.P)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "P");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Q)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Q");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.R)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "R");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.S)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "S");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.T)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "T");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.U)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "U");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.V)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "V");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.W)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "W");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.X)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "X");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Y)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Y");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Z)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Z");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.B)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "B");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.Numlock)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "Numlock");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.CapsLock)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "CapsLock");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.ScrollLock)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "ScrollLock");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightShift)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "RightShift");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftShift)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "LeftShift");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.RightAlt)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "RightAlt");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}if (Input.GetKeyDown (KeyCode.LeftAlt)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "LeftAlt");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.RightControl)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "RightControl");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.LeftControl)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "LeftControl");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			if (Input.GetKeyDown (KeyCode.AltGr)) 
			{
				PlayerPrefs.SetString("cancelturbo2", "AltGr");
				Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
				Turbo2_Text.text = "P2 Cancel / Turbo: ";
				Turbo2_Text.text += PlayerPrefs.GetString("cancelturbo2");
				Turbo2.Select ();
				Turbo2bool = false;
			}
			
		}
	}



	public void ChangeKeyboardLayout()
	{
		Keyboard_Layout.interactable = false;
		Gamepad_Layout.interactable = false;
		Back.interactable = false;
		Up1.interactable = true;
		Up1.Select ();
		Up2.interactable = true;
		Right1.interactable = true;
		Right2.interactable = true;
		Left1.interactable = true;
		Left2.interactable = true;
		Down1.interactable = true;
		Down2.interactable = true;
		Fire1.interactable = true;
		Fire2.interactable = true;
		Turbo1.interactable = true;
		Turbo2.interactable = true;
		Save.interactable = true;
		Default.interactable = true;
	}


	public void changeUp1()
	{
		Up1bool = true;
		Empty.Select ();

	}

	public void changeUp2()
	{
		Up2bool = true;
		Empty.Select ();
	}

	public void changeRight1()
	{
		Right1bool = true;
		Empty.Select ();

	}


	public void changeRight2()
	{
		Right2bool = true;
		Empty.Select ();
	}

	public void changeDown1()
	{
		Down1bool = true;
		Empty.Select ();
	}


	public void changeDown2()
	{
		Down2bool = true;
		Empty.Select ();
	}

	public void changeLeft1()
	{
		Left1bool = true;
		Empty.Select ();
	}

	public void changeLeft2()
	{
		Left2bool = true;
		Empty.Select ();
	}

	public void changefire2()
	{
		Shoot2bool = true;
		Empty.Select ();
	}

	public void changefire1()
	{
		Shoot1bool = true;
		Empty.Select ();
	}

	public void changeturbo1()
	{
		Turbo1bool = true;
		Empty.Select ();
	}

	public void changeturbo2()
	{
		Turbo2bool = true;
		Empty.Select ();
	}

	public void DefaultFunction()
	{
		// Restauramos los valores de las playerprefs a default y los cargamos
		PlayerPrefs.SetString("Up1", "UpArrow");
		Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
		PlayerPrefs.SetString("Right1", "RightArrow");
		Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
		PlayerPrefs.SetString("Left1", "LeftArrow");
		Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
		PlayerPrefs.SetString("Down1", "DownArrow");
		Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
		PlayerPrefs.SetString("acceptfire1", "RightControl");
		Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
		PlayerPrefs.SetString("cancelturbo1", "RightShift");
		Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
		
		PlayerPrefs.SetString("Up2", "W");
		Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
		PlayerPrefs.SetString("Right2", "D");
		Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
		PlayerPrefs.SetString("Left2", "A");
		Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
		PlayerPrefs.SetString("Down2", "S");
		Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
		PlayerPrefs.SetString("acceptfire2", "F");
		Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
		PlayerPrefs.SetString("cancelturbo2", "G");
		Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));


		//Salimos del keyboard layout
		PlayerPrefs.Save ();
		Keyboard_Layout.interactable = true;
		Keyboard_Layout.Select ();
		Gamepad_Layout.interactable = true;
		Back.interactable = true;
		Up1.interactable = false;
		Up2.interactable = false;
		Right1.interactable = false;
		Right2.interactable = false;
		Left1.interactable = false;
		Left2.interactable = false;
		Down1.interactable = false;
		Down2.interactable = false;
		Fire1.interactable = false;
		Fire2.interactable = false;
		Turbo1.interactable = false;
		Turbo2.interactable = false;
		Save.interactable = false;
		Default.interactable = false;



	}

	public void SaveFunction()
	{

		//Guardamos y aplicamos los cambios a las playerprefs
		Controls_Manager.GetComponent<Controls_Manager>().Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
		Controls_Manager.GetComponent<Controls_Manager>().Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
		Controls_Manager.GetComponent<Controls_Manager>().Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
		Controls_Manager.GetComponent<Controls_Manager>().Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
		Controls_Manager.GetComponent<Controls_Manager>().acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
		Controls_Manager.GetComponent<Controls_Manager>().cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));
		
		Controls_Manager.GetComponent<Controls_Manager>().Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
		Controls_Manager.GetComponent<Controls_Manager>().Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
		Controls_Manager.GetComponent<Controls_Manager>().Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
		Controls_Manager.GetComponent<Controls_Manager>().Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
		Controls_Manager.GetComponent<Controls_Manager>().acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
		Controls_Manager.GetComponent<Controls_Manager>().cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));



		PlayerPrefs.Save ();
		Keyboard_Layout.interactable = true;
		Keyboard_Layout.Select ();
		Gamepad_Layout.interactable = true;
		Back.interactable = true;
		Up1.interactable = false;
		Up2.interactable = false;
		Right1.interactable = false;
		Right2.interactable = false;
		Left1.interactable = false;
		Left2.interactable = false;
		Down1.interactable = false;
		Down2.interactable = false;
		Fire1.interactable = false;
		Fire2.interactable = false;
		Turbo1.interactable = false;
		Turbo2.interactable = false;
		Save.interactable = false;
		Default.interactable = false;
	}

	public void BackFunction()
	{
		SceneManager.LoadScene ("menu");

	}


	public void GamepadLayout()
	{
		Gamepad_Button.Select ();
		Color c = Gamepad.color;
		c.a = 255;
		Gamepad.color = c;
	}

	public void ExitGamepad()
	{
		Color c = Gamepad.color;
		c.a = 0;
		Gamepad.color = c;
		Gamepad_Layout.Select ();
	}

}
