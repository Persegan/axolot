﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class Tutorial_Manager : MonoBehaviour {

    public Text panel_text;
    public Image panel_image;
    public Image controls_image;
    public Image controls_image2;

    public List<string> Before_controls_messages;
    public List<string> After_controls_messages;

    public GameObject Powerups1;
    public GameObject Powerups2;
    public GameObject Tutorial_Powerup;

    private int state = 0;
    private bool auxiliar = false;
    private bool auxiliar2 = false;

    // Use this for initialization
    void Start () {
        panel_text.text = Before_controls_messages[0];
        state++;	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (auxiliar2 == true)
            {
                panel_image.enabled = false;
                panel_text.text = "";
            }
           else if (state < After_controls_messages.Count && auxiliar == true)
            {
                if (state == 1)
                {
                    //Handle tutorial powerup instantiation
                    Tutorial_Powerup.SetActive(true);

                }
                    controls_image.enabled = false;
                    controls_image2.enabled = false;
                    panel_image.enabled = true;
                    panel_text.text = After_controls_messages[state];
                    state++;
            }
            else if (state < Before_controls_messages.Count && auxiliar == false)
            {

                panel_text.text = Before_controls_messages[state];
                state++;
            }
            else if (state >= Before_controls_messages.Count && auxiliar == false)
            {
                panel_image.enabled = false;
                panel_text.text = "";
                controls_image.enabled = true;
                controls_image2.enabled = true;
                state = 0;
                auxiliar = true;
            }
            else if (state >= After_controls_messages.Count && auxiliar == true)
            {
                panel_image.enabled = false;
                panel_text.text = "";
                Powerups1.SetActive(true);
                Powerups2.SetActive(true);
                //StartCoroutine(Countdown());
            }
            else
            {

            }
        }
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			SceneManager.LoadScene("menu");
		}
	}

    IEnumerator Countdown()
    {
        yield return new WaitForSeconds(10.0f);
        Powerups1.SetActive(true);
        Powerups2.SetActive(true);
        panel_image.enabled = true;
        panel_text.text = "";
        auxiliar2 = true;
    }
}
