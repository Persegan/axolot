﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Mega_death_rocket : MonoBehaviour {

	public GameObject ship;
	public GameObject explosion;
	//public float rotation_speed;     	  // how fast the missile turns
	//public float angle;              	  // the angle at which the missile will start
	public float speed;					// the speed at which the missile will travel
	private bool fired;					// indicates if the missile has been fired
	public float time_life;     		// if the time is not 0, it will explode after that duartion
	public bool detonatable;			// if true, the player can make  the missile explode at any time
	public bool destructable;    		// if true, the enemy can shoot down the missile
	public float blast_radius;			// the area in which the missile will deal damage when explodes
	private bool exploded;
	private float duration;   			// used to count how much time has elapsed sine it was fired
	public Sprite icon;
	
	// Use this for initialization
	void Start () {
		fired = false;
		exploded = false;
		transform.position = ship.transform.position;
		transform.rotation = ship.transform.rotation;
		duration = time_life;
		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = "";	// this is for the ammo UI

	}
	
	
	void Update()
	{
		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire)) 
		{
			if (fired == false)
			{
				fired = true;
			}
			else if (detonatable == true)
			{
				exploded = true;
			}
		}
		
	}
	
	void FixedUpdate () {
		if (fired == false) {
			transform.position = ship.transform.position;
			transform.rotation = ship.transform.rotation;			
		}
		else 
		{	
			foreach (Collider2D coll in GetComponents<Collider2D>())
				coll.enabled = true;			// the power up can collide with things only after it is fired

			if (duration > 0)
			{
				duration -= Time.fixedDeltaTime;
				GetComponent<Rigidbody2D>().AddForce (transform.up * speed);   // while the missile is flying, you can control it
			}
			else if (duration < 0)
			{
				exploded = true;
			}
			if (exploded == true && detonatable == true)
			{
				detonate ();
			}

		}

	}
	
	
	void OnCollisionEnter2D(Collision2D collider)	
	{
		if (collider.gameObject != ship && fired == true)
		{
			if(collider.gameObject.tag == "jugador" || collider.gameObject.tag == "satellite" ||  collider.gameObject.tag == "mega_turbo_killer"
				|| collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "shield" || collider.gameObject.tag == "terrain" ){		// it explodes normally when it contacts these

				if (collider.gameObject.tag == "shield") {
					if (collider.gameObject.GetComponent<Shield> ().ship == ship) {

					} else {
						detonate ();
					}
				} else {
					detonate();
				}
			
			}
			else if(collider.gameObject.tag == "reflector"){		// a small explosion, to indicate it is destroyed
				ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
				ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
				Destroy (gameObject);
			}
		}
	}


	void OnTriggerEnter2D(Collider2D collider)	
	{
		if (collider.gameObject != ship && fired == true)
		{
			if(collider.gameObject.tag == "jugador" || collider.gameObject.tag == "satellite" ||  collider.gameObject.tag == "mega_turbo_killer"
				|| collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "shield" || collider.gameObject.tag == "terrain" ){		// it explodes normally when it contacts these

				if (collider.gameObject.tag == "shield") {
					if (collider.gameObject.GetComponent<Shield> ().ship == ship) {

					} else {
						detonate ();
					}
				} else {
					detonate();
				}

			}
			else if(collider.gameObject.tag == "reflector"){		// a small explosion, to indicate it is destroyed
				ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
				ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
				Destroy (gameObject);
			}
		}
	}


	void detonate(){
		GameObject temp = Instantiate (explosion, transform.position, new Quaternion ()) as GameObject;
		temp.GetComponent<missile_explosion> ().ship = ship;
		ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
		ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
		Destroy (gameObject);
	}
}