﻿


using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player_Selection_Screen : MonoBehaviour {
	
	//public GameObject Controls_Manager;
	public Text FiretoJoin1;
	public Text FiretoJoin2;
	public Text FiretoJoin3;
	public Text FiretoJoin4;

	public GameObject Player1Text;
	public GameObject Player2Text;
	public GameObject Player3Text;
	public GameObject Player4Text;

	public GameObject Player1ReadyText;
	public GameObject Player2ReadyText;
	public GameObject Player3ReadyText;
	public GameObject Player4ReadyText;

	public GameObject LeftArrow1;
	public GameObject LeftArrow2;
	public GameObject LeftArrow3;
	public GameObject LeftArrow4;

	public GameObject RightArrow1;
	public GameObject RightArrow2;
	public GameObject RightArrow3;
	public GameObject RightArrow4;

	public GameObject Player1Skin;
	public GameObject Player2Skin;
	public GameObject Player3Skin;
	public GameObject Player4Skin;

	public Player player1;
	public Player player2;
	public Player player3;
	public Player player4;

	public KeyCode Up1;
	public KeyCode Right1;
	public KeyCode Left1;
	public KeyCode Down1;
	public KeyCode acceptfire1;
	public KeyCode cancelturbo1;
	
	public KeyCode Up2;
	public KeyCode Right2;
	public KeyCode Left2;
	public KeyCode Down2;
	public KeyCode acceptfire2;
	public KeyCode cancelturbo2;

	public const int keyboard1 = 1;
	public const int keyboard2 = 2;
	public const int gamepad1 = 3;
	public const int gamepad2 = 4;
	public const int gamepad3 = 5;
	public const int gamepad4 = 6;

	public const int maxskins = 10;

	public AudioSource blip;
	public AudioSource laser;

	public int joinedplayers = 0;
	private bool joystick1available;
	private bool joystick2available;
	private bool joystick3available;
	private bool joystick4available;



	// Use this for initialization
	void Start () 
	{
		Up1 = Controls_Manager.controls_manager.Up1;
		Up2 = Controls_Manager.controls_manager.Up2;
		Right1 = Controls_Manager.controls_manager.Right1;
		Right2 = Controls_Manager.controls_manager.Right2;
		Left1 = Controls_Manager.controls_manager.Left1;
		Left2 = Controls_Manager.controls_manager.Left2;
		Down1 = Controls_Manager.controls_manager.Down1;
		Down2 = Controls_Manager.controls_manager.Down2;
		acceptfire1= Controls_Manager.controls_manager.acceptfire1;
		acceptfire2 = Controls_Manager.controls_manager.acceptfire2;
		cancelturbo1= Controls_Manager.controls_manager.cancelturbo1;
		cancelturbo2 = Controls_Manager.controls_manager.cancelturbo2;	

		player1 = new Player ();
		player2 = new Player();
		player3 = new Player();
		player4 = new Player();

	}

	void Awake()
	{
		if (Music_Manager.music_manager.Song4.isPlaying == true) 
		{

		} else 
		{
			Music_Manager.music_manager.Song4.loop = true;
			Music_Manager.music_manager.Song4.Play ();

		}

	
		//Controls_Manager = Controls_Manager;
	}
	
	// Update is called once per frame
	void Update () 
	{

		if (Input.GetKeyDown(KeyCode.Escape))
		{	 
			SceneManager.LoadScene ("Mode Selection Screen");

		}
		if (player1.controller == keyboard1)
		{
			if (player1.ready == false)
			{
				if (Input.GetKeyDown(acceptfire1))
				{
					player1.ready = true;
				}
				if (Input.GetKeyDown(Right1))
				{
					blip.Play ();
					if (player1.skin == maxskins)
					{
						player1.skin = 0;
					}
					else
					{
						player1.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left1))
				{
					blip.Play ();
					if (player1.skin == 0)
					{
						player1.skin = maxskins;
					}
					else
					{
						player1.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo1))
			{
				if (player1.ready == false)
				{
					
					player1.controller = 0;
					LeftArrow1.SetActive (false);
					RightArrow1.SetActive (false);
					Player1Text.SetActive (false);
					Player1Skin.SetActive (false);
					FiretoJoin1.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player1.ready = false;
				}
				
			}
			
		}
		
		if (player1.controller == keyboard2)
		{
			if (player1.ready == false)
			{
				if (Input.GetKeyDown(acceptfire2))
				{
					player1.ready = true;
				}
				if (Input.GetKeyDown(Right2))
				{
					blip.Play ();
					if (player1.skin == maxskins)
					{
						player1.skin = 0;
					}
					else
					{
						player1.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left2))
				{
					blip.Play ();
					if (player1.skin == 0)
					{
						player1.skin = maxskins;
					}
					else
					{
						player1.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo2))
			{
				if (player1.ready == false)
				{
					player1.controller = 0;
					LeftArrow1.SetActive (false);
					RightArrow1.SetActive (false);
					Player1Text.SetActive (false);
					Player1Skin.SetActive (false);
					FiretoJoin1.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player1.ready = false;
				}
				
			}
			
		}
		if (player1.controller == gamepad1)
		{
			if (player1.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button0))
				{
					player1.ready = true;
				}
				if (Input.GetAxisRaw("joystick 1 x")== 1 || Input.GetAxisRaw("joystick 1 dx") == 1)
				{
					
					if (joystick1available == true)
					{
						blip.Play ();
						if (player1.skin == maxskins)
						{
							player1.skin = 0;
							joystick1available = false;
						}
						else
						{
							player1.skin++;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== -1 || Input.GetAxisRaw("joystick 1 dx") == -1)
				{
					
					if (joystick1available == true)
					{
						blip.Play ();
						if (player1.skin == 0)
						{
							player1.skin = maxskins;
							joystick1available = false;
						}
						else
						{
							player1.skin--;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== 0 && Input.GetAxisRaw("joystick 1 dx") == 0)
				{
					joystick1available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick1Button1))
			{
				if (player1.ready == false)
				{
					player1.controller = 0;
					LeftArrow1.SetActive (false);
					RightArrow1.SetActive (false);
					Player1Text.SetActive (false);
					Player1Skin.SetActive (false);
					FiretoJoin1.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player1.ready = false;
				}
				
			}
			
		}
		if (player1.controller == gamepad2)
		{
			if (player1.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button0))
				{
					player1.ready = true;
				}
				if (Input.GetAxisRaw("joystick 2 x")== 1 || Input.GetAxisRaw("joystick 2 dx") == 1)
				{

					if (joystick2available == true)
					{
						blip.Play ();
						if (player1.skin == maxskins)
						{
							player1.skin = 0;
							joystick2available = false;
						}
						else
						{
							player1.skin++;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== -1 || Input.GetAxisRaw("joystick 2 dx") == -1)
				{
					blip.Play ();
					if (joystick2available == true)
					{
						if (player1.skin == 0)
						{
							player1.skin = maxskins;
							joystick2available = false;
						}
						else
						{
							player1.skin--;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== 0 && Input.GetAxisRaw("joystick 2 dx") == 0)
				{
					joystick2available = true;
				}
			}
			
			if (Input.GetKeyDown(KeyCode.Joystick2Button1))
			{
				if (player1.ready == false)
				{
					player1.controller = 0;
					LeftArrow1.SetActive (false);
					RightArrow1.SetActive (false);
					Player1Text.SetActive (false);
					Player1Skin.SetActive (false);
					FiretoJoin1.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player1.ready = false;
				}
				
			}
			
		}
		if (player1.controller == gamepad3)
		{
			if (player1.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick3Button0))
				{
					player1.ready = true;
				}
				if (Input.GetAxisRaw("joystick 3 x")== 1 || Input.GetAxisRaw("joystick 3 dx") == 1)
				{
					
					if (joystick3available == true)
					{
						blip.Play ();
						if (player1.skin == maxskins)
						{
							player1.skin = 0;
							joystick3available = false;
						}
						else
						{
							player1.skin++;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== -1 || Input.GetAxisRaw("joystick 3 dx") == -1)
				{
					
					if (joystick3available == true)
					{
						blip.Play ();
						if (player1.skin == 0)
						{
							player1.skin = maxskins;
							joystick3available = false;
						}
						else
						{
							player1.skin--;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== 0 && Input.GetAxisRaw("joystick 3 dx") == 0)
				{
					joystick3available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick3Button1))
			{
				if (player1.ready == false)
				{
					player1.controller = 0;
					LeftArrow1.SetActive (false);
					RightArrow1.SetActive (false);
					Player1Text.SetActive (false);
					Player1Skin.SetActive (false);
					FiretoJoin1.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player1.ready = false;
				}
				
			}
			
		}
		if (player1.controller == gamepad4)
		{
			if (player1.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick4Button0))
				{
					player1.ready = true;
				}
				if (Input.GetAxisRaw("joystick 4 x")== 1 || Input.GetAxisRaw("joystick 4 dx") == 1)
				{
			
					if (joystick4available == true)
					{
						blip.Play ();
						if (player1.skin == maxskins)
						{
							player1.skin = 0;
							joystick4available = false;
						}
						else
						{
							player1.skin++;
							joystick4available = false;
						}
					}
				}
				if (Input.GetAxisRaw("joystick 4 x")== -1 || Input.GetAxisRaw("joystick 4 dx") == -1)
				{
			
					if (joystick4available == true)
					{
						blip.Play ();
						if (player1.skin == 0)
						{
							player1.skin = maxskins;
							joystick4available = false;
						}
						else
						{
							player1.skin--;
							joystick4available = false;
						}
					}
				}
				if (Input.GetAxisRaw("joystick 4 x")== 0 && Input.GetAxisRaw("joystick 4 dx") == 0)
				{
					joystick4available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick4Button1))
			{
				if (player1.ready == false)
				{
					player1.controller = 0;
					LeftArrow1.SetActive (false);
					RightArrow1.SetActive (false);
					Player1Text.SetActive (false);
					Player1Skin.SetActive (false);
					FiretoJoin1.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player1.ready = false;
				}
				
			}
			
		}
		
		
		
		//Player 2
		
		if (player2.controller == keyboard1)
		{
			if (player2.ready == false)
			{
				if (Input.GetKeyDown(acceptfire1))
				{
					player2.ready = true;
				}
				if (Input.GetKeyDown(Right1))
				{
					blip.Play ();
					if (player2.skin == maxskins)
					{
						player2.skin = 0;
					}
					else
					{
						player2.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left1))
				{
					blip.Play ();
					if (player2.skin == 0)
					{
						player2.skin = maxskins;
					}
					else
					{
						player2.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo1))
			{
				if (player2.ready == false)
				{
					player2.controller = 0;
					LeftArrow2.SetActive (false);
					RightArrow2.SetActive (false);
					Player2Text.SetActive (false);
					Player2Skin.SetActive (false);
					FiretoJoin2.text = "Press Fire to Join ";
					joinedplayers--;
				}
				
				else
				{
					player2.ready = false;
				}
				
			}
			
		}
		
		if (player2.controller == keyboard2)
		{
			if (player2.ready == false)
			{
				if (Input.GetKeyDown(acceptfire2))
				{
					player2.ready = true;
				}
				if (Input.GetKeyDown(Right2))
				{
					blip.Play ();
					if (player2.skin == maxskins)
					{
						player2.skin = 0;
					}
					else
					{
						player2.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left2))
				{
					blip.Play ();
					if (player2.skin == 0)
					{
						player2.skin = maxskins;
					}
					else
					{
						player2.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo2))
			{
				if (player2.ready == false)
				{
					player2.controller = 0;
					LeftArrow2.SetActive (false);
					RightArrow2.SetActive (false);
					Player2Text.SetActive (false);
					Player2Skin.SetActive (false);
					FiretoJoin2.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player2.ready = false;
				}
				
			}
			
		}
		if (player2.controller == gamepad1)
		{
			if (player2.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button0))
				{
					player2.ready = true;
				}
				if (Input.GetAxisRaw("joystick 1 x")== 1 || Input.GetAxisRaw("joystick 1 dx") == 1)
				{
					
					if (joystick1available == true)
					{
						blip.Play ();
						if (player2.skin == maxskins)
						{
							player2.skin = 0;
							joystick1available = false;
						}
						else
						{
							player2.skin++;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== -1 || Input.GetAxisRaw("joystick 1 dx") == -1)
				{
					
					if (joystick1available == true)
					{
						blip.Play ();
						if (player2.skin == 0)
						{
							player2.skin = maxskins;
							joystick1available = false;
						}
						else
						{
							player2.skin--;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== 0 && Input.GetAxisRaw("joystick 1 dx") == 0)
				{
					joystick1available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick1Button1))
			{
				if (player2.ready == false)
				{
					player2.controller = 0;
					LeftArrow2.SetActive (false);
					RightArrow2.SetActive (false);
					Player2Text.SetActive (false);
					Player2Skin.SetActive (false);
					FiretoJoin2.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player2.ready = false;
				}
				
			}
			
		}
		if (player2.controller == gamepad2)
		{
			if (player2.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button0))
				{
					player2.ready = true;
				}
				if (Input.GetAxisRaw("joystick 2 x")== 1 || Input.GetAxisRaw("joystick 2 dx") == 1)
				{

					if (joystick2available == true)
					{
						blip.Play ();
						if (player2.skin == maxskins)
						{
							player2.skin = 0;
							joystick2available = false;
						}
						else
						{
							player2.skin++;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== -1 || Input.GetAxisRaw("joystick 2 dx") == -1)
				{
		
					if (joystick2available == true)
					{
						blip.Play ();
						if (player2.skin == 0)
						{
							player2.skin = maxskins;
							joystick2available = false;
						}
						else
						{
							player2.skin--;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== 0 && Input.GetAxisRaw("joystick 2 dx") == 0)
				{
					joystick2available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick2Button1))
			{
				if (player2.ready == false)
				{
					player2.controller = 0;
					LeftArrow2.SetActive (false);
					RightArrow2.SetActive (false);
					Player2Text.SetActive (false);
					Player2Skin.SetActive (false);
					FiretoJoin2.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player2.ready = false;
				}
				
			}
			
		}
		if (player2.controller == gamepad3)
		{
			if (player2.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick3Button0))
				{
					player2.ready = true;
				}
				if (Input.GetAxisRaw("joystick 3 x")== 1 || Input.GetAxisRaw("joystick 3 dx") == 1)
				{

					if (joystick3available == true)
					{
						blip.Play ();
						if (player2.skin == maxskins)
						{
							player2.skin = 0;
							joystick3available = false;
						}
						else
						{
							player2.skin++;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== -1 || Input.GetAxisRaw("joystick 3 dx") == -1)
				{
					
					if (joystick3available == true)
					{
						blip.Play ();
						if (player2.skin == 0)
						{
							player2.skin = maxskins;
							joystick3available = false;
						}
						else
						{
							player2.skin--;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== 0 && Input.GetAxisRaw("joystick 3 dx") == 0)
				{
					joystick3available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick3Button1))
			{
				if (player2.ready == false)
				{
					player2.controller = 0;
					LeftArrow2.SetActive (false);
					RightArrow2.SetActive (false);
					Player2Text.SetActive (false);
					Player2Skin.SetActive (false);
					FiretoJoin2.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player2.ready = false;
				}
				
			}
			
		}
		if (player2.controller == gamepad4)
		{
			if (player2.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick4Button0))
				{
					player2.ready = true;
				}
				if (Input.GetAxisRaw("joystick 4 x")== 1 || Input.GetAxisRaw("joystick 4 dx") == 1)
				{

					if (joystick4available == true)
					{
						blip.Play ();
						if (player2.skin == maxskins)
						{
							player2.skin = 0;
							joystick4available = false;
						}
						else
						{
							player2.skin++;
							joystick4available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 4 x")== -1 || Input.GetAxisRaw("joystick 4 dx") == -1)
				{

					if (joystick4available == true)
					{
						blip.Play ();
						if (player2.skin == 0)
						{
							player2.skin = maxskins;
							joystick4available = false;
						}
						else
						{
							player2.skin--;
							joystick4available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 4 x")== 0 && Input.GetAxisRaw("joystick 4 dx") == 0)
				{
					joystick4available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick4Button1))
			{
				if (player2.ready == false)
				{
					player2.controller = 0;
					LeftArrow2.SetActive (false);
					RightArrow2.SetActive (false);
					Player2Text.SetActive (false);
					Player2Skin.SetActive (false);
					FiretoJoin2.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player2.ready = false;
				}
				
			}
			
		}
		
		
		
		//Player 3
		if (player3.controller == keyboard1)
		{
			if (player3.ready == false)
			{
				if (Input.GetKeyDown(acceptfire1))
				{
					player3.ready = true;
				}
				if (Input.GetKeyDown(Right1))
				{
					blip.Play ();
					if (player3.skin == maxskins)
					{
						player3.skin = 0;
					}
					else
					{
						player3.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left1))
				{
					blip.Play ();
					if (player3.skin == 0)
					{
						player3.skin = maxskins;
					}
					else
					{
						player3.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo1))
			{
				if (player3.ready == false)
				{
					player3.controller = 0;
					LeftArrow3.SetActive (false);
					RightArrow3.SetActive (false);
					Player3Text.SetActive (false);
					Player3Skin.SetActive (false);
					FiretoJoin3.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player3.ready = false;
				}
				
			}
			
		}
		
		if (player3.controller == keyboard2)
		{
			if (player3.ready == false)
			{
				if (Input.GetKeyDown(acceptfire2))
				{
					player3.ready = true;
				}
				if (Input.GetKeyDown(Right2))
				{
					blip.Play ();
					if (player3.skin == maxskins)
					{
						player3.skin = 0;
					}
					else
					{
						player3.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left2))
				{
					blip.Play ();
					if (player3.skin == 0)
					{
						player3.skin = maxskins;
					}
					else
					{
						player3.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo2))
			{
				if (player3.ready == false)
				{
					player3.controller = 0;
					LeftArrow3.SetActive (false);
					RightArrow3.SetActive (false);
					Player3Text.SetActive (false);
					Player3Skin.SetActive (false);
					FiretoJoin3.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player3.ready = false;
				}
				
			}
			
		}
		if (player3.controller == gamepad1)
		{
			if (player3.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button0))
				{
					player3.ready = true;
				}
				if (Input.GetAxisRaw("joystick 1 x")== 1 || Input.GetAxisRaw("joystick 1 dx") == 1)
				{

					if (joystick1available == true)
					{
						if (player3.skin == maxskins)
						{
							blip.Play ();
							player3.skin = 0;
							joystick1available = false;
						}
						else
						{
							player3.skin++;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== -1 || Input.GetAxisRaw("joystick 1 dx") == -1)
				{
					if (joystick1available == true)
					{	
						blip.Play ();
						if (player3.skin == 0)
						{
							player3.skin = maxskins;
							joystick1available = false;
						}
						else
						{
							player3.skin--;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== 0 && Input.GetAxisRaw("joystick 1 dx") == 0)
				{
					joystick1available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick1Button1))
			{
				if (player3.ready == false)
				{
					player3.controller = 0;
					LeftArrow3.SetActive (false);
					RightArrow3.SetActive (false);
					Player3Text.SetActive (false);
					Player3Skin.SetActive (false);
					FiretoJoin3.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player3.ready = false;
				}
				
			}
			
		}
		if (player3.controller == gamepad2)
		{
			if (player3.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button0))
				{
					player3.ready = true;
				}
				if (Input.GetAxisRaw("joystick 2 x")== 1 || Input.GetAxisRaw("joystick 2 dx") == 1)
				{
					if (joystick2available == true)
					{
						blip.Play ();
						if (player3.skin == maxskins)
						{
							player3.skin = 0;
							joystick2available = false;
						}
						else
						{
							player3.skin++;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== -1 || Input.GetAxisRaw("joystick 2 dx") == -1)
				{
					if (joystick2available == true)
					{
						blip.Play ();
						if (player3.skin == 0)
						{
							player3.skin = maxskins;
							joystick2available = false;
						}
						else
						{
							player3.skin--;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== 0 && Input.GetAxisRaw("joystick 2 dx") == 0)
				{
					joystick2available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick2Button1))
			{
				if (player3.ready == false)
				{
					player3.controller = 0;
					LeftArrow3.SetActive (false);
					RightArrow3.SetActive (false);
					Player3Text.SetActive (false);
					Player3Skin.SetActive (false);
					FiretoJoin3.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player3.ready = false;
				}
				
			}
			
		}
		if (player3.controller == gamepad3)
		{
			if (player3.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick3Button0))
				{
					player3.ready = true;
				}
				if (Input.GetAxisRaw("joystick 3 x")== 1 || Input.GetAxisRaw("joystick 3 dx") == 1)
				{
					if (joystick3available == true)
					{
						blip.Play ();
						if (player3.skin == maxskins)
						{
							player3.skin = 0;
							joystick3available = false;
						}
						else
						{
							player3.skin++;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== -1 || Input.GetAxisRaw("joystick 3 dx") == -1)
				{
					if (joystick3available == true)
					{
						blip.Play ();
						if (player3.skin == 0)
						{
							player3.skin = maxskins;
							joystick3available = false;
						}
						else
						{
							player3.skin--;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== 0 && Input.GetAxisRaw("joystick 3 dx") == 0)
				{
					joystick3available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick3Button1))
			{
				if (player3.ready == false)
				{
					player3.controller = 0;
					LeftArrow3.SetActive (false);
					RightArrow3.SetActive (false);
					Player3Text.SetActive (false);
					Player3Skin.SetActive (false);
					FiretoJoin3.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player3.ready = false;
				}
				
			}
			
		}
		if (player3.controller == gamepad4)
		{
			if (player3.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick4Button0))
				{
					player3.ready = true;
				}
				if (Input.GetAxisRaw("joystick 4 x")== 1 || Input.GetAxisRaw("joystick 4 dx") == 1)
				{
					if (joystick4available == true)
					{
						blip.Play ();
						if (player3.skin == maxskins)
						{
							player3.skin = 0;
							joystick4available = false;
						}
						else
						{
							player3.skin++;
							joystick4available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 4 x")== -1 || Input.GetAxisRaw("joystick 4 dx") == -1)
				{
					if (joystick4available == true)
					{
						blip.Play ();
						if (player3.skin == 0)
						{
							player3.skin = maxskins;
							joystick4available = false;
						}
						else
						{
							player3.skin--;
							joystick4available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 4 x")== 0 && Input.GetAxisRaw("joystick 4 dx") == 0)
				{
					joystick4available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick4Button1))
			{
				if (player3.ready == false)
				{
					player3.controller = 0;
					LeftArrow3.SetActive (false);
					RightArrow3.SetActive (false);
					Player3Text.SetActive (false);
					Player3Skin.SetActive (false);
					FiretoJoin3.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player3.ready = false;
				}
				
			}
			
		}
		
		
		//Player 4
		if (player4.controller == keyboard1)
		{
			if (player4.ready == false)
			{
				if (Input.GetKeyDown(acceptfire1))
				{
					player4.ready = true;
				}
				if (Input.GetKeyDown(Right1))
				{
					blip.Play ();
					if (player4.skin == maxskins)
					{
						player4.skin = 0;
					}
					else
					{
						player4.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left1))
				{
					blip.Play ();
					if (player4.skin == 0)
					{
						player4.skin = maxskins;
					}
					else
					{
						player4.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo1))
			{
				if (player4.ready == false)
				{
					player4.controller = 0;
					LeftArrow4.SetActive (false);
					RightArrow4.SetActive (false);
					Player4Text.SetActive (false);
					Player4Skin.SetActive (false);
					FiretoJoin4.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player4.ready = false;
				}
				
			}
			
		}
		
		if (player4.controller == keyboard2)
		{
			if (player4.ready == false)
			{
				if (Input.GetKeyDown(acceptfire2))
				{
					player4.ready = true;
				}
				if (Input.GetKeyDown(Right2))
				{
					blip.Play ();
					if (player4.skin == maxskins)
					{
						player4.skin = 0;
					}
					else
					{
						player4.skin++;
					}
					
				}
				if (Input.GetKeyDown(Left2))
				{
					blip.Play ();
					if (player4.skin == 0)
					{
						player4.skin = maxskins;
					}
					else
					{
						player4.skin--;
					}
					
				}
			}
			if (Input.GetKeyDown(cancelturbo2))
			{
				if (player4.ready == false)
				{
					player4.controller = 0;
					LeftArrow4.SetActive (false);
					RightArrow4.SetActive (false);
					Player4Text.SetActive (false);
					Player4Skin.SetActive (false);
					FiretoJoin4.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player4.ready = false;
				}
				
			}
			
		}
		if (player4.controller == gamepad1)
		{
			if (player4.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick1Button0))
				{
					player4.ready = true;
				}
				if (Input.GetAxisRaw("joystick 1 x")== 1 || Input.GetAxisRaw("joystick 1 dx") == 1)
				{
		
					if (joystick1available == true)
					{
						blip.Play ();
						if (player4.skin == maxskins)
						{
							player4.skin = 0;
							joystick1available = false;
						}
						else
						{
							player4.skin++;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== -1 || Input.GetAxisRaw("joystick 1 dx") == -1)
				{
					
					if (joystick1available == true)
					{
						blip.Play ();
						if (player4.skin == 0)
						{
							player4.skin = maxskins;
							joystick1available = false;
						}
						else
						{
							player4.skin--;
							joystick1available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 1 x")== 0 && Input.GetAxisRaw("joystick 1 dx") == 0)
				{
					joystick4available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick1Button1))
			{
				if (player4.ready == false)
				{
					player4.controller = 0;
					LeftArrow4.SetActive (false);
					RightArrow4.SetActive (false);
					Player4Text.SetActive (false);
					Player4Skin.SetActive (false);
					FiretoJoin4.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player4.ready = false;
				}
				
			}
			
		}
		if (player4.controller == gamepad2)
		{
			if (player4.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick2Button0))
				{
					player4.ready = true;
				}
				if (Input.GetAxisRaw("joystick 2 x")== 1 || Input.GetAxisRaw("joystick 2 dx") == 1)
				{
					if (joystick2available == true)
					{
						blip.Play ();
						if (player4.skin == maxskins)
						{
							player4.skin = 0;
							joystick2available = false;
						}
						else
						{
							player4.skin++;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== -1 || Input.GetAxisRaw("joystick 2 dx") == -1)
				{
					if (joystick2available == true)
					{
						blip.Play ();
						if (player4.skin == 0)
						{
							player4.skin = maxskins;
							joystick2available = false;
						}
						else
						{
							player4.skin--;
							joystick2available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 2 x")== 0 && Input.GetAxisRaw("joystick 2 dx") == 0)
				{
					joystick2available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick2Button1))
			{
				if (player4.ready == false)
				{
					player4.controller = 0;
					LeftArrow4.SetActive (false);
					RightArrow4.SetActive (false);
					Player4Text.SetActive (false);
					Player4Skin.SetActive (false);
					FiretoJoin4.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player4.ready = false;
				}
				
			}
			
		}
		if (player4.controller == gamepad3)
		{
			if (player4.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick3Button0))
				{
					player4.ready = true;
				}
				if (Input.GetAxisRaw("joystick 3 x")== 1 || Input.GetAxisRaw("joystick 3 dx") == 1)
				{
					if (joystick3available == true)
					{
						blip.Play ();
						if (player4.skin == maxskins)
						{
							player4.skin = 0;
							joystick3available = false;
						}
						else
						{
							player4.skin++;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== -1 || Input.GetAxisRaw("joystick 3 dx") == -1)
				{
					if (joystick3available == true)
					{
						blip.Play ();
						if (player4.skin == 0)
						{
							player4.skin = maxskins;
							joystick3available = false;
						}
						else
						{
							player4.skin--;
							joystick3available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 3 x")== 0 && Input.GetAxisRaw("joystick 3 dx") == 0)
				{
					joystick3available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick3Button1))
			{
				if (player4.ready == false)
				{
					player4.controller = 0;
					LeftArrow4.SetActive (false);
					RightArrow4.SetActive (false);
					Player4Text.SetActive (false);
					Player4Skin.SetActive (false);
					FiretoJoin4.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player4.ready = false;
				}
				
			}
			
		}
		if (player4.controller == gamepad4)
		{
			if (player4.ready == false)
			{
				if (Input.GetKeyDown(KeyCode.Joystick4Button0))
				{
					player4.ready = true;
				}
				if (Input.GetAxisRaw("joystick 4 x")== 1 || Input.GetAxisRaw("joystick 4 dx") == 1)
				{
					if (joystick4available == true)
					{
						blip.Play ();
						if (player4.skin == maxskins)
						{
							player4.skin = 0;
							joystick4available = false;
						}
						else
						{
							player4.skin++;
							joystick4available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 4 x")== -1 || Input.GetAxisRaw("joystick 4 dx") == -1)
				{
					if (joystick4available == true)
					{
						blip.Play ();
						if (player4.skin == 0)
						{
							player4.skin = maxskins;
							joystick4available = false;
						}
						else
						{
							player4.skin--;
							joystick4available = false;
						}
					}
					
				}
				if (Input.GetAxisRaw("joystick 4 x")== 0 && Input.GetAxisRaw("joystick 4 dx") == 0)
				{
					joystick4available = true;
				}
			}
			if (Input.GetKeyDown(KeyCode.Joystick4Button1))
			{
				if (player4.ready == false)
				{
					player4.controller = 0;
					LeftArrow4.SetActive (false);
					RightArrow4.SetActive (false);
					Player4Text.SetActive (false);
					Player4Skin.SetActive (false);
					FiretoJoin4.text = "Press Fire to Join ";
					joinedplayers--;
				}
				else
				{
					player4.ready = false;
				}
				
			}
			
		}


		//Code to detect inputs and assign values to each player, so that we know the device each player uses
		if (Input.GetKeyDown(acceptfire1))
	    {
			if (player1.controller != keyboard1 && player2.controller != keyboard1 && player3.controller != keyboard1 && player4.controller != keyboard1)
			{
				if (player1.controller == 0)
				{
					player1.controller = keyboard1; 
					LeftArrow1.SetActive (true);
					RightArrow1.SetActive (true);
					Player1Text.SetActive (true);
					Player1Skin.SetActive (true);
					FiretoJoin1.text = " ";
					joinedplayers++;
				}
				else if (player2.controller == 0)
				{
					player2.controller =keyboard1;
					LeftArrow2.SetActive (true);
					RightArrow2.SetActive (true);
					Player2Text.SetActive (true);
					Player2Skin.SetActive (true);
					FiretoJoin2.text = " ";
					joinedplayers++;
				}
				else if (player3.controller == 0)
				{
					player3.controller =keyboard1;
					LeftArrow3.SetActive (true);
					RightArrow3.SetActive (true);
					Player3Text.SetActive (true);
					Player3Skin.SetActive (true);
					FiretoJoin3.text = " ";
					joinedplayers++;
				}
				else if (player4.controller == 0)
				{
					player4.controller =keyboard1;
					LeftArrow4.SetActive (true);
					RightArrow4.SetActive (true);
					Player4Text.SetActive (true);
					Player4Skin.SetActive (true);
					FiretoJoin4.text = " ";
					joinedplayers++;
				}
			}
		}

		if (Input.GetKeyDown(acceptfire2))
		{
			if (player1.controller != keyboard2 && player2.controller != keyboard2 && player3.controller != keyboard2 && player4.controller != keyboard2)
			{
				if (player1.controller == 0)
				{
					player1.controller =keyboard2;
					LeftArrow1.SetActive (true);
					RightArrow1.SetActive (true);
					Player1Text.SetActive (true);
					Player1Skin.SetActive (true);
					FiretoJoin1.text = " ";
					joinedplayers++;
				}
				else if (player2.controller == 0)
				{
					player2.controller =keyboard2;
					LeftArrow2.SetActive (true);
					RightArrow2.SetActive (true);
					Player2Text.SetActive (true);
					Player2Skin.SetActive (true);
					FiretoJoin2.text = " ";
					joinedplayers++;

				}
				else if (player3.controller == 0)
				{
					player3.controller =keyboard2;
					LeftArrow3.SetActive (true);
					RightArrow3.SetActive (true);
					Player3Text.SetActive (true);
					Player3Skin.SetActive (true);
					FiretoJoin3.text = " ";
					joinedplayers++;
				}
				else if (player4.controller == 0)
				{
					player4.controller =keyboard2;
					LeftArrow4.SetActive (true);
					RightArrow4.SetActive (true);
					Player4Text.SetActive (true);
					Player4Skin.SetActive (true);
					FiretoJoin4.text = " ";
					joinedplayers++;
				}
			}
		}
		if (Input.GetKeyDown("joystick 1 button 0"))
		{
			if (player1.controller != gamepad1 && player2.controller != gamepad1 && player3.controller != gamepad1 && player4.controller != gamepad1)
			{
				if (player1.controller == 0)
				{
					player1.controller =gamepad1;
					LeftArrow1.SetActive (true);
					RightArrow1.SetActive (true);
					Player1Text.SetActive (true);
					Player1Skin.SetActive (true);
					FiretoJoin1.text = " ";
					joinedplayers++;
				}
				else if (player2.controller == 0)
				{
					player2.controller =gamepad1;
					LeftArrow2.SetActive (true);
					RightArrow2.SetActive (true);
					Player2Text.SetActive (true);
					Player2Skin.SetActive (true);
					FiretoJoin2.text = " ";
					joinedplayers++;
				}
				else if (player3.controller == 0)
				{
					player3.controller =gamepad1;
					LeftArrow3.SetActive (true);
					RightArrow3.SetActive (true);
					Player3Text.SetActive (true);
					Player3Skin.SetActive (true);
					FiretoJoin3.text = " ";
					joinedplayers++;
				}
				else if (player4.controller == 0)
				{
					player4.controller =gamepad1;
					LeftArrow4.SetActive (true);
					RightArrow4.SetActive (true);
					Player4Text.SetActive (true);
					Player4Skin.SetActive (true);
					FiretoJoin4.text = " ";
					joinedplayers++;
				}
			}
		}
		if (Input.GetKeyDown("joystick 2 button 0"))
		{
			if (player1.controller != gamepad2 && player2.controller != gamepad2 && player3.controller != gamepad2 && player4.controller != gamepad2)
			{
				if (player1.controller == 0)
				{
					player1.controller =gamepad2;
					LeftArrow1.SetActive (true);
					RightArrow1.SetActive (true);
					Player1Text.SetActive (true);
					Player1Skin.SetActive (true);
					FiretoJoin1.text = " ";
					joinedplayers++;
				}
				else if (player2.controller == 0)
				{
					player2.controller =gamepad2;
					LeftArrow2.SetActive (true);
					RightArrow2.SetActive (true);
					Player2Text.SetActive (true);
					Player2Skin.SetActive (true);
					FiretoJoin2.text = " ";
					joinedplayers++;
				}
				else if (player3.controller == 0)
				{
					player3.controller =gamepad2;
					LeftArrow3.SetActive (true);
					RightArrow3.SetActive (true);
					Player3Text.SetActive (true);
					Player3Skin.SetActive (true);
					FiretoJoin3.text = " ";
					joinedplayers++;
				}
				else if (player4.controller == 0)
				{
					player4.controller =gamepad2;
					LeftArrow4.SetActive (true);
					RightArrow4.SetActive (true);
					Player4Text.SetActive (true);
					Player4Skin.SetActive (true);
					FiretoJoin4.text = " ";
					joinedplayers++;
				}
			}
		}

		if (Input.GetKeyDown("joystick 3 button 0"))
		{
			if (player1.controller != gamepad3 && player2.controller != gamepad3 && player3.controller != gamepad3 && player4.controller != gamepad3)
			{
				if (player1.controller == 0)
				{
					player1.controller =gamepad3;
					LeftArrow1.SetActive (true);
					RightArrow1.SetActive (true);
					Player1Text.SetActive (true);
					Player1Skin.SetActive (true);
					FiretoJoin1.text = " ";
					joinedplayers++;
				}
				else if (player2.controller == 0)
				{
					player2.controller =gamepad3;
					LeftArrow2.SetActive (true);
					RightArrow2.SetActive (true);
					Player2Text.SetActive (true);
					Player2Skin.SetActive (true);
					FiretoJoin2.text = " ";
					joinedplayers++;
				}
				else if (player3.controller == 0)
				{
					player3.controller =gamepad3;
					LeftArrow3.SetActive (true);
					RightArrow3.SetActive (true);
					Player3Text.SetActive (true);
					Player3Skin.SetActive (true);
					FiretoJoin3.text = " ";
					joinedplayers++;
				}
				else if (player4.controller == 0)
				{
					player4.controller =gamepad3;
					LeftArrow4.SetActive (true);
					RightArrow4.SetActive (true);
					Player4Text.SetActive (true);
					Player4Skin.SetActive (true);
					FiretoJoin4.text = " ";
					joinedplayers++;
				}
			}
		}
		if (Input.GetKeyDown("joystick 4 button 0"))
		{
			if (player1.controller != gamepad4 && player2.controller != gamepad4 && player3.controller != gamepad4 && player4.controller != gamepad4)
			{
				if (player1.controller == 0)
				{
					player1.controller =gamepad4;
					LeftArrow1.SetActive (true);
					RightArrow1.SetActive (true);
					Player1Text.SetActive (true);
					Player1Skin.SetActive (true);
					FiretoJoin1.text = " ";
					joinedplayers++;
				}
				else if (player2.controller == 0)
				{
					player2.controller =gamepad4;
					LeftArrow2.SetActive (true);
					RightArrow2.SetActive (true);
					Player2Text.SetActive (true);
					Player2Skin.SetActive (true);
					FiretoJoin2.text = " ";
					joinedplayers++;
				}
				else if (player3.controller == 0)
				{
					player3.controller =gamepad4;
					LeftArrow3.SetActive (true);
					RightArrow3.SetActive (true);
					Player3Text.SetActive (true);
					Player3Skin.SetActive (true);
					FiretoJoin3.text = " ";
					joinedplayers++;
				}
				else if (player4.controller == 0)
				{
					player4.controller =gamepad4;
					LeftArrow4.SetActive (true);
					RightArrow4.SetActive (true);
					Player4Text.SetActive (true);
					Player4Skin.SetActive (true);
					FiretoJoin4.text = " ";
					joinedplayers++;
				}
			}
		}




		//Code to make the screen behave in a different way according to the value assigned to each player
		//Player 1


		if (Player1Skin.activeSelf == true)
		{
		Player1Skin.GetComponent<Animator> ().SetInteger ("Skin", player1.skin);
		}
		if (Player2Skin.activeSelf == true)
		{
		Player2Skin.GetComponent<Animator> ().SetInteger ("Skin", player2.skin);
		}
		if (Player3Skin.activeSelf == true)
		{
		Player3Skin.GetComponent<Animator> ().SetInteger ("Skin", player3.skin);
		}
		if (Player4Skin.activeSelf == true)
		{
		Player4Skin.GetComponent<Animator> ().SetInteger ("Skin", player4.skin);
		}


		//Code so that something happens if players are ready

		if (player1.ready == true)
		{
			Player1ReadyText.SetActive(true);
		}
		else
		{
			Player1ReadyText.SetActive(false);
		}
		if (player2.ready == true)
		{
			Player2ReadyText.SetActive(true);
		}
		else
		{
			Player2ReadyText.SetActive(false);
		}
		if (player3.ready == true)
		{
			Player3ReadyText.SetActive(true);
		}
		else
		{
			Player3ReadyText.SetActive(false);
		}
		if (player4.ready == true)
		{
			Player4ReadyText.SetActive(true);
		}
		else
		{
			Player4ReadyText.SetActive(false);
		}
		
		if (joinedplayers >= 2 && ((player1.ready? 1:0)+(player2.ready? 1:0)+(player3.ready? 1:0)+(player4.ready? 1:0) == joinedplayers))
		{
			Controls_Manager.controls_manager.Player1.controller = player1.controller;
			Controls_Manager.controls_manager.Player2.controller = player2.controller;
			Controls_Manager.controls_manager.Player3.controller = player3.controller;
			Controls_Manager.controls_manager.Player4.controller = player4.controller;
			Controls_Manager.controls_manager.numberofplayers = joinedplayers;
			Controls_Manager.controls_manager.Player1.skin = player1.skin;
			Controls_Manager.controls_manager.Player2.skin = player2.skin;
			Controls_Manager.controls_manager.Player3.skin = player3.skin;
			Controls_Manager.controls_manager.Player4.skin = player4.skin;
			Controls_Manager.controls_manager.Player1.ready = player1.ready;
			Controls_Manager.controls_manager.Player2.ready = player2.ready;
			Controls_Manager.controls_manager.Player3.ready = player3.ready;
			Controls_Manager.controls_manager.Player4.ready = player4.ready;
			SceneManager.LoadScene("Stage Selection Screen");
		}
	}
}

[System.Serializable]
public class Player
{
	public int controller;
	public int skin;
	public bool ready;
	public int score;

	public Player()
	{
		controller = 0;
		skin = 0;
		ready = false;
		score = 0;

	}
}
