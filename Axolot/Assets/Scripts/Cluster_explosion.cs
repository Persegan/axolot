﻿using UnityEngine;
using System.Collections;

public class Cluster_explosion : MonoBehaviour {

	public GameObject cluster_projectile;		//type of projectile
	public float count;							// number of projectiles

	private GameObject temp;
	public GameObject ship;

	// Use this for initialization
	void Start () {
		for (int i=0; i<count; i++) 
		{
			temp = (GameObject)Instantiate (cluster_projectile, transform.position, transform.rotation);
			temp.GetComponent<ScriptDisparo>().ship = ship	; 
			transform.Rotate(0,0,360/count, Space.Self);
		}
	}
	
	// Update is called once per frame
	void Update () {
		Destroy (gameObject);
	}
}
