﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class stage_selection_menu : MonoBehaviour {
	public Button Stage1;

	public GameObject laser;


	private int music;
	// Use this for initialization
	void Start () {
		music = Random.Range (0, 3);
		Cursor.visible = false;
		Stage1.Select ();
		if (Music_Manager.music_manager.Song4.isPlaying == true) 
		{

		} else 
		{
			Music_Manager.music_manager.Song4.loop = true;
			Music_Manager.music_manager.Song4.Play ();

		}


	}

	// Update is called once per frame
	void Update () {
		Cursor.lockState = CursorLockMode.Locked;

	}


	public void mouseclick()
	{
		Stage1.Select ();
	}

	public void SubmitStage1()
	{
		
		Music_Manager.music_manager.Song4.Stop ();
		switch (music) 
		{	
		case 0:

			Music_Manager.music_manager.SongMenu.loop = true;
			Music_Manager.music_manager.SongMenu.Play ();
			break;
		case 1: 

			Music_Manager.music_manager.Song2.loop = true;
			Music_Manager.music_manager.Song2.Play ();			
			break;
		case 2: 

			Music_Manager.music_manager.Song3.loop = true;
			Music_Manager.music_manager.Song3.Play ();
			break;
		default:
			break;
		}
		Instantiate(laser, new Vector2(10f, 10f), transform.rotation);
		SceneManager.LoadScene ("Free For All Stage 1");
	}

	public void SubmitStage2()
	{
		Music_Manager.music_manager.Song4.Stop ();
		switch (music) 
		{	
		case 0:
			Music_Manager.music_manager.SongMenu.loop = true;
			Music_Manager.music_manager.SongMenu.Play ();
			break;
		case 1: 	
			Music_Manager.music_manager.Song2.loop = true;
			Music_Manager.music_manager.Song2.Play ();			
			break;
		case 2: 								//2. cluster bomb
			Music_Manager.music_manager.Song3.loop = true;
			Music_Manager.music_manager.Song3.Play ();
			break;
		default:
			break;
		}
		Instantiate(laser, new Vector2(10f, 10f), transform.rotation);
		SceneManager.LoadScene ("Free For All Stage 2");
	}

	public void SubmitStage3()
	{
		Music_Manager.music_manager.Song4.Stop ();
		switch (music) 
		{	
		case 0:
			Music_Manager.music_manager.SongMenu.loop = true;
			Music_Manager.music_manager.SongMenu.Play ();
			break;
		case 1: 	
			Music_Manager.music_manager.Song2.loop = true;
			Music_Manager.music_manager.Song2.Play ();			
			break;
		case 2: 								//2. cluster bomb
			Music_Manager.music_manager.Song3.loop = true;
			Music_Manager.music_manager.Song3.Play ();
			break;
		default:
			break;
		}
		Instantiate(laser, new Vector2(10f, 10f), transform.rotation);
		SceneManager.LoadScene ("Free For All Stage 3");
	}

    public void SubmitStage5()
    {

        Music_Manager.music_manager.Song4.Stop();
        switch (music)
        {
            case 0:

                Music_Manager.music_manager.SongMenu.loop = true;
                Music_Manager.music_manager.SongMenu.Play();
                break;
            case 1:

                Music_Manager.music_manager.Song2.loop = true;
                Music_Manager.music_manager.Song2.Play();
                break;
            case 2:

                Music_Manager.music_manager.Song3.loop = true;
                Music_Manager.music_manager.Song3.Play();
                break;
            default:
                break;
        }
        Instantiate(laser, new Vector2(10f, 10f), transform.rotation);
        SceneManager.LoadScene("Free For All Stage 5");
    }

    public void SubmitStage6()
    {

        Music_Manager.music_manager.Song4.Stop();
        switch (music)
        {
            case 0:

                Music_Manager.music_manager.SongMenu.loop = true;
                Music_Manager.music_manager.SongMenu.Play();
                break;
            case 1:

                Music_Manager.music_manager.Song2.loop = true;
                Music_Manager.music_manager.Song2.Play();
                break;
            case 2:

                Music_Manager.music_manager.Song3.loop = true;
                Music_Manager.music_manager.Song3.Play();
                break;
            default:
                break;
        }
        Instantiate(laser, new Vector2(10f, 10f), transform.rotation);
        SceneManager.LoadScene("Free For All Stage 6");
    }

    public void SubmitStage7()
    {

        Music_Manager.music_manager.Song4.Stop();
        switch (music)
        {
            case 0:

                Music_Manager.music_manager.SongMenu.loop = true;
                Music_Manager.music_manager.SongMenu.Play();
                break;
            case 1:

                Music_Manager.music_manager.Song2.loop = true;
                Music_Manager.music_manager.Song2.Play();
                break;
            case 2:

                Music_Manager.music_manager.Song3.loop = true;
                Music_Manager.music_manager.Song3.Play();
                break;
            default:
                break;
        }
        Instantiate(laser, new Vector2(10f, 10f), transform.rotation);
        SceneManager.LoadScene("Free For All Stage 7");
    }

}
