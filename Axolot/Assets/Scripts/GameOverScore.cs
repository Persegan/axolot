﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOverScore : MonoBehaviour {

	public int score;
	public Text text;

	// Use this for initialization
	void Start () 
	{
		score = GameObject.Find ("Game_control_center").GetComponent<control_center> ().puntuacion;
	}
	
	// Update is called once per frame
	void Update () 
	{
		text.text = "Your Score:" + score.ToString();
		if (Input.GetKey(KeyCode.Q))
		    {
			SceneManager.LoadScene ("menu");
			Destroy (GameObject.Find ("Game_control_center"));
			}
		else if (Input.GetKey (KeyCode.R))
		    {
			SceneManager.LoadScene ("Test");
			Destroy (GameObject.Find ("Game_control_center"));
			}
	}
}
