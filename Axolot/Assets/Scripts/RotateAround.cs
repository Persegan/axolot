﻿using UnityEngine;
using System.Collections;

public class RotateAround : MonoBehaviour {
	public float rotation_speed;
	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D> ().angularVelocity = rotation_speed;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
