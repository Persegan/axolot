﻿using UnityEngine;
using System.Collections;

public class soccer_field : MonoBehaviour {

	public GameObject ball;

	public float red_score { get; set; }
	public float blue_score { get; set; }

	void Start () {
		red_score = blue_score = 0;
	}

	void Update () {
	
	}


	public void RedTeamScores(){
		red_score++;
		print ("Red team scored! " + red_score + ":" + blue_score);
		ball.GetComponent<ball> ().RespawnBall ();
	}

	public void BlueTeamScores(){
		blue_score++;
		print ("Blue team scored! " + red_score + ":" + blue_score);
		ball.GetComponent<ball> ().RespawnBall ();
	}
}
