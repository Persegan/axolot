﻿using UnityEngine;
using System.Collections;

public class Controls_Manager : MonoBehaviour {

	public static Controls_Manager controls_manager;

	public Player Player1;
	public Player Player2;
	public Player Player3;
	public Player Player4;

	public int numberofplayers;

	public KeyCode Up1;
	public KeyCode Right1;
	public KeyCode Left1;
	public KeyCode Down1;
	public KeyCode acceptfire1;
	public KeyCode cancelturbo1;

	public KeyCode Up2;
	public KeyCode Right2;
	public KeyCode Left2;
	public KeyCode Down2;
	public KeyCode acceptfire2;
	public KeyCode cancelturbo2;


	void Awake()
	{
		if (controls_manager == null)
		{
			DontDestroyOnLoad (gameObject);
			controls_manager = this;
		}

		else if (controls_manager != this)
		{
			Destroy(gameObject);
		}
	
	}

	// Use this for initialization
	void Start () 
	{	
		if (PlayerPrefs.HasKey ("Controls")) 
		{
			Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
			Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
			Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
			Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
			acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
			cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));

			Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
			Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
			Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
			Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
			acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
			cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));
		}
		else
		{
			PlayerPrefs.SetString("Up1", "UpArrow");
			Up1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up1"));
			PlayerPrefs.SetString("Right1", "RightArrow");
			Right1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right1"));
			PlayerPrefs.SetString("Left1", "LeftArrow");
			Left1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left1"));
			PlayerPrefs.SetString("Down1", "DownArrow");
			Down1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down1"));
			PlayerPrefs.SetString("acceptfire1", "RightControl");
			acceptfire1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire1"));
			PlayerPrefs.SetString("cancelturbo1", "RightShift");
			cancelturbo1 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo1"));

			PlayerPrefs.SetString("Up2", "W");
			Up2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Up2"));
			PlayerPrefs.SetString("Right2", "D");
			Right2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right2"));
			PlayerPrefs.SetString("Left2", "A");
			Left2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left2"));
			PlayerPrefs.SetString("Down2", "S");
			Down2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Down2"));
			PlayerPrefs.SetString("acceptfire2", "F");
			acceptfire2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("acceptfire2"));
			PlayerPrefs.SetString("cancelturbo2", "G");
			cancelturbo2 = (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("cancelturbo2"));

	
			PlayerPrefs.SetInt("Controls", 1);
			PlayerPrefs.Save ();
		}
	
	}
	
	// Update is called once per frame
	void Update () 
	{

	}
}

