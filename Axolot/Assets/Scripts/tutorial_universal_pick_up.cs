﻿using UnityEngine;
using System.Collections;

public class tutorial_universal_pick_up : MonoBehaviour {

	public int PowerUp_number;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other){
		if (other.gameObject.tag == "jugador") {
			if (other.gameObject.GetComponent<Ship_Movement_2Player> ().tutorial_pu == 0) {
				other.gameObject.GetComponent<Ship_Movement_2Player> ().tutorial_pu = PowerUp_number;
				StartCoroutine (other.gameObject.GetComponent<Ship_Movement_2Player> ().PowerUpPickUp ());
			}
		}
	}
}
