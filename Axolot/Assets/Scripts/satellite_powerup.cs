﻿using UnityEngine;
using System.Collections;

public class satellite_powerup : MonoBehaviour {

	public GameObject ship;
	public float rotation_speed;    // how fast the satellites go
	public float rotation_distance; // how far away from the ship the satellites are
	public float fire_speed;		// the speed at which the satellite will travel when fired
	public float duration;			// how long the satellites will travel before expiring
	public GameObject controller;

	private bool fired;				// indicates if the satellite has been fired
	private bool player_fire_input;
	private float angle;              // the angle at which one this specific satellite will start
					
			
	// Use this for initialization
	void Start () {	
		fired = false;
		player_fire_input = false;
		if (duration == 0)
			duration = 1;		// to make sure there is some flying time
	}

	void Update()
	{
		if (Input.GetKeyDown (ship.GetComponent<Ship_Movement_2Player>().p_fire))
	    {
			player_fire_input = true;		// get user input for future use
		}
	}
	

	void FixedUpdate () {
		// Create a circular orbit using sin and cos math functions
		// The rotation speed is in degrees, mutliplied by Mathf.Deg2Rad to get radians

		if (fired == false) {
			transform.position = new Vector3 (ship.transform.position.x + Mathf.Cos (angle * Mathf.Deg2Rad) * rotation_distance, ship.transform.position.y + Mathf.Sin (angle * Mathf.Deg2Rad) * rotation_distance);
			angle = angle%360 + rotation_speed;
		}

		if (player_fire_input == true && fired == false) {		// if the satellite has not been fired

			if ( Mathf.Abs(angle - ((ship.transform.eulerAngles.z + 90)%360)) < 60){   // fire a satellite that is 60 degrees near where the ship is facing
				GetComponent<Rigidbody2D>().AddForce(ship.transform.up * fire_speed);			 
				fired = true;
				controller.GetComponent<satellite_controller>().number_of_satellites--;	//accesses the controller to reduce the satellite count
				Destroy (gameObject, duration);
			}
			else
			{
					player_fire_input = false;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D collider)
	{// currently satellites can kill their owners
		if (collider.gameObject.tag == "jugador" || collider.gameObject.tag == "boomerang" || collider.gameObject.tag == "directional_misile" 
			|| collider.gameObject.tag == "mine" || collider.gameObject.tag == "bala_jugador" ||  collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "mega_death_rocket" 
			|| collider.gameObject.tag == "shield"){ 	// A satellite will be destroyed when collided with an of these

			if (collider.gameObject != ship) {
				if (fired == false) {
					controller.GetComponent<satellite_controller> ().number_of_satellites--;	//accesses the controller to reduce the satellite count
				}
				Destroy (gameObject);
			}
		}
	}


	void OnTriggerEnter2D(Collider2D collider)
	{// currently satellites can kill their owners
		if (collider.gameObject.tag == "jugador" || collider.gameObject.tag == "boomerang" || collider.gameObject.tag == "directional_missile" 
			|| collider.gameObject.tag == "mine" || collider.gameObject.tag == "bala_jugador" ||  collider.gameObject.tag == "mega_turbo_killer" || collider.gameObject.tag == "mega_death_rocket" 
			|| collider.gameObject.tag == "shield"){ 	// A satellite will be destroyed when collided with an of these
			if (collider.gameObject != ship) {
				if (fired == false) {
					controller.GetComponent<satellite_controller> ().number_of_satellites--;	//accesses the controller to reduce the satellite count
				}
				Destroy (gameObject);
			}
		}
	}



	public void set_angle(float initial_angle)		// used for initialization by the satellite controller
	{
		angle = initial_angle;
	}


}
