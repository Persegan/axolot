﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class satellite_controller : MonoBehaviour {

	public GameObject ship;
	public GameObject satellite;
	public float number_of_satellites;	// how many mines can be deployed per power up;
	//public float rotation_distance;	// how far away from the ship the satellites are
	//public float rotation_speed;	// how fast the satellites go

	//private Transform sat_pos;		// this is used to set the initial position of each satellite
	private float angle;			// this is used to store a calculation of where each satellite will start based on angles
	private GameObject temp_sat;
	public Sprite icon;
	public int duration;
	public float updatetime = 0;
	

	// Create a circular orbit using sin and cos math functions
	// The rotation speed is in degrees, mutliplied by Mathf.Deg2Rad to get radians
	void Start () {
		
		//sat_pos = gameObject.transform;		// maybe not needed
		for (int i=0; i < number_of_satellites; i++) {
			angle = (360 / number_of_satellites) * i;		// this moves the spawining point of each satellite some degrees based on how many we have
			//sat_pos.position = new Vector3 (ship.transform.position.x + Mathf.Cos (angle * Mathf.Deg2Rad) * rotation_distance, ship.transform.position.y + Mathf.Sin (angle * Mathf.Deg2Rad) * rotation_distance);
			//temp_sat = (GameObject)Instantiate (satellite, sat_pos.position, new Quaternion());
			temp_sat = Instantiate (satellite, transform.position, new Quaternion()) as GameObject;
			Destroy (temp_sat, duration);
			temp_sat.GetComponent<satellite_powerup>().ship = ship;
			temp_sat.GetComponent<satellite_powerup>().set_angle(angle);		// after creating a new satellite, we inform it of it's starting angle
		}
	}
	
	// Update is called once per frame
	void Update () {
		updatetime -= Time.deltaTime;
		if (updatetime < 0) {
			duration -= 1;
			updatetime = 1;
		}
		ship.GetComponent<Ship_Movement_2Player> ().municion_texto.text = number_of_satellites.ToString();
		if (number_of_satellites == 0 || duration == -1) {
			ship.GetComponent<Ship_Movement_2Player>().PowerUp = 0;
			ship.GetComponent<Ship_Movement_2Player> ().tutorial_pu = 0;	// for the tutorial only
			Destroy(gameObject);
		}
	}

	
	void FixedUpdate()
	{
		
	}
}
