﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class EventManger_2Player : MonoBehaviour {

	public GameObject player1;
	public GameObject player2;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (player1 == null)
		{
			StartCoroutine(victory2 ());
		}

		if (player2 == null)
		{
			StartCoroutine(victory1 ());
		}
	
	}

	IEnumerator victory1()
	{
		yield return new WaitForSeconds (1);
		SceneManager.LoadScene ("2Player_Player1Wins");
	}
	IEnumerator victory2()
	{
		yield return new WaitForSeconds (1);
		SceneManager.LoadScene  ("2Player_Player2Wins");
	}
}
