﻿using UnityEngine;
using System.Collections;

public class Script_Burbuja : MonoBehaviour {
	
	public int MinTorque;
	public int MaxTorque;
	public float MinFuerza;
	public float MaxFuerza;
	public float maxSpeed;
	public GameObject Explosion;
	public bool can_stun = false;

	
	
	// Use this for initialization
	void Start () 
	{
		//movimiento del asteroide
		float magnitud = Random.Range (MinFuerza, MaxFuerza);
		float x = Random.Range(-1f, 1f);
		float y = Random.Range (-1f, 1f);
		//rotacion del asteroide
		GetComponent<Rigidbody2D>().AddForce (magnitud * new Vector2(x, y)); 
		float torque = Random.Range (MinTorque, MaxTorque);
		GetComponent<Rigidbody2D>().AddTorque (torque);
		//GetComponent<Rigidbody2D>().angularDrag = 0.5f;
		
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
		{
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
		}
		if (can_stun == true)
		{
			gameObject.GetComponent<SpriteRenderer>().color = Color.red;
		}
	}
	
	void OnCollisionEnter2D(Collision2D collider)
	{
		if (collider.gameObject.tag == "boomerang" || collider.gameObject.tag == "directional_missile" 
			|| collider.gameObject.tag == "mine" || collider.gameObject.tag == "satellite" || collider.gameObject.tag == "bala_jugador" || collider.gameObject.tag == "mega_turbo_killer" 
			|| collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "explosion") {		// if it collides with any of these, it explodes normaly

			Instantiate (Explosion, transform.position, new Quaternion ());
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "boomerang" || collider.gameObject.tag == "directional_missile" 
			|| collider.gameObject.tag == "mine" || collider.gameObject.tag == "satellite" || collider.gameObject.tag == "bala_jugador" || collider.gameObject.tag == "mega_turbo_killer" 
			|| collider.gameObject.tag == "mega_death_rocket" || collider.gameObject.tag == "explosion") {		// if it collides with any of these, it explodes normaly

			Instantiate (Explosion, transform.position, new Quaternion ());
			Destroy (gameObject);
		}
	}
} 