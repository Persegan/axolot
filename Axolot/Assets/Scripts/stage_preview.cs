﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class stage_preview : MonoBehaviour {
	public Button Stage1;
	public Button Stage2;
	public Button Stage3;
	public Sprite Stage1sprite;
	public Sprite Stage2sprite;
	public Sprite Stage3sprite;
    public Button Stage5;
    public Button Stage6;
    public Button Stage7;
    public Sprite Stage5sprite;
    public Sprite Stage6sprite;
    public Sprite Stage7sprite;



    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	public void SelectStage1 () {
		gameObject.GetComponent<Image> ().sprite = Stage1sprite;
	}
	public void SelectStage2()
	{
		gameObject.GetComponent<Image> ().sprite = Stage2sprite;
	}
	public void SelectStage3()
	{
		gameObject.GetComponent<Image> ().sprite = Stage3sprite;
	}

    public void SelectStage5()
    {
        gameObject.GetComponent<Image>().sprite = Stage5sprite;
    }

    public void SelectStage6()
    {
        gameObject.GetComponent<Image>().sprite = Stage6sprite;
    }

    public void SelectStage7()
    {
        gameObject.GetComponent<Image>().sprite = Stage7sprite;
    }
}
