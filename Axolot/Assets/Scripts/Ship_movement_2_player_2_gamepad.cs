﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ship_movement_2_player_2_gamepad : MonoBehaviour {
	public float RotationSpeed;
	public float ThrustForce;
	public float freno;
	public ParticleSystem ThrustParticleEffect;
	public AudioSource SonidoMotor;
	public AudioSource PowerUpSound;
	public float maxSpeed;
	public GameObject Explosion_Jugador;
	public GameObject DisparoAjolote;
	public Text municion_texto;
	public Image arma;
	public Sprite Arma1;
	Animator animator;
	
	private int PowerUp = 0;
	private int municion = 0;
	private GameObject Powerup_PickedUp;
	Animator powerup_animator;
	
	// Use this for initialization
	void Start () 
	{
		ThrustParticleEffect.GetComponent<Renderer>().sortingLayerName = "Foreground";
		GetComponent<Rigidbody2D>().drag = freno;
		animator = gameObject.GetComponent<Animator> ();
		animator.SetBool("Movement", false);
		municion_texto = municion_texto.GetComponent <Text>();
		municion_texto.text = "";
		
	}
	void Update()
	{
		
	}
	
	
	void FixedUpdate()
	{
		if (PowerUp == 0)
		{
			arma.enabled = false;
			municion_texto.text = "";
		}
		else
		{
			arma.enabled = true;
			municion_texto.text = municion.ToString();
		}
		if (Input.GetAxis ("Move_Sides") < -0.99) 
		{
			//rotar el ajolote a la izquierda
			GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;					
		}
		else if (Input.GetAxis ("Move_Sides") > 0.99 )
		{
			//rotar el ajolote a la derecha
			GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
		} 
		else
		{
			//parar la rotacion
			GetComponent<Rigidbody2D>().angularVelocity = 0f;
			
			
		}
		if (Input.GetButton ("Move_Forward"))
		{
			animator.SetBool("Movement", true);
			//engage the engine
			GetComponent<Rigidbody2D>().AddForce (transform.up*ThrustForce);
			//sistema de particulas :D
			ThrustParticleEffect.Emit(5);
			//Audio del motor
			if (SonidoMotor.isPlaying == false)
			{
				SonidoMotor.Play();
			}
		}
		else
		{
			animator.SetBool("Movement", false);
			//parar el sonido
			SonidoMotor.Stop ();
		}
		if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
		{
			GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
		}
		if (Input.GetButtonDown ( "Fire"))
		{
			if (PowerUp == 1)
			{
				Instantiate (DisparoAjolote, transform.position, transform.rotation);
				municion -= 1;
				
			}
			
		}
		if (municion == 0)
		{
			PowerUp = 0;
		}
		
		
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "asteroide" )
		{
			GetComponent<Rigidbody2D>().AddForce (collider.GetComponent<Rigidbody2D>().velocity.magnitude * new Vector2(-GetComponent<Rigidbody2D>().velocity.x, -GetComponent<Rigidbody2D>().velocity.y));
			
		}
		if (collider.gameObject.tag == "bala_mob" || collider.gameObject.tag == "mob")
		{
			Destroy (gameObject);
			Destroy (collider.gameObject);
			Instantiate (Explosion_Jugador, transform.position, new Quaternion());
			
			
		}
		if (collider.gameObject.tag == "PowerUp")
		{
			if (PowerUp == 0)
			{
				Powerup_PickedUp = collider.gameObject;
				StartCoroutine(PowerUpPickUp());
				
			}
			
		}
		if (collider.gameObject.tag == "Bala_jugador_1")
		{
			Destroy (gameObject);
			Destroy (collider.gameObject);
			Instantiate (Explosion_Jugador, transform.position, new Quaternion());
		}
		
	}
	
	IEnumerator PowerUpPickUp()
	{
		//PowerUp = Random.Range (1, 6);
		powerup_animator = Powerup_PickedUp.GetComponent<Animator> ();
		Powerup_PickedUp.GetComponent<PowerUp_Script> ().PowerUp_available = false;
		PowerUpSound.Play ();
		PowerUp = 1;
		if (PowerUp == 1)
		{
			municion = 3;
			arma.GetComponent<Image>().sprite = Arma1;
		}
		powerup_animator.SetBool ("pick_up", true);
		yield return new WaitForSeconds(0.45f);	
		Destroy (Powerup_PickedUp);
	}
}
