﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Spawner : MonoBehaviour {

	public float TiempoRespawn;
	public GameObject cosa;
	public float MaxX;
	public float MaxY;
	public float MinX;
	public float MinY;
	public float tiempo;
	private float x;
	private float y;



	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		tiempo -= Time.deltaTime; 
		if (tiempo < 0)
		{
			Instanciar();
			tiempo = TiempoRespawn;
		}


	
	}

	public void RestartGame()
	{
		SceneManager.LoadScene ("GetReady");
	}
	
	public void ExitToMenu()
	{
		SceneManager.LoadScene("menu");
	}
	void Instanciar()
	{
		x = Random.Range(MinX, MaxX);
		y = Random.Range (MinY, MaxY);
		Instantiate (cosa, new Vector2(x, y), new Quaternion());

	}
	
}
