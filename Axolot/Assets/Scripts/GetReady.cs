﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GetReady : MonoBehaviour {

	public float loadingtime = 2f;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		loadingtime -= Time.deltaTime;

		if (loadingtime<0)
		{
			SceneManager.LoadScene ("Test");
		}
	
	}
}
