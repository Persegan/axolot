﻿using UnityEngine;
using System.Collections;

public class ball : MonoBehaviour {

	public float Max_speed;
	public float small_force;	//these 3 force are used to determine how strong the ball will be pushed when colliding with certain objects
	public float medium_force;
	public float big_force;

	private Vector2 v_max_speed;
	private float velocity_x, velocity_y;
	private Vector2 SpawnPos;

	void Start () {
		SpawnPos = transform.position;
	}

	void FixedUpdate () {
		//the following just limits the speed of the ball to a min/max of Max_speed
		velocity_x = GetComponent<Rigidbody2D> ().velocity.x;
		velocity_y = GetComponent<Rigidbody2D> ().velocity.y;
		velocity_x = Mathf.Clamp (velocity_x, -Max_speed, Max_speed);
		velocity_y = Mathf.Clamp (velocity_y, -Max_speed, Max_speed);
		v_max_speed = new Vector2 (velocity_x, velocity_y);
		GetComponent<Rigidbody2D> ().velocity = v_max_speed;
	}

	public void RespawnBall(){
		transform.position = SpawnPos;
		GetComponent<Rigidbody2D> ().velocity = GetComponent<Rigidbody2D> ().velocity * 0;
		GetComponent<Rigidbody2D> ().angularVelocity = 0;

	}

	void OnCollisionExit2D(Collision2D other){
		/*switch(other.gameObject.tag){
		case "boomerang":
			GetComponent<Rigidbody2D> ().AddForceAtPosition (-small_force * other.gameObject.GetComponent<Rigidbody2D>().velocity, other.contacts[0].point);
			//print("Pushing the ball with small force");
			break;
		case "cluster_bomb":
			GetComponent<Rigidbody2D> ().AddForceAtPosition (-small_force * other.gameObject.GetComponent<Rigidbody2D>().velocity, other.contacts[0].point);
			//print("Pushing the ball with small force");
			break;
		case "directional_missile":
			GetComponent<Rigidbody2D> ().AddForceAtPosition (-medium_force * other.gameObject.GetComponent<Rigidbody2D>().velocity, other.contacts[0].point);
			//print("Pushing the ball with medium force");
			break;
		case "mega_turbo_killer":
			GetComponent<Rigidbody2D> ().AddForceAtPosition (-big_force * other.gameObject.transform.up, other.contacts[0].point);
			//print("Pushing the ball with big force");
			break;
		case "explosion":
			break;
		default:
			break;
		}*/

	}
}

