﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Ship_Movement_2Player_2 : MonoBehaviour {
	public float RotationSpeed;
	public float ThrustForce;
	public float TurboForce = 50;
	public float freno;
	public ParticleSystem ThrustParticleEffect;
	public ParticleSystem TurboParticleEffect;
	public AudioSource SonidoMotor;
	public AudioSource PowerUpSound;
	public float maxSpeed;
	public float maxSpeedTurbo = 5.5f;
	public GameObject Explosion_Jugador;
	public GameObject DisparoAjolote;
	public Text municion_texto;
	public Image arma;
	public Sprite Arma1;
	public Slider turbo;
	public GameObject stun_effect;
	public GameObject explosion_burbuja;
	public GameObject explosion_burbuja_stun;
	Animator animator;

	private int PowerUp = 0;
	private int municion = 0;
	private GameObject Powerup_PickedUp;
	private bool turbo_bool = false;
	private bool stunned = false;
	Animator powerup_animator;
	
	// Use this for initialization
	void Start () 
	{
		ThrustParticleEffect.GetComponent<Renderer>().sortingLayerName = "Foreground";
		GetComponent<Rigidbody2D>().drag = freno;
		animator = gameObject.GetComponent<Animator> ();
		animator.SetBool("Movement", false);
		municion_texto = municion_texto.GetComponent <Text>();
		municion_texto.text = "";
	}
	void Update()
	{
		
	}
	
	
	void FixedUpdate()
	{
		if (PowerUp == 0)
		{
			arma.enabled = false;
			municion_texto.text = "";
		}
		else
		{
			arma.enabled = true;
			municion_texto.text = municion.ToString();
		}
		if (stunned == false)
		{
			if (Input.GetAxisRaw ("P2 Rotate") == -1) 
			{
				//rotar el ajolote a la izquierda
				GetComponent<Rigidbody2D>().angularVelocity = RotationSpeed;					
			}
			else if (Input.GetAxisRaw ("P2 Rotate") == 1) 
			{
				//rotar el ajolote a la derecha
				GetComponent<Rigidbody2D>().angularVelocity = -RotationSpeed;
			} 
			else
			{
				//parar la rotacion
				GetComponent<Rigidbody2D>().angularVelocity = 0f;
				
				
			}
			if (Input.GetButton ("P2 Turbo"))
			{
				if (turbo.value > 0)
				{
					turbo.value = turbo.value - 3f;
					turbo_bool = true;
					animator.SetBool("Movement", true);
					//engage the engine
					GetComponent<Rigidbody2D>().AddForce (transform.up*TurboForce);
					//sistema de particulas :D
					TurboParticleEffect.Emit(15);
					//Audio del motor
					if (SonidoMotor.isPlaying == false)
					{
						SonidoMotor.Play();
					}
				}
				else
				{
					animator.SetBool("Movement", false);
					//parar el sonido
					SonidoMotor.Stop ();
				}
			}
			else
			{
				turbo.value = turbo.value + 0.5f;
				turbo_bool = false;
			}
			/*if (Input.GetButtonDown ("P2 Turbo"))
			{
				turbo_bool = true;
			}
			if (Input.GetButtonUp ("P2 Turbo"))
			{
				turbo_bool = false;
			}			*/
			if (Input.GetButton("P2 Move Forward"))
			{
				if (turbo_bool == false)
				{
					animator.SetBool("Movement", true);
					//engage the engine
					GetComponent<Rigidbody2D>().AddForce (transform.up*ThrustForce);
					//GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
					//sistema de particulas :D
					ThrustParticleEffect.Emit(5);
					//Audio del motor
					if (SonidoMotor.isPlaying == false)
					{
						SonidoMotor.Play();
					}
				}
			}
			else if (turbo_bool == false)
			{
				animator.SetBool("Movement", false);
				//parar el sonido
				SonidoMotor.Stop ();
			}
			if (turbo_bool == false)
			{
				if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeed)
				{
			
					GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeed;
				}
			}

			if (turbo_bool == true)
			{
					if(GetComponent<Rigidbody2D>().velocity.magnitude > maxSpeedTurbo)
					{
						
						GetComponent<Rigidbody2D>().velocity = GetComponent<Rigidbody2D>().velocity.normalized * maxSpeedTurbo;
					}

			}
			if (Input.GetButtonDown ("P2 Fire"))
			{
				if (PowerUp == 1)
				{
					Instantiate (DisparoAjolote, transform.position, transform.rotation);
					municion -= 1;
					
				}
				
			}
		}
		else if (stunned == true)
		{
			StartCoroutine(GetStunned());
		}
		if (municion == 0)
		{
			PowerUp = 0;
		}

		
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.tag == "asteroide" )
		{
			if (turbo_bool == false && collider.GetComponent<Script_Burbuja>().can_stun == false)
			{
				GetComponent<Rigidbody2D>().AddForce (1000 * new Vector2((transform.position.x - collider.transform.position.x), (transform.position.y - collider.transform.position.y)));	
				Instantiate (explosion_burbuja, collider.transform.position, new Quaternion());
				Destroy (collider.gameObject);
			}
			else if (turbo_bool == true && collider.GetComponent<Script_Burbuja>().can_stun == false)
			{
				collider.GetComponent<Script_Burbuja>().maxSpeed = 4;
				GetComponent<Rigidbody2D>().AddForce (500 * new Vector2((transform.position.x - collider.transform.position.x), (transform.position.y - collider.transform.position.y)));
				collider.GetComponent<Rigidbody2D>().AddForce (900 * new Vector2((collider.transform.position.x - transform.position.x), (collider.transform.position.y - transform.position.y)));
				collider.GetComponent<Script_Burbuja>().can_stun = true;
			}
			else if (collider.GetComponent<Script_Burbuja>().can_stun == true)
			{
				GetComponent<Rigidbody2D>().AddForce (350* new Vector2((transform.position.x - collider.transform.position.x), (transform.position.y - collider.transform.position.y)));
				Instantiate (explosion_burbuja_stun, collider.transform.position, new Quaternion());
				Destroy (collider.gameObject);
				GameObject stunned_effect;
				stunned_effect = Instantiate (stun_effect);
				stunned_effect.GetComponent<Stunned_Effect_script>().owner = gameObject;
				stunned = true;
			}

		}
		if (collider.gameObject.tag == "bala_mob" || collider.gameObject.tag == "mob")
		{
			Destroy (gameObject);
			Destroy (collider.gameObject);
			Instantiate (Explosion_Jugador, transform.position, new Quaternion());
			
			
		}
		if (collider.gameObject.tag == "PowerUp")
		{
			if (PowerUp == 0)
			{
				Powerup_PickedUp = collider.gameObject;
				StartCoroutine(PowerUpPickUp());

			}

		}
		if (collider.gameObject.tag == "Bala_jugador_1")
		{
			Destroy (gameObject);
			Destroy (collider.gameObject);
			Instantiate (Explosion_Jugador, transform.position, new Quaternion());
		}

	}

	IEnumerator PowerUpPickUp()
	{
		//PowerUp = Random.Range (1, 6);
		powerup_animator = Powerup_PickedUp.GetComponent<Animator> ();
		Powerup_PickedUp.GetComponent<PowerUp_Script> ().PowerUp_available = false;
		PowerUpSound.Play ();
		PowerUp = 1;
		if (PowerUp == 1)
		{
			municion = 3;
			arma.GetComponent<Image>().sprite = Arma1;
		}
		powerup_animator.SetBool ("pick_up", true);
		yield return new WaitForSeconds(0.45f);	
		Destroy (Powerup_PickedUp);
	}
	IEnumerator GetStunned()
	{
		animator.SetBool("Movement", false);
		//parar el sonido
		SonidoMotor.Stop ();
		yield return new WaitForSeconds (1.5f);
		stunned = false;
	}
}